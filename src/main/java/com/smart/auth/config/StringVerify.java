/**
 * ****************************************************************
 * StringVerify.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config;

/**
 * Class StringVerify.
 * This class implements method for manipulate string.
 *
 * @author : Jose Antonio Rodriguez Martinez
 */
public final class StringVerify {

  /**
   * Constructor empty.
   */
  private StringVerify() {
    //empty
  }

  /**
   * Retrieves a Modified String.
   *
   * @param value    String to be modified.
   * @param numEspacio Modified String Length.
   * @param stAdd      Charater to fill String modified.
   * @param bder       if character to fill has to be on left (false) or right (true).
   * @return String : stValue modified.
   */
  public static String getFormatAlpha(final String value,
                                      final int numEspacio, final String stAdd,
                                      final boolean bder) {
    String stValue = value;
    if (stValue != null && numEspacio > 0) {
      if (stValue.length() > numEspacio) {
        return stValue.substring(0, numEspacio);
      }

      for (int i = stValue.length(); i < numEspacio; i++) {
        stValue = bder ? stValue + stAdd : stAdd + stValue;
      }

    }
    return stValue;
  }
}
