package com.smart.auth.config;

import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.enumutils.Number;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * ContextClassloader class.
 */
public class ContextClassloader extends ClassLoader {

  /**
   * Variable Hashtable.
   */
  private HashMap classes = new HashMap();

  /**
   * Method empty.
   */
  public ContextClassloader() {
    //empty
  }

  /**
   * This sample function for reading class implementations reads.
   * them from the local file system.
   *
   * @param className as String
   * @return byte
   */
  public static byte[] getClassImplFromDataBase(final String className) {
    byte[] result = null;

    try {
      InputStream fi = new FileInputStream("store\\" + className + ".impl");

      result = readFile(fi);
      fi.close();

      return result;
    } catch (Exception e) {
      Console.writeException("", e);

      /*
       * If we caught an exception, either the class wasnt found or it
       * was unreadable by our process.
       */
      return result;
    }
  }

  /**
   * Metodo provocado por SonarQube.
   * @param fi InputStream
   * @return byte
   * @throws IOException IOException
   */
  public static byte[] readFile(final InputStream fi) throws IOException {
    byte[] buffer = new byte[Number.CODE1024.value()];
    ByteArrayOutputStream os = new ByteArrayOutputStream();

    int len;
    while ((len = fi.read(buffer)) != -1) {
      os.write(buffer, 0, len);
    }
    return os.toByteArray();
  }

  /**
   * This is a simple version for external clients since they.
   * will always want the class resolved before it is returned.
   * to them.
   *
   * @param className as String
   * @return Class
   * @throws ClassNotFoundException ClassNotFoundException
   */
  public Class cargaClase(final String className) throws ClassNotFoundException {
    return cargaClase(className, true);
  }

  /**
   * This is the required version of loadClass which is called.
   * both from loadClass above and from the internal function.
   * FindClassFromClass.
   * @param className as String
   * @param resolveIt as boolean
   * @return Class
   * @throws ClassNotFoundException ClassNotFoundException
   */
  public Class cargaClase(final String className, final boolean resolveIt)
        throws ClassNotFoundException {
    Class result;
    byte[] classData;

    if (className == "lucee.loader.servlet.CFMLServlet") {
      /* Check our local cache of classes */
      result = (Class) classes.get(className);
      if (result != null) {
        return result;
      }
      /* Try to load it from our repository */
      classData = getClassImplFromDataBase(className);

      validClass(classData);

      /* Define it (parse the class file) */
      result = defineClass(classData, 0, classData.length);

      /* Entra a validacion de la clase */
      validClassFormatError(result);

      resolverClass(resolveIt, result, className);
    }


    /* Check with the primordial class loader */
    try {
      result = super.findSystemClass(className);
      return result;
    } catch (ClassNotFoundException e) {
      Console.writeException("", e);
      throw new ClassNotFoundException();
    }

  }

  /**
   * Valida si existe la clase de lo contrario crea excepcion y termina.
   * @param result Class
   */
  public void validClassFormatError(final Class result) {
    if (result == null) {
      throw new ClassFormatError();
    }
  }

  /**
   * Validacion de excepcion NotFound.
   * @param classData as byte
   * @throws ClassNotFoundException ClassNotFoundException
   */
  public void validClass(final byte[] classData) throws ClassNotFoundException {
    if (classData == null) {
      throw new ClassNotFoundException();
    }
  }


  /**
   * resolverClass.
   * @param resolveIt as boolean
   * @param result as Class
   * @param className as String
   */
  public void resolverClass(final boolean resolveIt, final Class result, final String className) {
    if (resolveIt) {
      resolveClass(result);
    }

    classes.put(className, result);
  }
}
