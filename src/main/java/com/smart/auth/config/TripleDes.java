/**
 * ****************************************************************
 * TripleDes.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 -******************************************************************
 */

package com.smart.auth.config;

import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.enumutils.Number;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;

/**
 * Class TripleDes.
 * @author Jose A Rodriguez M.
 */
public final class TripleDes {

  /** * variable TRANSFORMATION. */
  public static final String TRANSFORMATION = "DESede/ECB/NoPadding";

  /**
   * Constructor empty.
   */
  private TripleDes() {
    //empty
  }

  /**
   * Method stDecodeData.
   * @param input byte[].
   * @param stTripleDesKey String.
   * @return byte[].
   */
  public static synchronized byte[] stDecodeData(final byte[] input, final String stTripleDesKey) {
    Cipher ciCipher;
    SecretKey seTripleDesKey;
    byte[] byCipherTxt = null;

    try {
      ciCipher = Cipher.getInstance(TRANSFORMATION);
      seTripleDesKey = seGetTripleDesKey(stTripleDesKey);

      if (seTripleDesKey != null) {

        ciCipher.init(Cipher.DECRYPT_MODE, seTripleDesKey);
        byCipherTxt = ciCipher.doFinal(input);
      }
    } catch (Exception e) {
      Console.writeException("", e);
    }
    return byCipherTxt;
  }

  /**
   * Method stEncodeData.
   * @param btInput byte[].
   * @param stTripleDesKey String.
   * @param padding byte.
   * @return byte[].
   */
  public static synchronized byte[] stEncodeData(final byte[] btInput, final String stTripleDesKey,
                                                 final byte padding) {
    byte[] input = btInput;
    Cipher ciCipher;
    SecretKey seTripleDesKey;
    byte[] output = null;

    if (input != null && stTripleDesKey != null) {

      if (input.length % Number.CODE8.value() != 0) {
        byte[] fixed = Arrays.copyOf(input, input.length + Number.CODE8.value()
              - (input.length % Number.CODE8.value()));

        for (int i = input.length; i < fixed.length; i++) {
          fixed[i] = padding;
        }
        input = fixed;
      }

      try {
        ciCipher = Cipher.getInstance(TRANSFORMATION);
        seTripleDesKey = seGetTripleDesKey(stTripleDesKey);

        if (seTripleDesKey != null) {
          ciCipher.init(Cipher.ENCRYPT_MODE, seTripleDesKey);
          output = ciCipher.doFinal(input);
        }
      } catch (Exception e) {
        Console.writeException("", e);
      }
    }

    return output;
  }

  /**
   * seGetTripleDesKey.
   * @param stKey String.
   * @return SecretKey.
   */
  private static synchronized SecretKey seGetTripleDesKey(final String stKey) {
    SecretKey seTripleDesKey = null;
    String stKey1;
    String stKey2;
    String stTripleDesKey;
    byte[] byTripleDesKey;

    if (stKey.length() == Number.CODE32.value()) {
      stKey1 = stKey.substring(0, Number.CODE16.value());
      stKey2 = stKey.substring(Number.CODE16.value());
      stTripleDesKey = stKey1 + stKey2 + stKey1;
      byTripleDesKey = ConversionUtils.string2Bcd(stTripleDesKey);
      seTripleDesKey = new SecretKeySpec(byTripleDesKey, "DESede");
    } else {
      if (stKey.length() == Number.CODE16.value()) {
        byTripleDesKey = ConversionUtils.string2Bcd(stKey + stKey + stKey);
        seTripleDesKey = new SecretKeySpec(byTripleDesKey, "DESede");

      }
    }

    return seTripleDesKey;
  }
}
