package com.smart.auth.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;

import javax.sql.DataSource;

/**
 * Clase MySqlAppConfig.java.
 * */
@Configuration
@PropertySource("classpath:application.properties")
public class MySqlAppConfig {
  /**
   * Declaración de la variable driverClassName.
   * */
  @Value("${app.datasource.driverClassName}")
  private String driverClassName;
  /**
   * Declaración de la variable url.
   * */
  @Value("${app.datasource.url}")
  private String url;
  /**
   * Declaración de la variable username.
   * */
  @Value("${app.datasource.username}")
  private String username;
  /**
   * Declaración de la variable password.
   * */
  @Value("${app.datasource.password}")
  private String password;

  /**
   * Bean del dataSource.
   * @return DataSource
   * */
  @Bean(name = "dataSource")
  public DataSource getDataSource() {
    return DataSourceBuilder
          .create()
          .username(username)
          .password(password)
          .url(url)
          .driverClassName(driverClassName)
          .build();
  }

  /**
   * Métedo dataSourceInitializer que inicializa la base de datos.
   * @param dataSource as DataSource.
   * @return DataInitializer
   * */
  @Bean
  public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
    final DataSourceInitializer initializer = new DataSourceInitializer();
    initializer.setDataSource(dataSource);
    return initializer;
  }
}
