/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.config;

import com.smart.auth.exception.CodeMessageException;
import com.smart.auth.exception.MicroserviceException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.logs.SystemLog;
import com.smart.auth.exception.message.MessageRespose;
import com.smart.auth.service.error.IErrorServices;
import com.smart.auth.service.impl.error.Management;
import com.smart.auth.model.enumutils.Number;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.Cipher;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;

/**
 * Clase InitApplication.java.
 *
 * @author Jose A Rodriguez M
 */
public class InitApplication implements ServletContextListener {
  /**
   * Declaración de variable SYSTEM_IDOPERATION.
   */
  public static final String SYSTEM_IDOPERATION = SystemLog.newIdOperation();
  /**
   * Declaración de variable APPNAME.
   */
  public static final String APPNAME = "appGNP";
  /**
   * Declaración de variable path.
   */
  private static String path;
  /**
   * Declaración de variable defaultLanguage.
   */
  private static String defaultLanguage;
  /**
   * Declaración de variable contextName.
   */
  private static String contextName = null;
  /**
   * Declaración de variable context.
   */
  private ServletContext context = null;

  /**
   * Instancia de la clase Logger.
   */
  private static final Logger LOGGER = LogManager.getLogger(InitApplication.class.getName());

  /**
   * Método contextInitialized.
   *
   * @param sce as ServletContextEvent.
   */
  @Override
  public void contextInitialized(final ServletContextEvent sce) {
    try {
      fixKeyLength();
      context = sce.getServletContext();
      initConstants();
    } catch (CodeMessageException e) {
      Console.writeException("00000", e);
    }
  }

  /**
   * fixKeyLength.
   * @throws CodeMessageException CodeMessageException.
   */
  public static void fixKeyLength() throws CodeMessageException {
    int newMaxKeyLength;
    try {
      ContextClassloader contextClassloader = new ContextClassloader();


      newMaxKeyLength = Cipher.getMaxAllowedKeyLength("AES");
      if (newMaxKeyLength < Number.CODE256.value()) {
        Class c = contextClassloader.cargaClase("javax.crypto.CryptoAllPermissionCollection");
        Constructor con = c.getDeclaredConstructor();
        con.setAccessible(true);
        Object allPermissionCollection = con.newInstance();
        Field f = c.getDeclaredField("all_allowed");
        f.setAccessible(true);
        f.setBoolean(allPermissionCollection, true);
        c = contextClassloader.cargaClase("javax.crypto.CryptoPermissions");
        con = c.getDeclaredConstructor();
        con.setAccessible(true);
        Object allPermissions = con.newInstance();
        f = c.getDeclaredField("perms");
        f.setAccessible(true);
        ((Map) f.get(allPermissions)).put("*", allPermissionCollection);
        c = contextClassloader.cargaClase("javax.crypto.JceSecurityManager");
        f = c.getDeclaredField("defaultPolicy");
        f.setAccessible(true);
        Field mf = Field.class.getDeclaredField("modifiers");
        mf.setAccessible(true);
        mf.setInt(f, f.getModifiers() & ~Modifier.FINAL);
        f.set(null, allPermissions);
        newMaxKeyLength = Cipher.getMaxAllowedKeyLength("AES");
      }
    } catch (Exception e) {
      IErrorServices error = new MessageRespose(((MicroserviceException) e).getErrorMsg(), ((MicroserviceException) e).getErrorCode());
      throw new CodeMessageException(error);
    }

    validfixKeyLength(newMaxKeyLength);

  }

  /**
   *  Validacion de tamaño de la clase  y termina si es menor al numero.
   * @param newMaxKeyLength as int
   * @throws CodeMessageException CodeMessageException
   */
  public static void validfixKeyLength(final int newMaxKeyLength) throws CodeMessageException {
    if (newMaxKeyLength < Number.CODE256.value()) {
      IErrorServices error = new MessageRespose("Failed manually overriding key-length permissions.", "96");
      throw new CodeMessageException(error);
    }
  }

  /**
   * método initConstants.
   */
  public synchronized void initConstants() {
    try {
      Thread.currentThread().setName("InitApplication_Thread");
      path = new File("").getAbsolutePath();
      contextName = context.getContextPath();

      Console.writeln(Console.Level.INFO, SYSTEM_IDOPERATION, "Nueva instancia de InitApplication");
      Thread.currentThread().setName(SYSTEM_IDOPERATION);
      Management.start(SYSTEM_IDOPERATION, path, path);
    } catch (Exception ex) {
      Console.writeException(ex);
    }
  }

  /**
   * Método contextDestroyed.
   *
   * @param sce as ServletContextEvent
   */
  @Override
  public void contextDestroyed(final ServletContextEvent sce) {
    LOGGER.info(" ");
  }

  /**
   * Getter getPath.
   *
   * @return String
   */
  public static String getPath() {
    return path;
  }

  /**
   * Getter getDefaultLanguage.
   *
   * @return String
   */
  public static String getDefaultLanguage() {
    return defaultLanguage;
  }

  /**
   * Getter getContextName.
   *
   * @return String
   */
  public static String getContextName() {
    return contextName;
  }

  /**
   * Getter getSystemIdOperation.
   *
   * @return String
   */
  public static String getSystemIdOperation() {
    return SYSTEM_IDOPERATION;
  }

}
