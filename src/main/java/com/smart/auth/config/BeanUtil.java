/*******************************************************************
 BeanUtil.java
 @author : Carlos A Lopez P
 @creationDate : 16/03/2019
 @specFile:
 @revisedBy : Carlos A Lopez P
 @date : 16/03/2019
 *******************************************************************/


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.config;

import org.springframework.context.ApplicationContext;

/**
 * Class BeanUtil.java.
 *
 */

public class BeanUtil {

  /**
   * DEclaración de la clase ApplicationContext.
   * */
  private static ApplicationContext context;

  /**
   * Constructor.*/
  protected BeanUtil() {
    //empty for test.
  }


  /**
   * Método setApplicationContext.
   * @param applicationContext as ApplicationContext.java
   * */
  public static void setApplicationContext(final ApplicationContext applicationContext) {
    context = applicationContext;
  }

  /**
   * Obtener bean.
   * @param beanClass Class
   * @param <T> as class
   * @return applicationContext
   */
  public static <T> T getBean(final Class<T> beanClass) {

    return context.getBean(beanClass);

  }
}

//<editor-fold defaultstate="collapsed" desc="Modifications comments">

/************************************************************
 * @updateDate: dd/MM/yyyy
 * @author: ?
 * @revisedBy : ?
 *           @date : dd/MM/yyyy
 * @Description:
 *   (Write text here)
 * ************************************************************
 *  *
 *  *
 *  *
 * ************************************************************
 */

//</editor-fold>

