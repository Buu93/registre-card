/**
 * ****************************************************************
 * HashMapSTypes.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config;

/**
 * Class HashMapSTypes.java.
 *
 * @author Jose A Rodriguez M.
 */
public class HashMapSTypes {

  /**
   * variable idOperation.
   */
  private final String idOperation;

  /////////////////////////////////////////////////////////////////////
  ////Functions to use variable information
  /////////////////////////////////////////////////////////////////////

  /**
   * Constructor HashMapSTypes.
   * @param idOperation HashMapSTypes.
   */
  public HashMapSTypes(final String idOperation) {
    this.idOperation = idOperation;
  }

  /** Getter idOperation.
   * @return String
   * */
  public String getIdOperation() {
    return idOperation;
  }
}
