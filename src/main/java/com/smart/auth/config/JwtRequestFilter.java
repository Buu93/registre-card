package com.smart.auth.config;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.smart.auth.service.JwtUserDetailsService;
import com.smart.auth.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.UrlPathHelper;


@Component
public class JwtRequestFilter extends OncePerRequestFilter {

  @Autowired
  private JwtUserDetailsService jwtUserDetailsService;

  @Autowired
  private TokenService tokenService;

  private String urlcliente;
  private String urlImagen;
  private String txtTitulo;
  private String colorFondo;
  private String colorTitulo;

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
        throws ServletException, IOException {

    String Xparam = "X-AUTHCHECKOUT";
    HttpServletRequest httpRequest = asHttp(request);
    HttpServletResponse httpResponse = asHttp(response);
    String token;
    String cliente;
    String referencia;
    String pagina;

    String resourcePath = new UrlPathHelper().getPathWithinApplication(httpRequest);

    System.out.println("Pagina:::" + resourcePath);

    Optional<String> xcabecero = Optional.ofNullable(request.getHeader(Xparam));
    if (xcabecero.isPresent()) {
      String[] cadena = xcabecero.get().split("\\|");
      token = cadena[0];
      cliente = cadena[1];
      referencia = cadena[2];
      pagina = cadena[3].toUpperCase();
    } else {
      token = request.getParameter("APP_TOKEN_SMART");
      cliente = request.getParameter("APP_ID_CLIENT");
      referencia = request.getParameter("APP_ID_TXN");
      validaDatos(request);
      pagina = resourcePath.substring(1);
    }


    System.out.println("Empezando Validacion....");
    String jwtToken = token + "|" +
          cliente + "|" +
          referencia + "|" +
          pagina + "|" +
          urlcliente + "|" +
          urlImagen + "|" +
          txtTitulo + "|" +
          colorFondo + "|" +
          colorTitulo;


    UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(cliente);

    // if token is valid configure Spring Security to manually set
    // authentication
    if (tokenService.contains(jwtToken)) {

      UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
            userDetails, null, userDetails.getAuthorities());
      usernamePasswordAuthenticationToken
            .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
      // After setting the Authentication in the context, we specify
      // that the current user is authenticated. So it passes the
      // Spring Security Configurations successfully.
      SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    response.setHeader(Xparam, jwtToken);

    chain.doFilter(request, response);
  }

  @Override
  protected boolean shouldNotFilter(HttpServletRequest request) {
    String path = request.getServletPath();
    return !path.startsWith("/api/");
  }

  private HttpServletRequest asHttp(ServletRequest request) {
    return (HttpServletRequest) request;
  }

  private HttpServletResponse asHttp(ServletResponse response) {
    return (HttpServletResponse) response;
  }

  public void validaDatos(HttpServletRequest request) {

    if (null != request.getParameter("URL_CALLBACK")) {
      urlcliente = request.getParameter("URL_CALLBACK");
    } else {
      urlcliente = " ";
    }
    if (null != request.getParameter("APP_URL_IMG")) {
      urlImagen = request.getParameter("APP_URL_IMG");
    } else {
      urlImagen = " ";
    }
    if (null != request.getParameter("APP_MAIN_TEXT")) {
      txtTitulo = request.getParameter("APP_MAIN_TEXT");
    } else {
      txtTitulo = " ";
    }
    if (null != request.getParameter("APP_BACK_COLOR")) {
      colorFondo = request.getParameter("APP_BACK_COLOR");
    } else {
      colorFondo = " ";
    }
    if (null != request.getParameter("APP_FIELD_COLOR")) {
      colorTitulo = request.getParameter("APP_FIELD_COLOR");
    } else {
      colorTitulo = " ";
    }
  }


}