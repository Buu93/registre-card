package com.smart.auth.config;

import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.enumutils.Number;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.MessageDigest;


/**
 *  Method public class HashUtils.
 * @author CALP -Smart BT.
 */
public final class HashUtils {

  /**
   * Inicializate Log.
   */
  private static final Logger LOG = LogManager.getLogger(HashUtils.class);

  /**
   * constructor empty.
   */
  private HashUtils() {
    //empty
  }

  /**
   * convertion SHA1.
   * @param clearTxt String.
   * @return String.
   */
  public static String sha1(final String clearTxt) {
    return sha(clearTxt, "UTF-8");
  }

  /**
   * Encrypt sha.
   * @param clearTxt String.
   * @param charsetName String.
   * @return String.
   */
  public static String sha(final String clearTxt, final String charsetName) {
    StringBuilder sb = new StringBuilder();

    try {
      MessageDigest messageDigestDigest = MessageDigest.getInstance("SHA1");
      byte[] result = messageDigestDigest.digest(clearTxt.getBytes(charsetName));

      for (int i = 0; i < result.length; i++) {
        sb.append(Integer.toString((result[i] & Number.CODE0XFF.value())
              + Number.CODE0X100.value(), Number.CODE16.value()).substring(1));
      }
    } catch (Exception ex) {
      Console.writeException("", ex);
      LOG.info(ex.getClass().getName() + ":" + ex.getMessage());
    }

    return sb.toString();
  }
}
