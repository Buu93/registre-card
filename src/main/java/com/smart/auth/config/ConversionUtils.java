/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates.
 * and open the template in the editor.
 */

package com.smart.auth.config;

import com.smart.auth.model.enumutils.Number;
import java.util.Calendar;
import java.util.Locale;

/**
 * Class ConversionUtils.java.
 *
 * @author Jose A Rodriguez M.
 */
public final class ConversionUtils {

  /**
   * Data exadecimal.
   */
  static final String HEXES = "0123456789ABCDEF";

  /**
   * Constructor.
   */
  protected ConversionUtils() {
    //empty for test
  }

  /**
   * Método bcd2String.
   * @param buffer     type byte[].
   * @param beginIndex type int.
   * @param length     type int.
   * @return String.
   */
  public static String bcd2String(final byte[] buffer, final int beginIndex, final int length) {
    StringBuilder retVal = new StringBuilder();

    for (int i = beginIndex, count = 0; i < buffer.length && count < length; i++, count++) {
      byte b1 = (byte) ((buffer[i] & Number.CODE0XF0.value()) >> Number.CODE4.value());
      byte b2 = (byte) (buffer[i] & Number.CODE0X0F.value());
      retVal.append((char) (b1 < Number.CODE10.value() ? b1 + '0' : (b1 - Number.CODE10.value()) + 'A'));
      retVal.append((char) (b2 < Number.CODE10.value() ? b2 + '0' : (b2 - Number.CODE10.value()) + 'A'));
    }

    return retVal.toString();
  }

  /**
   * Método string2Bcd.
   * @param cadena type String.
   * @return byte[].
   */
  public static byte[] string2Bcd(final String cadena) {

    String str = cadena;

    byte[] data;
    Locale locale = Locale.getDefault();

    if (str.length() % 2 != 0) {
      str = "0" + str;
    }

    str = str.toUpperCase(locale);

    data = new byte[str.length() >> 1];

    for (int i = 0; i < data.length; i++) {
      char c1 = str.charAt(i << 1);
      char c2 = str.charAt((i << 1) + 1);
      int c3 = c1 >= '0' && c1 <= '9' ? c1 - '0' : c1 - 'A' + Number.CODE10.value();
      int c4 = c2 >= '0' && c2 <= '9' ? c2 - '0' : c2 - 'A' + Number.CODE10.value();
      data[i] = (byte) (((c3) << Number.CODE4.value())
            + (c4));
    }

    return data;
  }

  /**
   * Método binaryToString.
   * @param raw  type byte[].
   * @param size type int.
   * @return String.
   */
  public static String binaryToString(final byte[] raw, final int size) {
    if (raw == null) {
      return null;
    }
    final StringBuilder hex = new StringBuilder(2 * size);
    for (int i = 0; i < raw.length && i < size; i++) {
      hex.append(HEXES.charAt((raw[i] & Number.CODE0XF0.value()) >> Number.CODE4.value()))
            .append(HEXES.charAt(raw[i] & Number.CODE0X0F.value()));
    }
    return hex.toString();
  }

  /**
   * Método binaryToStringFormatted.
   *
   * @param raw  type byte [].
   * @param size int.
   * @return String.
   */
  public static String binaryToStringFormatted(final byte[] raw, final int size) {
    if (raw == null || size == 0) {
      return "";
    }

    StringBuilder hex = new StringBuilder();
    hex.append("\r\n");
    for (int i = 0; i < raw.length && i < size; i++) {
      hex.append(HEXES.charAt((raw[i] & Number.CODE0XF0.value()) >> Number.CODE4.value()))
            .append(HEXES.charAt(raw[i] & Number.CODE0X0F.value())).append(" ");
      if (i % Number.CODE16.value() == Number.CODE15.value()) {
        hex.append("\r\n");
      }
    }
    return hex.toString();
  }

  /**
   * Método paddLeftZero.
   *
   * @param source type String.
   * @param size   type int.
   * @return String.
   */
  public static String paddLeftZero(final String source, final int size) {
    StringBuilder builder = new StringBuilder(source == null ? "" : source);

    if (size == 0) {
      return "";
    }

    while (builder.length() < size) {
      builder.insert(0, "0");
    }

    return builder.toString();
  }

  /**
   * Método stringToCalendar.
   *
   * @param date type String.
   * @param time type String.
   * @return Calendar.
   */
  public static Calendar stringToCalendar(final String date, final String time) {
    Calendar intervalStart = Calendar.getInstance();
    intervalStart.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(0, 2)));
    intervalStart.set(Calendar.MONTH, Integer.parseInt(date.substring(Number.CODE3.value(), Number.CODE5.value())) - 1);
    intervalStart.set(Calendar.YEAR, Integer.parseInt(date.substring(Number.CODE6.value())));
    intervalStart.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time.substring(0, 2)));
    intervalStart.set(Calendar.MINUTE, Integer.parseInt(time.substring(2)));
    intervalStart.set(Calendar.MILLISECOND, 0);
    return intervalStart;
  }

  /**
   * Método bytesToHex.
   *
   * @param hexaBytes type byte[].
   * @return String.
   */
  public static String bytesToHex(final byte[] hexaBytes) {
    StringBuilder hex = new StringBuilder();
    for (int i = 0; i < hexaBytes.length; i++) {
      if ((hexaBytes[i] & Number.CODE0XFF.value()) <= Number.CODE0X0F.value()) {
        hex.append("0");
      }
      hex.append(Long.toString((int) hexaBytes[i] & Number.CODE0XFF.value(), Number.CODE16.value()));
    }
    return hex.toString();
  }

  /**
   * Método hexToBytes.
   *
   * @param stTextHex type String.
   * @return byte[].
   */
  public static byte[] hexToBytes(final String stTextHex) {
    byte[] res = null;

    if (stTextHex != null) {
      res = new byte[stTextHex.length() / 2];
      for (int i = 0, j = 0; i < stTextHex.length() - 1; i += 2, j++) {
        String output = stTextHex.substring(i, i + 2);
        int decimal = Integer.parseInt(output, Number.CODE16.value());
        res[j] = (byte) (decimal & Number.CODE0XFF.value());
      }
    }

    return res != null ? res : new byte[0];
  }

  /**
   * Método changeAccents.
   *
   * @param sacute type String.
   * @return String.
   */
  public static synchronized String changeAccents(final String sacute) {
    String sret = sacute;
    sret = sret.replaceAll("á", "a");
    sret = sret.replaceAll("é", "e");
    sret = sret.replaceAll("í", "i");
    sret = sret.replaceAll("ó", "o");
    sret = sret.replaceAll("ú", "u");
    sret = sret.replaceAll("Á", "A");
    sret = sret.replaceAll("É", "E");
    sret = sret.replaceAll("Í", "I");
    sret = sret.replaceAll("Ó", "O");
    sret = sret.replaceAll("Ú", "U");
    return sret;
  }

  /**
   * Método forceSize.
   *
   * @param cadena         type String.
   * @param size        type int.
   * @param charPadding type char.
   * @param left        type boolean.
   * @return String.
   */
  public static String forceSize(final String cadena, final int size, final char charPadding, final boolean left) {
    String cad = cadena;

    if (cad.length() == size) {
      return cad;
    }

    if (cad.length() > size) {
      return cad.substring(0, size - 1);
    }

    StringBuilder res = new StringBuilder();

    for (int i = 0; i < size - cad.length(); i++) {
      res.append(charPadding);
    }

    cad = left ? res.toString() + cad : cad + res.toString();
    return cad;
  }

  /**
   * Método hexToString.
   *
   * @param arrayEncript type byte[].
   * @return String.
   */
  public static String hexToString(final byte[] arrayEncript) {
    if (arrayEncript == null) {
      return "";
    }

    StringBuilder dataEncript = new StringBuilder();
    for (int i = 0; i < arrayEncript.length; i++) {
      int aux = arrayEncript[i] & Number.CODE0XFF.value();
      if (aux < Number.CODE16.value()) {
        dataEncript.append("0");
      }
      dataEncript.append(Integer.toHexString(aux));
    }
    return dataEncript.toString();
  }

  /**
   * Método stringToHex.
   *
   * @param dataEncript type String.
   * @return byte[].
   */
  public static byte[] stringToHex(final String dataEncript) {
    byte[] enBytes = new byte[dataEncript.length() / 2];
    for (int i = 0; i < enBytes.length; i++) {
      int index = i * 2;
      String aux = dataEncript.substring(index, index + 2);
      int v = Integer.parseInt(aux, Number.CODE16.value());
      enBytes[i] = (byte) (v & Number.CODE0XFF.value());
    }
    return enBytes;
  }

  // Agregado por moises.martinez // 2012-05-14 //

}
