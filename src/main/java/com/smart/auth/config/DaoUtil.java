package com.smart.auth.config;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.StatementCreatorUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Class DaoUtil.
 */
public final class DaoUtil {

  /**
   * method DaoUtil.
   */
  private DaoUtil() {
    //empty
  }

  /**
   * Method createAndGetId.
   * @param temp JdbcTemplate.
   * @param sql String.
   * @param args Object[].
   * @param arrypes int.
   * @return Long.
   */
  public static Long createAndGetId(final JdbcTemplate temp, final String sql, final Object[] args,
                                    final int[] arrypes) {
    Long id;
    KeyHolder kh = new GeneratedKeyHolder();
    temp.update(new PreparedStatementCreator() {
      @Override
      public PreparedStatement createPreparedStatement(final Connection con) throws SQLException {
        int i = 1;
        int indx = 0;
        PreparedStatement ps;
        ps = con.prepareStatement(sql, new String[]{"id"});
        for (Object o : args) {
          StatementCreatorUtils.setParameterValue(ps, i, arrypes[indx], o);
          i++;
          indx++;
        }
        return ps;
      }
    }, kh);

    Number key = kh.getKey();
    if (key instanceof BigDecimal) {
      id = ((BigDecimal) key).longValue();
    } else if (key instanceof BigInteger) {
      id = ((BigInteger) key).longValue();
    } else {
      id = (Long) key;
    }

    return id;
  }

}
