/**
 * ****************************************************************
 * ByteRegister.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config.cipher;

import com.smart.auth.model.enumutils.Number;
/**
 * Class ByteRegister.java.
 *
 * @author Jose A Rodriguez M
 */
public class ByteRegister {

  /**
   * constante size inicializada en 0.
   * */
  private int size = 0;
  /**
   * Constante reg inicializada en null.
   * */
  private byte[] reg = null;


  /**
   * Sobrecarga de método.
   * @param val type byte[]
   */
  public ByteRegister(final byte[] val) {
    reg = new byte[val.length];
    this.size = val.length;
    setBytes(val);
  }

  /**
   * Constructor ByteRegister.
   * */
  public ByteRegister() {
//    is empty for test.
  }

  /**
   * Método getLength.
   * @return int
   * */
  public int getLength() {
    return size;
  }

  /**
   * Método getBuffer.
   * @return byte[]
   * */
  public final byte[] getBuffer() {
    return reg;
  }

  /**
   * Setter Bytes.
   * @param val as byte[]
   */
  public final void setBytes(final byte[] val) {
    for (int i = 0, j = val.length - 1; i < val.length && j >= 0; i++, j--) {
      setByte(val[j], i);
    }
  }

  /**
   * Método getByte.
   * @param nbyte type int
   * @return byte
   */
  public final byte getByte(final int nbyte) {
    if (nbyte >= size) {
      return 0;
    }

    return reg[size - 1 - nbyte];
  }

  /**
   * Método setByte.
   * @param val as byte
   * @param nbyte as int
   */
  public final void setByte(final byte val, final int nbyte) {
    if (nbyte < size) {
      reg[size - 1 - nbyte] = val;
    }
  }

  /**
   * Método ByteRegister.
   * @param val as byte
   * @param pos as int
   * @return ByteRegister
   */
  public final ByteRegister and(final byte val, final int pos) {
    ByteRegister bregister = new ByteRegister(reg);
    bregister.setByte((byte) ((bregister.getByte(pos) & val) & Number.CODE0XFF.value()), pos);
    return bregister;
  }

  /**
   * Método ByteRegister.
   * @param byteReg type ByteRegister
   * @return ByteRegister
   */
  public final ByteRegister and(final ByteRegister byteReg) {
    ByteRegister bregister = new ByteRegister(reg);

    int count = Math.max(byteReg.size, size);

    for (int i = 0; i < count; i++) {
      bregister.setByte((byte) ((byteReg.getByte(i) & Number.CODE0XFF.value())
            & (bregister.getByte(i) & Number.CODE0XFF.value())), i);
    }

    return bregister;
  }
}
