/**
 * ****************************************************************
 * KeyManager.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config.cipher;


import com.smart.auth.service.KGeneratorServices;

import java.util.HashSet;
import java.util.Set;

/**
 * Class KeyManager.java.
 *
 * @author Jose A Rodriguez M
 */
public class KeyManager {

  /**
   * Instancia de la clase KGeneratorServices.
   */
  private KGeneratorServices kg;
  /**
   * Inicialización de clase en null.
   */
  private static KeyManager uniqueInstance = null;
  /**
   * declaración de variable systemkeys.
   */
  private String systemKeys = "";

  /**
   * constructor.
   */
  public KeyManager() {
    kg = new SecureKeyGeneratorServices();
    systemKeys = "KEY_SERVICE_ENCRYPTION";
  }

  /**
   * Método KeyManager.
   *
   * @return uniqueInstance
   */
  public static synchronized KeyManager getInstance() {
    if (uniqueInstance == null) {
      uniqueInstance = new KeyManager();
    }
    return uniqueInstance;
  }

  /**
   * Método SecureString.
   *
   * @param keyName type String
   * @return SecureString
   */
  public synchronized SecureString getKey(final String keyName) {
    if (systemKeys.contains(keyName)) {
      if (kg.containsKey(keyName)) {
        return kg.getKey(keyName);
      } else {
        throw new IllegalArgumentException("Keymanager: system key not found " + keyName);
      }
    } else {
      throw new IllegalArgumentException("Keymanager: no system key " + keyName);
    }
  }
}
