/**
 * ****************************************************************
 * ServiceEncryptionData.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config.cipher;

import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.enumutils.Number;

import java.security.MessageDigest;

/**
 * Class ServiceEncryptionData.java.
 *
 * @author Jose A Rodriguez M
 */
public final class ServiceEncryptionData {

  /**
   * declaraciónde la variable idOperation.
   */
  private static String idOperation = "ServiceEncryptionData";

  /**
   * Constructor.
   */
  private ServiceEncryptionData() {
  }

  /**
   * Encrypt a data block.
   *
   * @param data Data block to encrypt
   * @return The data block dencrypted
   */
  public static String encrypt(final String data) {
    String sret = "";
    try {
      sret = AesCryptUtil.encodeData(data, getKey(idOperation));
    } catch (Exception e) {
      Console.writeException(idOperation, e);
    }
    return sret;
  }

  /**
   * Decrypt a data block.
   *
   * @param data Data block to decrypt
   * @return The data block decrypted
   */
  public static String decrypt(final String data) {
    String sret = "";
    try {
      sret = AesCryptUtil.decodeData(data, getKey(idOperation));
    } catch (Exception e) {
      Console.writeException(idOperation, e);
    }

    return sret;
  }

  /**
   * Método getKey.
   *
   * @param idOperation as String
   * @return SecureString
   */
  private static SecureString getKey(final String idOperation) {
    KeyManager km = KeyManager.getInstance();
    SecureString key = null;
    try {
      SecureString ky = km.getKey("KEY_SERVICE_ENCRYPTION");
      key = ky;
    } catch (Exception ex) {
      key = null;
      Console.writeException(idOperation, ex);
    }
    return key;
  }

  /**
   * Método maskAccount.
   *
   * @param saccount type String
   * @return String
   */
  public static String maskAccount(final String saccount) {
    String ecard = saccount.trim();
    if (ecard != null && "=".indexOf(ecard) >= 0) {
      ecard = ecard.substring(0, "=".indexOf(ecard));
    }
    StringBuilder scardenc = new StringBuilder();
    int lenIni = Number.CODE6.value();
    int lenFin = Number.CODE4.value();
    if (ecard != null && ecard.length() > lenIni) {
      for (int i = 0; i < ecard.length(); i++) {
        if (i < lenIni || i >= (ecard.length() - lenFin)) {
          scardenc.append(ecard.substring(i, i + 1));
        } else {
          scardenc.append("*");
        }
      }

    } else {
      scardenc.append(ecard);
    }
    return scardenc.toString();
  }

  /**
   * Método getMaskedSha256Account.
   *
   * @param input type String
   * @param idOp  type String
   * @return String.
   */
  public static String getMaskedSha256Account(final String input, final String idOp) {
    StringBuilder sb = new StringBuilder();

    try {
      MessageDigest mdigest = MessageDigest.getInstance("SHA-256");
      byte[] result = mdigest.digest(input.getBytes("UTF-8"));

      for (int i = 0; i < result.length; i++) {
        sb.append(Integer.toString((result[i] & Number.CODE0XFF.value())
              + Number.CODE0X100.value(), Number.CODE16.value()).substring(1));
      }
    } catch (Exception ex) {
      Console.writeException(idOp, ex);
    }

    return sb.toString();
  }

}
