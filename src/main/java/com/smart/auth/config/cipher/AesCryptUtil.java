/**
 * ****************************************************************
 * AesCryptUtil.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config.cipher;

import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.enumutils.Number;
import com.smart.auth.config.ConversionUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;


/**
 * Class AesCryptUtil.java.
 *
 * @author Jose A Rodriguez M
 */
public final class AesCryptUtil {
  /**
   * path del método.
   * */
  public static final String METHOD = "AES/CBC/PKCS5PADDING";
  /**
   * Tipo de codificación de la cadena String.
   * */
  public static final String STRINGCODIFICATION = "UTF-8";
  /**
   * Nombre de la clase.
   * */
  private static final String AESCRYPT = "AesCryptUtil";

  /**
   * Constructor de la clase.
   * */
  private AesCryptUtil() {
  }

  /**
   * Método getSecureIVector.
   * @return byte[]
   * */
  private static byte[] getSecureIVector() {
    SecureRandom random = new SecureRandom();
    byte[] bytes = new byte[Number.CODE16.value()];
    random.nextBytes(bytes);
    return bytes;
  }

  /**
   * Método decodificad datos.
   * @param cipherText key
   * @param key as SecureString
   * @return String
   */
  public static String decodeData(final String cipherText, final SecureString key) {
    String result = null;
    try {
      byte[] data = ConversionUtils.stringToHex(cipherText);
      byte[] bkey = getNormalizedKey(data, key);
      data = normalizeData(data);
      byte[] res = decodeData(data, bkey);
      result = new String(res, STRINGCODIFICATION);
    } catch (Exception ex) {
      Console.writeException(AESCRYPT, ex);
    }

    return result;
  }

  /**
   * Método decodeData.
   * @param input as byte[]
   * @param key as byte[]
   * @return byte[]
   * */
  private static byte[] decodeData(final byte[] input, final byte[] key) {
    Cipher ciCipher;
    SecretKey secKey;
    byte[] byCipherTxt = null;

    ByteArrayOutputStream biv = new ByteArrayOutputStream();
    ByteArrayOutputStream bdata = new ByteArrayOutputStream();

    biv.write(input, Number.CODE0.value(), Number.CODE16.value());
    bdata.write(input, Number.CODE16.value(), input.length - Number.CODE16.value());
    AlgorithmParameterSpec paramSpec = new IvParameterSpec(biv.toByteArray());

    try {
      ciCipher = Cipher.getInstance(METHOD);
      secKey = seGetKey128(key);

      ciCipher.init(Cipher.DECRYPT_MODE, secKey, paramSpec);
      byCipherTxt = ciCipher.doFinal(bdata.toByteArray());

    } catch (Exception e) {
      Console.writeException(AESCRYPT, e);
    }
    return byCipherTxt;
  }

  /**
   * Método encodeData.
   * @param data key
   * @param key as SecureString
   * @return String
   */
  public static String encodeData(final String data, final SecureString key) {
    byte[] result = null;
    String sresult = null;
    try {
      result = encodeData(data.getBytes(STRINGCODIFICATION), key.getKeyValue(),
            key.getCurrentVersion());
      sresult = ConversionUtils.hexToString(result);
    } catch (Exception ex) {
      Console.writeException(AESCRYPT, ex);
    }

    return sresult;
  }

  /**
   * Método encodeData.
   * @param input as byte[]
   * @param key as byte[]
   * @param kversion as Long
   * @return byte[]
   * */
  public static byte[] encodeData(final byte[] input, final byte[] key, final Long kversion) {
    Cipher ciCipher;
    SecretKey keyaes;

    ByteArrayOutputStream bdata = new ByteArrayOutputStream();
    byte[] iv = getSecureIVector();
    AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);

    if (input != null && key != null) {
      try {
        ciCipher = Cipher.getInstance(METHOD);
        keyaes = seGetKey128(key);

        ciCipher.init(Cipher.ENCRYPT_MODE, keyaes, paramSpec);
        byte[] output = ciCipher.doFinal(input);
        bdata.write(getByteVersion(kversion));
        bdata.write(iv);
        bdata.write(output);

      } catch (Exception e) {
        Console.writeException("", e);
      }
    }

    return bdata.toByteArray();
  }

  /**Método seGetKey128.
   * @param stKey as byte[]
   * @return secretKey
   * */
  private static synchronized SecretKey seGetKey128(final byte[] stKey) {
    return new SecretKeySpec(stKey, "AES");
  }

  /**
   * Método getNormalizedKey.
   * @param data as byte[]
   * @param key as SecureString
   * @return byte[]
   * */
  private static byte[] getNormalizedKey(final byte[] data, final SecureString key) {
    if (data.length < Number.CODE5.value()) {
      return key.getKeyValue();
    }

    long kv = 0L;

    if (hasVersion(data)) {
      int res = ((data[Number.CODE3.value()] & Number.CODE0XFF.value())
            << Number.CODE8.value()) + (data[Number.CODE4.value()] & Number.CODE5.value());
      kv = (long) res;
    }

    return key.getKeyValue(kv);
  }

  /**
   * Método hasVersion.
   * @param data as byte[]
   * @return boolean
   * */
  public static boolean hasVersion(final byte[] data) {
    return (data[0] | data[1] | data[2]) == 0 && data.length > Number.CODE5.value();
  }

  /**
   * Método normalizeData.
   * @param data as byte[]
   * @return byte[]
   * */
  private static byte[] normalizeData(final byte[] data) {
    byte[] dataRes = data;

    if (hasVersion(data)) {
      dataRes = Arrays.copyOfRange(data, Number.CODE5.value(), data.length);
    }

    return dataRes;
  }

  /**
   * Método getByteVersion.
   * @param v as Long
   * @return byte[]
   * */
  public static byte[] getByteVersion(final Long v) {
    if (v < 1) {
      return new byte[0];
    }

    int aux = (int) (long) v;

    byte[] bres = new byte[]{0, 0, 0, 0, 0};

    bres[Number.CODE3.value()] = (byte) ((aux & Number.CODE0XFF00.value()) >> Number.CODE8.value());
    bres[Number.CODE4.value()] = (byte) (aux & Number.CODE0X00FF.value());

    return bres;
  }

}
