/**
 * ****************************************************************
 * SecureKeyGeneratorServices.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config.cipher;

import com.smart.auth.exception.logs.Console;
import com.smart.auth.service.KGeneratorServices;

import java.util.HashMap;
import java.util.Map;

/**
 * Class SecureKeyGeneratorServices.java.
 *
 * @author Jose A Rodriguez M
 */
public class SecureKeyGeneratorServices implements KGeneratorServices {

  /**
   * Declaración de Map.
   * */
  private Map<String, SecureString> mapKeys = new HashMap<>();

  /**
  * constructor.
   * */
  SecureKeyGeneratorServices() {
    buildKeys();
  }

  /**
   * Método getKey.
   * @param keyName as STring
   * @return SecureString
   * */
  @Override
  public SecureString getKey(final String keyName) {
    if (mapKeys.containsKey(keyName)) {
      return mapKeys.get(keyName);
    }

    return null;
  }

  /**
   * Método getKey.
   * @param keyName as String
   * @param idEnterprise as Long
   * @return SecureString
   * */
  @Override
  public SecureString getKey(final String keyName, final Long idEnterprise) {
    if (mapKeys.containsKey(keyName)) {
      return mapKeys.get(keyName);
    }

    return null;
  }

  /**
   * Método buildKeys.
   * */
  private void buildKeys() {
    try {
      mapKeys.put("KEY_SERVICE_ENCRYPTION", new SecureString("b7621853225cef1a547a230033e1fbca"
            .getBytes("UTF-8")));
    } catch (Exception ex) {
      Console.writeException("", ex);
    }
  }


  /**
   * Método containsKey.
   * @param keyName as String
   * @return boolean
   * */
  @Override
  public boolean containsKey(final String keyName) {
    return mapKeys.containsKey(keyName);
  }


}
