/**
 * ****************************************************************
 * RsaCrypt.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config.cipher;

import com.smart.auth.exception.MicroserviceException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.message.CoreErrorServicesTable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.Cipher;
import java.security.interfaces.RSAPublicKey;


/**
 * Class RsaCrypt.java.
 *
 * @author Jose A Rodriguez M - Smart BT ,S.A. de C.V.
 */
public class RsaCrypt {

  /**
   * Declaración de la constante ALGORITHM.
   * */
  private static final String ALGORITHM = "RSA";

  /**
   * Instancia de registro de eventos.
   */
  private static final Logger LOGGER  = LogManager.getLogger(RsaCrypt.class);

  /**
   * constructor.
   * */
  protected RsaCrypt() {
    //is empty for test.
  }

  /**
   * Método encrypt.
   * @param clearText publicKey
   * @param publicKey as RSAPublicKey
   * @throws MicroserviceException as MicroserviceException
   * @return byte[]
   */
  public static byte[] encrypt(final byte[] clearText, final RSAPublicKey publicKey)
          throws MicroserviceException {

    byte[] bufferResult;

    try {

      Cipher c = Cipher.getInstance(ALGORITHM);
      c.init(Cipher.ENCRYPT_MODE, publicKey);
      bufferResult = c.doFinal(clearText);

    } catch (Exception e) {
      LOGGER.error("Error en el metodo de Encriptado RSA --> ", e);
      Console.writeln(Console.Level.ERROR, e.getMessage());
      throw new MicroserviceException(CoreErrorServicesTable.INVALID_CRYPTED_DATA, e.getMessage(), "", true);
    }
    return bufferResult;
  }


}
