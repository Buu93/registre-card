/**
 * ****************************************************************
 * SecureString.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config.cipher;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Class SecureString.java.
 *
 * @author Jose A Rodriguez M
 */
public class SecureString {
  /** declaración constante key.
   * */
  private byte[] key;

  /**Declaración variable currVersion.
   * */
  private Long currVersion = 0L;

  /**
   * Declaración de la variable Map version versionKeysMap.
   * */
  private Map<Long, ByteRegister> versionKeysMap = new HashMap<>();

  /**
   * Constructor.
   * */
  public SecureString() {
//    is empty for test.
  }

  /**
   * Método SecureString.
   * @param k as byte[]
   * */
  public SecureString(final byte[] k) {
    this.key = Arrays.copyOf(k, k.length);
  }

  /**
   * método getKeyValue.
   * @return byte[]
   * */
  public byte[] getKeyValue() {
    return key;
  }

  /**
   * Método getKeyValue.
   * @param version type Long
   * @return byte[]
   */
  public byte[] getKeyValue(final Long version) {
    if (version.equals(0L)) {
      return key;
    }

    if (versionKeysMap.containsKey(version)) {
      return versionKeysMap.get(version).getBuffer();
    }

    return getKeyValue();
  }

  /**
   * Método getCurrentVersion.
   * @return Long
   * */
  public Long getCurrentVersion() {
    return currVersion;
  }

}
