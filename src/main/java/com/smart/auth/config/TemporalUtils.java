/**
 * ****************************************************************
 * Field.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.config;

import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.enumutils.Number;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class TemporalUtils.java.
 * @author Antonio Rodriguez.
 */
public final class TemporalUtils {

  /** *variable SECONDSINMILLI.  */
  public static final long SECONDSINMILLI = 1000;
  /** *variable MINUTESINMILLI.  */
  public static final long MINUTESINMILLI = SECONDSINMILLI * 60;
  /** *variable HOURSINMILLI.  */
  public static final long HOURSINMILLI = MINUTESINMILLI * 60;

  /**
   * Constructor emty.
   */
  protected TemporalUtils() {
    //empty
  }

  /**
   * Transform ocalendar to string.
   *
   * @param stFormat  String date format to return like dd/MM/yyyy
   * @param ocalendar Date to transform
   * @return String date representation, null if error occur.
   */
  public static synchronized String getDate(final String stFormat, final Calendar ocalendar) {
    String stTime = "";
    if (ocalendar == null) {
      return stTime;
    }
    try {
      SimpleDateFormat formatter = new SimpleDateFormat(stFormat);
      stTime = "" + formatter.format(ocalendar.getTime());
    } catch (Exception ex) {
      Console.writeException("", ex);
    }
    return stTime;
  }

  /**
   * get value second the diferent.
   * @param dtInicial as date.
   * @param dtFin as date.
   * @return int.
   */
  public static int getSecond(final Calendar dtInicial, final Calendar dtFin) {
    Date date = dtInicial.getTime();
    Date date2 = dtFin.getTime();
    int diferencia = (int) ((date2.getTime() - date.getTime()) / Number.CODE1000.value());

    int dias;
    int horas;

    if (diferencia > Number.CODE86400.value()) {
      dias = diferencia / Number.CODE86400.value();
      diferencia = diferencia - (dias * Number.CODE86400.value());
    }
    if (diferencia > Number.CODE3600.value()) {
      horas = diferencia / Number.CODE3600.value();
      diferencia = diferencia - (horas * Number.CODE3600.value());
    }

    return diferencia;
  }
}
