/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.controller.xml;


import com.smart.auth.exception.XmlException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.MicroserviceRequest;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.model.enumutils.Number;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Generado por.
 * @author Jose A Rodriguez M.
 */
public class MicoserviceXml {

  /**
   * Parameter de entry.
   */
  private String strXml;
  /**
   * Parameter pojo de entry.
   */
  private MicroserviceRequest info;

  /**
   * Construct empty.
   */
  public MicoserviceXml() {
    //empty.
  }

  /**
   * Inicializacion de datos.
   * @param is InputStream.
   * @param txnObjects TransactionObjectsDto.
   * @throws XmlException XmlException.
   */
  public MicoserviceXml(final InputStream is, final TransactionObjectsDto txnObjects) throws XmlException {
    buildXml(is, txnObjects);
  }

  /**
   * Init pojo MicroserviceRequest.
   * @param inf MicroserviceRequest.
   */
  public MicoserviceXml(final MicroserviceRequest inf) {
    info = inf;
  }

  /**
   * Generate Xml.
   * @param is InputStream.
   * @param txnObjects TransactionObjectsDto.
   * @throws XmlException XmlException.
   */
  private void buildXml(final InputStream is, final TransactionObjectsDto txnObjects) throws XmlException {
    try {
      byte[] buffer = new byte[Number.CODE1024.value()];
      ByteArrayOutputStream bout = new ByteArrayOutputStream();
      int readed;
      while ((readed = is.read(buffer, 0, buffer.length)) != -1) {
        bout.write(buffer, 0, readed);
      }
      bout.flush();
      byte[] bytes = bout.toByteArray();
      strXml = new String(bytes, Charset.forName("UTF-8"));

      if (strXml.isEmpty()) {
        throw new XmlException(XmlException.Error.EMPTY);
      }

      if (!strXml.contains("<arg0>")) {
        throw new XmlException(XmlException.Error.NOSBTFORMAT);
      }

      String msg = strXml.substring(strXml.indexOf("<arg0"),
            strXml.indexOf("</arg0") + "</arg0>".length());
      setInfo(MicroserviceRequest.createObject(msg, txnObjects.getIdOperation()));
      txnObjects.setGNPRequest(info);


    } catch (Exception ex) {
      Console.writeException(txnObjects.getIdOperation(), ex);
      throw new XmlException(XmlException.Error.NOREAD, ex.getMessage());
    }
  }


  /**
   * Method getType.
   * @return String.
   * @throws XmlException XmlException.
   */
  public String getType() throws XmlException {
    String type;

    if (info != null) {
      type = info.getTipoOperacion();
    } else {
      throw new XmlException(XmlException.Error.NOTYPE);
    }

    return type;
  }


  /**
   * param Xml.
   * @return strXml.
   */
  public String getStrXml() {
    return strXml;
  }

  /**
   * Method getInfo.
   * @return the info.
   */
  public MicroserviceRequest getInfo() {
    return info;
  }

  /**
   * Method setInfo.
   * @param info the info to set.
   */
  public void setInfo(final MicroserviceRequest info) {
    this.info = info;
  }

}
