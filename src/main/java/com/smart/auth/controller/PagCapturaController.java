package com.smart.auth.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

@Controller
public class PagCapturaController {

  @Value("${server.port}")
  String port;

  @RequestMapping(value = "/api/alta", method = RequestMethod.GET)
  public String AltaPage() {
    return "altaCuenta";
  }

  @RequestMapping({"/api/devolucion"})
  public String DevPage() {
    return "devolucion";
  }

  @RequestMapping({"/index"})
  public String index() {
    return "index";
  }

  @RequestMapping("/api/capturaCuenta")
  public void browse(HttpServletRequest request) throws UnknownHostException {


    InetAddress address = InetAddress.getLocalHost();
//    String url = ("https://" + address.getHostAddress() + ":" + port + "/api/alta");
    //pruebass
    String url = ("https://" + "localhost" + ":" + port + "/api/alta" + "?" + formarPeticion(request));

    if (Desktop.isDesktopSupported()) {
      Desktop desktop = Desktop.getDesktop();
      try {

        desktop.browse(new URI(url));
      } catch (IOException | URISyntaxException e) {
        e.printStackTrace();
      }
    } else {
      Runtime runtime = Runtime.getRuntime();
      try {
        runtime.exec("rundll32 url.dll,FileProtocolHandler " + url);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public String formarPeticion(HttpServletRequest request) {
    String cadenaPeticion = null;
    String token;
    String cliente;
    String referencia;

    token = request.getParameter("APP_TOKEN_SMART");
    cliente = request.getParameter("APP_ID_CLIENT");
    referencia = request.getParameter("APP_ID_TXN");

    cadenaPeticion = "APP_ID_TXN=" + referencia + "&" + "APP_ID_CLIENT=" + cliente + "&" + "APP_TOKEN_SMART=" + token;

    return cadenaPeticion;
  }

}