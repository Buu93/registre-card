package com.smart.auth.controller;

import com.smart.auth.model.MicroserviceRequest;
import com.smart.auth.model.dto.MicroserviceResponseDTO;
import com.smart.auth.model.dto.RespuestaGenerica;
import com.smart.auth.service.AppMicroservicioServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.HttpURLConnection;

/**
 * Clase AppMicroservice.java.
 * */
//@RestController
//@RequestMapping({"/api"})
//@Api(value = "onlinestore")//description = "Proceso de tramar GNP"
@Controller
public class MicroserviceController {

  /**
   * Instancia AppMicroservice.
   * */
  @Autowired
  private AppMicroservicioServices services;

  /**
   * Método processRequestNoThread.
   * @param dto as MicroserviceRequest
   * @param request as HttpServletRequest
   * @return MicroserviceResponseDTO
   * */
  @RequestMapping(value = "/api/doRequest", method = RequestMethod.POST)
//  @PostMapping(value = "/doRequest/")
//  @ApiOperation(value = "DOREQUEST", response = RespuestaGenerica.class)
//  @ApiResponses(value = {
//        @ApiResponse(code = HttpURLConnection.HTTP_OK,
//              message = "La solicitud ha tenido éxito."),
//        @ApiResponse(code = HttpURLConnection.HTTP_CONFLICT,
//              message = "Conflicto actual del servidor."),
//        @ApiResponse(code = HttpURLConnection.HTTP_UNAVAILABLE,
//              message = "El servidor no esta listo para manejar la peticion.")})
  public MicroserviceResponseDTO processRequestNoThread(@RequestBody final MicroserviceRequest dto,
                                                        final HttpServletRequest request) {
    return services.processRequest(dto, request);
  }
}
