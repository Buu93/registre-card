/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.dao;


import com.smart.auth.exception.SystemException;
import com.smart.auth.model.dto.PosDataDto;
import com.smart.auth.model.dto.StanDto;

/**
 * Interface SystemDao.
 *
 * @author Jose A Rodriguez M
 */
public interface SystemDao {

  /**
   * Método updatePosData.
   *
   * @param logonData   as PosDataDto
   * @param idOperation as String
   * @return boolean
   */
  boolean updatePosData(PosDataDto logonData, String idOperation);

  /**
   * Método createUpdate.
   *
   * @param logonData   as PosDataDto
   * @param idOperation as String
   * @return boolean
   */
  boolean createUpdate(PosDataDto logonData, String idOperation);

  /**
   * Método updatePosDataStatus.
   *
   * @param logonData   as PosDataDto
   * @param idOperation as String
   * @return boolean
   */
  boolean updatePosDataStatus(PosDataDto logonData, String idOperation);

  /**
   * Método updatePosDataUserP.
   *
   * @param logonData   as PosDataDto
   * @param idOperation as String
   * @return boolean
   */
  boolean updatePosDataUserP(PosDataDto logonData, String idOperation);

  /**
   * Método createUpdateStan.
   *
   * @param stanData as StanDto
   * @return boolean
   */
  boolean createUpdateStan(StanDto stanData);

  /**
   * Método findPosData.
   *
   * @param txnFilter   as   PosDataDto
   * @param idOperation as  String
   * @return PosDataDto
   * @throws SystemException error.
   */
  PosDataDto findPosData(PosDataDto txnFilter, String idOperation) throws SystemException;

  /**
   * Método findStan.
   *
   * @param txnFilter   as StanDto
   * @param idOperation as String
   * @return StanDto
   */
  StanDto findStan(StanDto txnFilter, String idOperation);

  /**
   * Método getStan.
   *
   * @param txnFilter   as StanDto
   * @param idOperation as String
   * @return Long
   */
//  Long getStan(StanDto txnFilter, String idOperation);
}
