/**
 * -------------------------------------------------------------------
 * SystemDaoImpl.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.dao;


import com.smart.auth.config.DaoUtil;
import com.smart.auth.config.TemporalUtils;
import com.smart.auth.exception.SystemException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.constants.SqlConsulta;
import com.smart.auth.model.dto.PosDataDto;
import com.smart.auth.model.dto.StanDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.Calendar;


/**
 * Class SystemDaoImpl.java.
 *
 * @author Jose A Rodriguez M
 */
@Repository
public class SystemDaoImpl implements SystemDao {

  /**
   * Instancia jdbcTemplate.
   * */
  private JdbcTemplate jdbcTemplate;

  /**
   * Setter jdbcTemplate.
   * @param jdbcTemplate as JdbcTemplate
   * */
  @Autowired
  public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  /**
   * Método updatePosData.
   * @param posData as PosDataDto
   * @param idOperation as String
   * @return boolean
   * */
  @Override
  public boolean updatePosData(final PosDataDto posData, final String idOperation) {
    boolean boolRes = false;
    try {
      Object[] params = new Object[]{posData.getLastServerKey(), posData.getTimeOut(),
            posData.getStatus(), TemporalUtils.getDate("dd/MM/yyyy", Calendar.getInstance()),
            posData.getId()};
      int[] types = new int[]{Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR,
        Types.BIGINT};

      if (posData.getId() != 0L) {
        jdbcTemplate.update(SqlConsulta.POS_UPDATE, params, types);
      }
      boolRes = true;
    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
    }
    return boolRes;
  }

  /**
   * Método createUpdate.
   * @param posData as PosdataDto
   * @param idOperation as String
   * @return boolean
   * */
  @Override
  public boolean createUpdate(final PosDataDto posData, final String idOperation) {
    boolean boolRes = false;
    Object[] params = new Object[]{posData.getSerialPos(), posData.getClientId(),
          posData.getUserName(), posData.getUserP(), posData.getLastServerKey(),
          posData.getTimeOut(), posData.getStatus(), posData.getLastDate(),
          posData.getStatusData()};
    int[] types = new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
        Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER};
    try {
      if (posData.getId() != 0L) {
        params = new Object[]{posData.getUserName(), posData.getUserP(), posData.getTimeOut(),
              posData.getStatus(), posData.getLastDate(), posData.getStatusData(), posData.getId()};
        types = new int[]{Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER,
          Types.VARCHAR, Types.INTEGER, Types.BIGINT};
        jdbcTemplate.update(SqlConsulta.POS_UPDATE_ALL, params, types);

      } else {
        Long id = DaoUtil.createAndGetId(jdbcTemplate, SqlConsulta.POS_INSERT, params, types);
        posData.setId(id);
      }
      boolRes = true;
    } catch (Exception ex) {
      Console.writeException("Error BD Update: ", ex);
      boolRes = false;
    }
    return boolRes;
  }

  /**
   * Método updatePosDataStatus.
   * @param posData as PosDataDto
   * @param idOperation as String
   * @return boolean
   * */
  @Override
  public boolean updatePosDataStatus(final PosDataDto posData, final String idOperation) {
    boolean boolRes = false;
    try {
      Object[] params = new Object[]{posData.getStatus(), posData.getId()};
      int[] types = new int[]{Types.INTEGER, Types.BIGINT};

      if (posData.getId() != 0L) {
        jdbcTemplate.update(SqlConsulta.POS_UPDATE_STATUS, params, types);
      }
      boolRes = true;
    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
    }
    return boolRes;
  }

  /**
   * Método updatePosDataUserP.
   * @param posData as PosDataDto
   * @param idOperation as String
   * @return boolean
   * */
  @Override
  public boolean updatePosDataUserP(final PosDataDto posData, final String idOperation) {
    boolean boolRes = false;
    try {
      Object[] params = new Object[]{posData.getStatusData(), posData.getUserP(), posData.getId()};
      int[] types = new int[]{Types.INTEGER, Types.VARCHAR, Types.BIGINT};

      if (posData.getId() != 0L) {
        jdbcTemplate.update(SqlConsulta.POS_UPDATE_USER_P, params, types);
      }
      boolRes = true;
    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
    }
    return boolRes;
  }

  /**
   * Método createUpdateStan.
   * @param stanData as StanDto
   * @return boolean
   * */
  @Override
  public boolean createUpdateStan(final StanDto stanData) {
    boolean boolRes = false;
    String idOperation = "createUpdateStan";
    try {
      Object[] params = new Object[]{stanData.getSerialPos(), stanData.getNumStan()};
      int[] types = new int[]{Types.VARCHAR, Types.BIGINT};

      if (stanData.getId() != 0L) {
        params = new Object[]{stanData.getNumStan(), stanData.getId()};
        types = new int[]{Types.BIGINT, Types.BIGINT};
        jdbcTemplate.update(SqlConsulta.STAN_UPDATE, params, types);
      } else {
        Long id = DaoUtil.createAndGetId(jdbcTemplate, SqlConsulta.STAN_INSERT, params, types);
        stanData.setId(id);
      }
      boolRes = true;
    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
    }
    return boolRes;
  }

  /**
   * Método findPosData.
   * @param txnFilter as posDataDto.
   * @param idOperation as String
   * @return PosDataDto
   * */
  @Override
  public PosDataDto findPosData(final PosDataDto txnFilter, final String idOperation) throws SystemException {
    PosDataDto posRet = new PosDataDto();
    try {
      posRet = (PosDataDto) jdbcTemplate.queryForObject(SqlConsulta.POS_SELECT,
            new Object[]{txnFilter.getClientId()},
            new BeanPropertyRowMapper(PosDataDto.class));
    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
    }

    return posRet;

  }

  /**
   * Método findStan.
   * @param txnFilter as StanDto
   * @param idOperation as String
   * @return StanDto
   * */
  @Override
  public StanDto findStan(final StanDto txnFilter, final String idOperation) {
    String key = txnFilter.getSerialPos();
    StanDto posRet = null;
    try {
      posRet = (StanDto) jdbcTemplate.queryForObject(SqlConsulta.STAN_SELECT,
            new Object[]{txnFilter.getSerialPos()},
            new BeanPropertyRowMapper(StanDto.class));

    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
    }
    if (posRet == null) {
      posRet = new StanDto(key);
      posRet.setNumStan(0L);
    }
    posRet.incrementStan();
    // Agrega o actualiza registro de stan por punto de venta
    createUpdateStan(posRet);
    return posRet;
  }

  /**
   * Método getStan.
   * @param txnFilter as StandDto
   * @param idOperation as String
   * @return Long
   * */
//  @Override
//  public Long getStan(final StanDto txnFilter, final String idOperation) {
//    StanDto posRet = findStan(txnFilter, idOperation);
//    return posRet.getNumStan();
//  }


}
