package com.smart.auth.dao;


import com.smart.auth.model.AppUser;
import java.util.List;

public interface AuthenticateService {
  List<AppUser> findByUsername(String cliente);
}
