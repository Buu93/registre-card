package com.smart.auth.dao;


import com.smart.auth.exception.DbException;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.model.dto.TxnByDayDto;

/**
 * Interface TxnByDayDao.
 *
 * @author Jose A Rodriguez M
 */
public interface TxnByDayDao {

  /**
   * Método createUpdate.
   *
   * @param dto as  TransactionObjectsDto
   * @return boolean
   * @throws DbException error.
   */
  boolean createUpdate(TransactionObjectsDto dto) throws DbException;

  /**
   * Método findByHash.
   *
   * @param txnFilter as TxnByDayDto
   * @return TxnByDayDto
   */
  TxnByDayDto findByHash(TxnByDayDto txnFilter);
}
