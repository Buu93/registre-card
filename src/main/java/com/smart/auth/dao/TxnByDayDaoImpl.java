/**
 * --------------------------------------------------------------------
 * TxnByDayDaoImpl.java
 *
 * @author : Carlos A Lopez P
 * @creationDate : 12/03/2019
 * @specFile:
 * @revisedBy : Carlos A Lopez P
 * @date : 12/03/2019
 */

package com.smart.auth.dao;


import com.smart.auth.config.DaoUtil;
import com.smart.auth.exception.DbException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.message.CoreErrorServicesTable;
import com.smart.auth.exception.message.MessageRespose;
import com.smart.auth.model.constants.SqlConsulta;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.model.dto.TxnByDayDto;
import com.smart.auth.service.error.IErrorServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;


/**
 * Class TxnByDayDaoImpl.java.
 *
 */
@Repository
public class TxnByDayDaoImpl implements TxnByDayDao {

  /**
   * Instancia Jdbctemplate.
   */
  private JdbcTemplate jdbcTemplate;

  /**variable update.*/
  private boolean isUpdate = false;

  /**
   * Setter Jdbctemplate.
   *
   * @param jdbcTemplate as JdbcTemplate
   */
  @Autowired
  public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  /**
   * Método createUpdate.
   *
   * @param dto as TransactionObjectsDto
   * @return boolean
   */
  @Override
  public boolean createUpdate(final TransactionObjectsDto dto) throws DbException {
    boolean boolRes = false;
    TxnByDayDto txn = dto.getTxnByDay();
    String idOperation = dto.getIdOperation();

    try {
      Console.writeln(Console.Level.INFO, idOperation, "Entra a guardar=" + txn);
      Object[] params = new Object[]{txn.getTxnHash(), txn.getType(), txn.getTxnOrig(),
            txn.getProcessingCode(), txn.getTypeReverse(),
            txn.getAmount(), txn.getResponseCode(), txn.getResponseMessage(), txn.getStatus(),
            txn.getIdOperation(), txn.getPosSerial(),
            txn.getTsn(), txn.gettResp(), txn.getTxnReference(),
            new java.sql.Date(txn.getTxnDate().getTime()),
            txn.getReference(), txn.getApprovalCode(),
            txn.getRefSPNum(), txn.getExpDate(), txn.getTxnReferenceDataHash()};
      int[] types = new int[]{Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR,
            Types.VARCHAR, Types.INTEGER,
            Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
            Types.VARCHAR, Types.INTEGER, Types.INTEGER,
            Types.TIMESTAMP,
            Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};

      if (txn.getId() != 0L) {
        isUpdate = true;
        params = new Object[]{txn.getTxnHash(), txn.getType(), txn.getTxnOrig(),
              txn.getProcessingCode(), txn.getTypeReverse(),
              txn.getAmount(), txn.getResponseCode(), txn.getResponseMessage(), txn.getStatus(),
              txn.getIdOperation(), txn.getPosSerial(),
              txn.getTsn(), txn.gettResp(), txn.getTxnReference(),
              new java.sql.Date(txn.getTxnDate().getTime()),
              txn.getReference(), txn.getApprovalCode(),
              txn.getRefSPNum(), txn.getExpDate(), txn.getTxnReferenceDataHash(), txn.getId()};
        types = new int[]{Types.VARCHAR, Types.INTEGER, Types.INTEGER, Types.VARCHAR,
              Types.VARCHAR, Types.INTEGER,
              Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
              Types.VARCHAR, Types.INTEGER, Types.INTEGER,
              Types.TIMESTAMP,
              Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
              Types.BIGINT};

        jdbcTemplate.update(SqlConsulta.TXXBYDAY_UPDATE, params, types);

      } else {
        Long id = DaoUtil.createAndGetId(jdbcTemplate, SqlConsulta.TXXBYDAY_INSERT, params, types);
        txn.setId(id);
      }
      boolRes = true;
    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
      IErrorServices error = new MessageRespose(ex.getMessage(), "96");
      if (isUpdate) {
        error = CoreErrorServicesTable.DB_INSERT_ERROR;
      }
      throw new DbException(error, idOperation);
    }
    return boolRes;
  }

  /**
   * Método findByHash.
   *
   * @param txnFilter as TxnByDayDto
   * @return TxnByDayDto
   */
  @Override
  public TxnByDayDto findByHash(final TxnByDayDto txnFilter) {
    TxnByDayDto tbd = null;
    try {
      tbd = (TxnByDayDto) jdbcTemplate.queryForObject(SqlConsulta.TXXBYDAY_SELECT,
            new Object[]{txnFilter.getTxnHash(), txnFilter.getTxnReferenceDataHash()},
            new BeanPropertyRowMapper(TxnByDayDto.class));

    } catch (Exception ex) {
      Console.writeException("1", ex);
    }
    return tbd;
  }

}

//<editor-fold defaultstate="collapsed" desc="Modifications comments">
/**
 * **********************************************************
 *
 * @updateDate: dd/MM/yyyy
 * @author: ?
 * @revisedBy : ?
 * @date : dd/MM/yyyy
 * @Description: (Write text here)
 * ************************************************************ * * *
 * ************************************************************
 */
//</editor-fold>

