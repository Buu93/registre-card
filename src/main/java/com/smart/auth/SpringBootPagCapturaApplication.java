package com.smart.auth;

import com.smart.auth.config.BeanUtil;
import com.smart.auth.config.InitApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.servlet.ServletContextListener;

@SpringBootApplication/*(scanBasePackages={"com.smart.auth"})*/
public class SpringBootPagCapturaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPagCapturaApplication.class, args);
	}

  @Autowired
  public void context(final ApplicationContext context) {
    BeanUtil.setApplicationContext(context);
  }

  @Bean
  public ServletListenerRegistrationBean<ServletContextListener> listenerRegistrationBean() {
    ServletListenerRegistrationBean<ServletContextListener> bean =
          new ServletListenerRegistrationBean<>();
    bean.setListener(new InitApplication());
    return bean;
  }
}