package com.smart.auth.service;


import com.smart.auth.dao.AuthenticateService;
import com.smart.auth.model.AppUser;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.util.List;
import java.util.UUID;

@Service
public class TokenService {

    @Autowired
    AuthenticateService authservice;

    private static final Logger logger = LoggerFactory.getLogger(TokenService.class);
    private static final Cache restApiAuthTokenCache = CacheManager.getInstance().getCache("restApiAuthTokenCache");
    public static final int HALF_AN_HOUR_IN_MILLISECONDS = 30 * 60 * 1000;

    @Scheduled(fixedRate = HALF_AN_HOUR_IN_MILLISECONDS)
    public void evictExpiredTokens() {
        logger.info("Evicting expired tokens");
        restApiAuthTokenCache.evictExpiredElements();
    }

    public String generateNewToken() {
        return UUID.randomUUID().toString();
    }

    public void store(String token, Authentication authentication) {
        restApiAuthTokenCache.put(new Element(token, authentication));
    }

    public boolean contains(String token) {

        String[] parts = token.split("\\|");
        boolean valida = false;

        String stoken = parts[0];
        String cliente = parts[1];
        String referencia = parts[2];
        String pagina = parts[3].toUpperCase();

        List<AppUser> users = authservice.findByUsername(cliente);
        for(AppUser appUser: users) {
            if(appUser.getClienteid().equals(cliente) && appUser.getPagina().equals(pagina) ) {
                String data = appUser.getUsername() + "|" + appUser.getPassword() + "|" + referencia;
                System.out.println("cadena armada:::: " + data);
                String hashtoken = sha256(data);
                System.out.println("toekn:::: " + hashtoken);

               if(hashtoken.equals(stoken)) {
                   valida = true;
                   break;
               }
            }
        }
        return valida;
    }

    public Authentication retrieve(String token) {
        return (Authentication) restApiAuthTokenCache.get(token).getObjectValue();
    }

    private String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
