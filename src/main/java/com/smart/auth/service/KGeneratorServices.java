/**
 * ****************************************************************
 * KGeneratorServices.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.service;


import com.smart.auth.config.cipher.SecureString;

/**
 * Class KGeneratorServices.java.
 * @author Jose A Rodriguez M.
 */
public interface KGeneratorServices {
  /**
   * Method getKey.
   * @param keyName String.
   * @return SecureString.
   */
  SecureString getKey(String keyName);

  /**
   * Method getKey.
   * @param keyName String.
   * @param idEnterprise Long.
   * @return SecureString.
   */
  SecureString getKey(String keyName, Long idEnterprise);

  /**
   * Method containsKey.
   * @param keyName String.
   * @return boolean.
   */
  boolean containsKey(String keyName);
}
