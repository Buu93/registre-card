package com.smart.auth.service.error;


import com.smart.auth.model.enumutils.ErrorType;

/**
 * Class IErrorServices.java.
 */
public interface IErrorServices {
  /**
   * Method getErrorCode.
   * @return String.
   */
  String getErrorCode();

  /**
   * Method getErrorMessage.
   * @return String.
   */
  String getErrorMessage();

  /**
   * Method getErrorType.
   * @return ErrorType.
   */
  ErrorType getErrorType();
}
