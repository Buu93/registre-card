package com.smart.auth.service;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import com.smart.auth.dao.AuthenticateService;
import com.smart.auth.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;



@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private AuthenticateService userDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<AppUser> users = userDao.findByUsername(username);
		AppUser user = obten(users,username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}
	
	public AppUser obten(List<AppUser> users, String cliente) {
		for(AppUser appUser: users) {
			if(appUser.getClienteid().equals(cliente) ) {
				System.out.println("toekn:::: " + appUser.getUsername());
				return appUser;
			}
		}
		return null;
	}



}