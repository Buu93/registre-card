package com.smart.auth.service;


import com.smart.auth.exception.CodeMessageException;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.service.impl.operation.OperationResponse;

/**
 * Class RequestServices.java.
 */
public interface RequestServices {
  /**
   *
   * @param txnObjects TransactionObjectsDto.
   * @param idOperation String.
   * @return OperationResponse.
   * @throws CodeMessageException Exception.
   */
  OperationResponse processRequest(TransactionObjectsDto txnObjects, String idOperation)
        throws CodeMessageException;
}
