/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.service.clientmessage;

import com.smart.auth.config.HashUtils;
import com.smart.auth.config.TripleDes;
import com.smart.auth.config.cipher.RsaCrypt;
import com.smart.auth.exception.MicroserviceException;
import com.smart.auth.model.constants.ProcessingCode;
import com.smart.auth.service.impl.error.Management;
import com.smart.auth.service.impl.error.SystemSettings;
import com.smart.auth.model.enumutils.Number;
import com.smart.auth.exception.logs.Console;
import org.apache.commons.net.util.Base64;

import java.io.*;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Clase XmlRequest.java.
 *
 * @author Iván Israel Uresti Adame - Smart BT ,S.A. de C.V.
 */
public class XmlRequest {
  /**
   * Declaración de variable type.
   */
  private String type;
  /**
   * Declaración de variable stan.
   */
  private String stan;
  /**
   * Declaración de variable deviceTime.
   */
  private String deviceTime;
  /**
   * Declaración de variable refClientNum.
   */
  private String refClientNum;
  /**
   * Declaración de variable RefSPNum.
   */
  private String refSPNum;
  /**
   * Declaración de variable autCode.
   */
  private String autCode;
  /**
   * Declaración de variable serialPos.
   */
  private String serialPos;
  /**
   * Declaración de variable dataTextMap.
   */
  private Map<String, String> dataTextMap;
  /**
   * Declaración de variable cipherTextMap.
   */
  private Map<String, String> cipherTextMap;
  /**
   * Declaración de variable customMap.
   */
  private Map<String, String> customMap;
  /**
   * Declaración de variable xmlString.
   */
  private String xmlString;
  /**
   * Declaración de variable emv.
   */
  private byte[] emv;
  /**
   * Declaración de variable lastServerKey.
   */
  private String lastServerKey = "";
  /**
   * Declaración de variable dataCipher.
   */
  private String dataCipher = "";
  /**
   * Declaración de variable dataText.
   */
  private String dataText = "";
  /**
   * Declaración de variable custonKey.
   */
  private String custonKey = "";
  /**
   * Declaración de variable custonValue.
   */
  private String custonValue = "";
  /**
   * Declaración de variable dataEmv.
   */
  private String dataEmv = "";

  /**
   * Constructor XmlRequest.
   */
  public XmlRequest() {
    dataTextMap = new HashMap<>();
    cipherTextMap = new HashMap<>();
    customMap = new HashMap<>();
  }

  /**
   * Constructor XmlRequest.
   *
   * @param haMap as HashMap.
   */
  public XmlRequest(final Map haMap) {
    cipherTextMap = haMap;
  }

  /**
   * XmlRequest.
   *
   * @param map    as map.
   * @param tsByte byte.
   * @param divce  as String.
   * @param stStan as String.
   * @param key    as String.
   */
  public XmlRequest(final Map map, final byte[] tsByte, final String divce,
                    final String stStan, final String key) {
    customMap = map;
    emv = tsByte;
    deviceTime = divce;
    stan = stStan;
    lastServerKey = key;
  }

  /**
   * Método encode.
   *
   * @return String.
   */
  public String encode() {
    StringBuilder response = new StringBuilder();
    Calendar now = Calendar.getInstance();

    deviceTime = String.format("%1$td%1$tm%1$tY%1$tH%1$tM%1$tS", now);

    response.append("<?xml version=\"1.0\" encoding=\"").append(SystemSettings.getCHARSET().name()).append("\"?>");
    response.append("<sbt-ws-message version=\"1.3\">");
    response.append("<header>");
    response.append("<DeviceTime>").append(deviceTime).append("</DeviceTime>");
    response.append("<Type>").append(type).append("</Type>");
    response.append("<ClientID>").append(SystemSettings.getString("sp.clientId", "")).append("</ClientID>");
    response.append("<SerialPos>").append(getSerialPos()).append("</SerialPos>");
    response.append("<Stan>").append(stan).append("</Stan>");
    response.append("</header>");
    response.append("<message>");

    response = responseAppen(response);

    try {
      addDataCipherTag(response); // encryptacion llave pública
      addDataTextTag(response);
      addCustomTags(response);
      addEmv(response);
      response.append("</message>");
      response.append("</sbt-ws-message>");
      xmlString = response.toString();
    } catch (MicroserviceException e) {
      Console.writeException(e);
    } catch (NoSuchAlgorithmException e) {
      Console.writeException(e);
    } catch (IOException e) {
      Console.writeException(e);
    } catch (InvalidKeySpecException e) {
      Console.writeException(e);
    }

    return xmlString;
  }
  
  /**
   * responseAppen provocada por sonar.
   * @param builder as StringBuilder
   * @return StringBuilder
   */
  public StringBuilder responseAppen(final StringBuilder builder) {
    StringBuilder response = builder;

    if (refClientNum != null && !refClientNum.isEmpty()) {
      response.append("<RefClientNum>").append(refClientNum).append("</RefClientNum>");
    }

    if (refSPNum != null && !refSPNum.isEmpty()) {
      response.append("<RefSPNum>").append(refSPNum).append("</RefSPNum>");
    }

    if (autCode != null && !autCode.isEmpty()) {
      response.append("<Aut-Code>").append(autCode).append("</Aut-Code>");
    }

    return response;
  }

  /**
   * Método getType.
   *
   * @return String.
   */
  public String getType() {
    return type;
  }

  /**
   * Método setType.
   *
   * @param type as String
   */
  public void setType(final String type) {
    this.type = type;
  }

  /**
   * Método getStan.
   *
   * @return String
   */
  public String getStan() {
    return stan;
  }

  /**
   * Método setStan.
   *
   * @param stan as String
   */
  public void setStan(final String stan) {
    this.stan = stan;
  }

  /**
   * Método getDeviceTime.
   *
   * @return String
   */
  public String getDeviceTime() {
    return deviceTime;
  }

  /**
   * Método getRefClientNum.
   *
   * @return String
   */
  public String getRefClientNum() {
    return refClientNum;
  }

  /**
   * Método setRefClientNum.
   *
   * @param refClientNum as String
   */
  public void setRefClientNum(final String refClientNum) {
    this.refClientNum = refClientNum;
  }

  /**
   * Método getRefSPNum.
   *
   * @return String
   */
  public String getRefSPNum() {
    return refSPNum;
  }

  /**
   * Método setRefSPNum.
   *
   * @param refSPNum as String
   */
  public void setRefSPNum(final String refSPNum) {
    this.refSPNum = refSPNum;
  }

  /**
   * Método getDataCipher.
   *
   * @return String
   */
  public String getDataCipher() {
    return dataCipher;
  }

  /**
   * Método setDataCipher.
   *
   * @param dataCipher as String
   */
  public void setDataCipher(final String dataCipher) {
    this.dataCipher = dataCipher;
  }

  /**
   * Método getDataText.
   *
   * @return String
   */
  public String getDataText() {
    return dataText;
  }

  /**
   * Método setDataText.
   *
   * @param dataText as String
   */
  public void setDataText(final String dataText) {
    this.dataText = dataText;
  }

  /**
   * Getter getCustonKey.
   *
   * @return String.
   */
  public String getCustonKey() {
    return custonKey;
  }

  /**
   * Setter setCustonKey.
   *
   * @param custonKey as String
   */
  public void setCustonKey(final String custonKey) {
    this.custonKey = custonKey;
  }

  /**
   * Getter getCustonValue.
   *
   * @return String
   */
  public String getCustonValue() {
    return custonValue;
  }

  /**
   * Setter setCustonValue.
   *
   * @param custonValue as String
   */
  public void setCustonValue(final String custonValue) {
    this.custonValue = custonValue;
  }

  /**
   * Getter getDataEmv.
   *
   * @return String
   */
  public String getDataEmv() {
    return dataEmv;
  }

  /**
   * Setter setDataEmv.
   *
   * @param dataEmv as String
   */
  public void setDataEmv(final String dataEmv) {
    this.dataEmv = dataEmv;
  }

  /**
   * Método addDataTextParam.
   *
   * @param param as String
   * @param value as String
   */
  public void addDataTextParam(final String param, final String value) {
    if (value.length() > 0) {
      dataTextMap.put(param, value);
    }
  }

  /**
   * Método addCipherTextParam.
   *
   * @param param as String
   * @param value as String
   */
  public void addCipherTextParam(final String param, final String value) {
    if (value.length() > 0) {
      cipherTextMap.put(param, value);
    }
  }

  /**
   * Método getCipherTextParam.
   *
   * @param param as String
   * @return String
   */
  public String getCipherTextParam(final String param) {
    String sret = "";
    if (cipherTextMap.containsKey(param)) {
      sret = cipherTextMap.get(param);
    }
    return sret;
  }

  /**
   * Método getCipherTextParam.
   *
   * Para limpiar los datos que se mandan CDATA
   *
   */
  public void cleanCipherTextParam() {
    cipherTextMap.clear();

  }


  /**
   * Método addDataCipherTag.
   *
   * @param response as StringBuilder
   * @throws MicroserviceException error
   * @throws NoSuchAlgorithmException error
   * @throws IOException error
   * @throws InvalidKeySpecException error
   */
  private void addDataCipherTag(final StringBuilder response)
        throws MicroserviceException, NoSuchAlgorithmException, IOException, InvalidKeySpecException {
    String paramsList = getDataCipherString();

    if (paramsList != null) {
      dataCipher = paramsList;
      response.append("<DataCipher><![CDATA[").append(paramsList).append("]]></DataCipher>");
    }
  }

  /**
   * Método getDataCipherString.
   *
   * @return String
   * @throws MicroserviceException error
   * @throws NoSuchAlgorithmException error
   * @throws IOException error
   * @throws InvalidKeySpecException error
   */
  protected String getDataCipherString() throws MicroserviceException, NoSuchAlgorithmException,
        IOException, InvalidKeySpecException {
    String paramList = null;

    if (cipherTextMap != null && !cipherTextMap.isEmpty()) {
      String fieldValue = mapToStringList(cipherTextMap);
      Base64 base64 = new Base64();
      byte[] cipherData;

      if (type.equals(ProcessingCode.LOGON)) {
        // Es autenticación se cifra con llave pública
        RSAPublicKey publicKey = loadPublicKey();
        cipherData = RsaCrypt.encrypt(fieldValue.getBytes(SystemSettings.getCHARSET()), publicKey);
      } else {
        // Si no es autenticación se cifra en 3DES
        String key = buildKey();

        cipherData = TripleDes.stEncodeData(fieldValue.getBytes(SystemSettings.getCHARSET()), key,
              (byte) Number.CODE0XFF.value());
      }

      cipherData = base64.encode(cipherData);
      paramList = new String(cipherData, SystemSettings.getCHARSET());
    }

    return paramList;
  }

  
  /**
   * Map to string list.
   *
   * @param map the map
   * @return the string
   */
  protected String mapToStringList(final Map<String, String> map) {
    StringBuilder result = new StringBuilder();

    for (Map.Entry entry : map.entrySet()) {
      if (result.length() > 0) {
        result.append("\r\n");
      }
      result.append(entry.getKey()).append("=").append(entry.getValue());
    }

    return result.toString();
  }

  /**
   * Método buildKey.
   *
   * @return String
   */
  public String buildKey() {
    StringBuilder key = new StringBuilder();
    String serverKey = lastServerKey;

    String sha1Part = HashUtils.sha1(deviceTime + "|" + stan);

    key.append(serverKey.substring(0, Number.CODE12.value()));
    key.append(sha1Part.substring(0, Number.CODE12.value()));
    key.append(serverKey.substring(Number.CODE24.value()));

    return key.toString();
  }

  /**
   * Método loadPublicKey.
   *
   * @return RSAPublicKey.
   * @throws IOException              error
   * @throws NoSuchAlgorithmException as error.
   * @throws InvalidKeySpecException  as error.
   */
  private static RSAPublicKey loadPublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
    StringBuilder key = new StringBuilder();
    Base64 base64 = new Base64();
    String publicKeyPem = Management.getPathFiles() + File.separator + "key_pub.pem";

    RSAPublicKey publicKey;

    File privKeyFile = new File(publicKeyPem);
    try (InputStream is = new FileInputStream(privKeyFile)) {
      try (InputStreamReader isReader = new InputStreamReader(is)) {
        try (BufferedReader reader = new BufferedReader(isReader)) {
          key = cicloLoadPublicKey(reader, key);
        }
      }
    }
    byte[] keyEncoded = base64.decode(key.toString());
    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    KeySpec ks = new X509EncodedKeySpec(keyEncoded);
    publicKey = (RSAPublicKey) keyFactory.generatePublic(ks);

    return publicKey;
  }

  /**
   * ciclo ejecutado dentro del método loadPublicKey.
   *
   * @param reader as BufferedReader
   * @param key    as StringBuilder
   * @return StringBuilder
   * @throws IOException as error
   */
  public static StringBuilder cicloLoadPublicKey(final BufferedReader reader,
                              final StringBuilder key) throws IOException {

    String line;
    while ((line = reader.readLine()) != null) {
      if (!line.startsWith("-----")) {
        key.append(line.trim());
      }
    }
    return key;
  }

  /**
   * Método addDataTextTag.
   *
   * @param response as StringBuilder
   */
  private void addDataTextTag(final StringBuilder response) {
    if (dataTextMap != null && !dataTextMap.isEmpty()) {
      String fieldValue = mapToStringList(dataTextMap);
      dataText = fieldValue;
      response.append("<DataText><![CDATA[").append(fieldValue).append("]]></DataText>");
    }
  }

  /**
   * Método addCustomTags.
   *
   * @param response as StringBuilder
   */
  public void addCustomTags(final StringBuilder response) {
    for (Map.Entry<String, String> custom : customMap.entrySet()) {
      custonKey = custom.getKey();
      custonValue = custom.getValue();
      response.append("<").append(custom.getKey()).append(">").append(custom.getValue()).append("</")
            .append(custom.getKey()).append(">");
    }
  }

  /**
   * Método addEmv.
   *
   * @param response as StringBuilder
   */
  public void addEmv(final StringBuilder response) {
    if (emv != null) {
      String key = buildKey();
      Base64 base64 = new Base64();
      byte[] fixedEmv = TripleDes.stEncodeData(emv, key, (byte) Number.CODE0XFF.value());
      fixedEmv = base64.encode(fixedEmv);
      dataEmv = new String(fixedEmv, Charset.defaultCharset());
      response.append("<DataEMV><![CDATA[").append(dataEmv).append("]]></DataEMV>");
    }
  }

  /**
   * GetSerialPos.
   *
   * @return the serialPos
   */
  public String getSerialPos() {
    return serialPos;
  }

  /**
   * SetSerialPos.
   *
   * @param serialPos the serialPos to set
   */
  public void setSerialPos(final String serialPos) {
    this.serialPos = serialPos;
  }

  /**
   * SetLastServerKey.
   *
   * @param lastServerKey the lastServerKey to set
   */
  public void setLastServerKey(final String lastServerKey) {
    this.lastServerKey = lastServerKey;
  }

  /**
   * SetAutCode.
   *
   * @param autCode the autCode to set
   */
  public void setAutCode(final String autCode) {
    this.autCode = autCode;
  }
}
