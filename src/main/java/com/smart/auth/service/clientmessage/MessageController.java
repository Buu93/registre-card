/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.service.clientmessage;

import com.smart.auth.config.TemporalUtils;
import com.smart.auth.controller.core.MicroserviceSendHttp;
import com.smart.auth.exception.CommonOperationException;
import com.smart.auth.exception.GeneralException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.message.CoreErrorServicesTable;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.service.impl.error.SystemSettings;
import com.smart.auth.model.enumutils.Number;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Clase MessageController.java.
 *
 * @author Jose A Rodriguez M
 */
public class MessageController {

  /**
   * Declaración de la variable idOperation.
   * */
  private String idOperation;
  /**
   * Declaración de variable HOSTPRI.
   * */
  private static final String HOSTPRI = "pri";
  /**
   * Declaración de vaiable bolContinue.
   * */
  private boolean bolContinue = false;

  /**
   * constructor.
   * @param idOperation as String
   * */
  public MessageController(final String idOperation) {
    this.idOperation = idOperation;
  }

  /**
   * Método getChannel.
   * @param timeOut as int
   * @param prefix as String
   * @param conf as TransactionObjectsDto
   * @return MicroserviceSendHttp
   **/
  public MicroserviceSendHttp getChannel(final int timeOut, final String prefix, final TransactionObjectsDto conf) {
    Calendar fecha = new GregorianCalendar();
    int timeout1 = timeOut;
    if (timeout1 == 0) {
      timeout1 = SystemSettings.getInt("ws.sp.timeout." + prefix, Number.CODE40.value());
    }
    MicroserviceSendHttp sgComm = new MicroserviceSendHttp(idOperation);
    sgComm.setToSend("Plataforma");
    sgComm.setDomain(SystemSettings.getString("ws.sp.url." + prefix));
    sgComm.addHeader("Content-Type", SystemSettings.getString("ws.sp.contentType." + prefix));
    sgComm.addHeader("Connection", "Keep-Alive");

    bolContinue = sgComm.initHttp(SystemSettings.getAsBoolean("ws.sp.url.https." + prefix, false),
          timeout1, timeout1);

    if (bolContinue) {
      String usedUrl = "Primaria";

      usedUrl = validUri(usedUrl, prefix);
      Console.writeln(Console.Level.DEBUG, conf.getIdOperation(),
            "*** Conectando url ==>" + usedUrl);
      return sgComm;
    }
    // si no hay conexion reconecta a url secundaria.
    if (prefix != null && prefix.equals(HOSTPRI)) {
      Console.writeln(Console.Level.DEBUG, conf.getIdOperation(),
            "*** Intento de conexion url ==> [Primaria] con valor a [" + timeout1
                  + "] segundos de espera: ["
                  + TemporalUtils.getSecond(fecha, new GregorianCalendar()) + "]");
      return getChannel(0, "sec", conf);
    }
    return null;

  }

  /**
   * validacion de tipo de Uri.
   * @param url as String
   * @param prefix as String
   * @return String
   */
  public String validUri(final String url, final String prefix) {
    String usedUrl = url;
    if (prefix != null && !prefix.equals(HOSTPRI)) {
      usedUrl = "Secundaria";
    }

    return usedUrl;
  }

  /**
   * Método sendAndWaitForResponse.
   * @param conf as TransactionObjectsDto
   * @return XmlResponse
   * @throws GeneralException error.
   */
  public XmlResponse sendAndWaitForResponse(final TransactionObjectsDto conf)
        throws GeneralException {
    XmlRequest request = conf.getRequestWS();
    MicroserviceSendHttp sgComm = getChannel(conf.getTimeout(), HOSTPRI, conf);
    if (sgComm == null) {
      throw new GeneralException(CoreErrorServicesTable.NOT_COMM_PROVIDER);
    }


    XmlResponse response = new XmlResponse();
    try {
      response = sendMsg(request.encode(), conf, sgComm);
    } catch (CommonOperationException e) {
      Console.writeException(idOperation, e);
    }

    conf.setResponseWS(response);
    return response;
  }

  /**
   * Validacion de tipo de Transaccion.
   * @param conf as TransactionObjectsDto
   * @return String
   */
  public static String validTxtType(final TransactionObjectsDto conf) {
    return conf.getTxnType() != null ? conf.getTxnType().getStrType() : "";
  }

  /**
   * send message to platform.
   * @param smessage as String
   * @param conf as TransactionObjectsSto
   * @param sgComm as MicroserviceSendHttp
   * @return XmlResponse
   @throws CommonOperationException error.
   */
  public static XmlResponse sendMsg(final String smessage, final TransactionObjectsDto conf,
                                    final MicroserviceSendHttp sgComm) throws CommonOperationException {
    XmlResponse response;
    StringBuilder sbuffersend = new StringBuilder();
    String type = validTxtType(conf);
    if (smessage != null) {
      sbuffersend.append(smessage); //create smartMicroservicio.xml for data platform
      Console.writeln(Console.Level.DEBUG, conf.getIdOperation(), "MESSAGE REQUEST " + type + " "
            + "[Plataforma] ==> " + smessage);
        String stTxn = sgComm.sendPost(sbuffersend.toString(), true);

      response = validSendPost(stTxn, conf, type);
    } else {
      throw new GeneralException(CoreErrorServicesTable.INCOMPLETE_PARAMETERS);
    }
    return response;
  }

  /**
   *  Validacion de post probocada por sonar.
   * @param stTxn as String
   * @param conf as TransactionObjectsDto
   * @param type as String
   * @return  XmlResponse
   * @throws GeneralException GeneralException
   */
  public static XmlResponse validSendPost(final String stTxn, final TransactionObjectsDto conf,
                                   final String type) throws GeneralException {

    XmlResponse response;
    if (stTxn != null && !"".equals(stTxn)) {
      Console.writeln(Console.Level.DEBUG, conf.getIdOperation(), "MESSAGE RESPONSE " + type
            + " [Plataforma] ==> " + stTxn);
        response = XmlResponse.createObject(stTxn, conf); //Valid request the platform
    } else {
      throw new GeneralException(CoreErrorServicesTable.NOT_ANSWER_PROVIDER);
    }

    return response;
  }

}
