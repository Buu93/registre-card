package com.smart.auth.service.clientmessage;


import com.smart.auth.exception.GeneralException;
import com.smart.auth.exception.SystemException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.message.CoreErrorServicesTable;
import com.smart.auth.exception.message.MessageRespose;
import com.smart.auth.model.dto.HeaderDto;
import com.smart.auth.model.dto.MessageDto;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.util.HashMap;
import java.util.Map;

/**
 * clase XmlResponse.java.
 *
 * @author Jose Antonio Rodriguez Martinez - Smart BT ,S.A. de C.V.
 */
public class XmlResponse {

  /**
   * Declaración de variable header.
   */
  private HeaderDto header;
  /**
   * Declaración de variable message.
   */
  private MessageDto message;
  /**
   * Declaración de variable version.
   */
  private String version = "";
  /**
   * Declaración de variable paramsMap.
   */
  private Map<String, Object> paramsMap;
  /**
   * Declaración de variable MSGWS.
   */
  private static final String MSGWS = "sbt-ws-message";
  /**
   * Declaración de variable HEAD.
   */
  private static final String HEAD = "header";

  /**
   * constructor.
   */
  public XmlResponse() {
    this.paramsMap = new HashMap<>();
  }

  /**
   * constructor.
   *
   * @param map as HashMap.
   */
  public XmlResponse(final Map map) {
    this.paramsMap = map;
  }

  /**
   * Constructor XmlRepsonse.
   *
   * @param header  as HeaderDto.
   * @param message as MessageDto.
   * @param version as String
   */
  public XmlResponse(final HeaderDto header, final MessageDto message, final String version) {
    this.header = header;
    this.message = message;
    this.version = version;
    this.paramsMap = new HashMap<>();
  }

  /**
   * Getter Header.
   *
   * @return HeaderDto
   */
  public HeaderDto getHeader() {
    if (header == null) {
      header = new HeaderDto();
    }
    return header;
  }

  /**
   * Setter Header.
   *
   * @param header as HeaderDto
   */
  public void setHeader(final HeaderDto header) {
    this.header = header;
  }

  /**
   * Getter Message.
   *
   * @return MessageDto
   */
  public MessageDto getMessage() {
    if (message == null) {
      message = new MessageDto();
    }
    return message;
  }

  /**
   * Setter Message.
   *
   * @param message as MessageDto
   */
  public void setMessage(final MessageDto message) {
    this.message = message;
  }

  /**
   * Getter version.
   *
   * @return String
   */
  public String getVersion() {
    return version;
  }

  /**
   * Setter version.
   *
   * @param version as String
   */
  public void setVersion(final String version) {
    this.version = version;
  }

  /**
   * Método toString.
   *
   * @return String
   */
  @Override
  public String toString() {
    return "XmlResponse [header = " + header + ", message = "
          + message + ", version = " + version + "]";
  }

  /**
   * Método toStringXml.
   *
   * @return String
   */
  public String toStringXml() {
    StringBuilder xml = new StringBuilder();
    xml.append("<?xmlversion=\"1.0\"encoding=\"UTF-8\"?>");
    xml.append("<sbt-ws-message version=\"").append(version).append("\">");
    if (header != null) {
      xml.append(header.toStringXml());
    }
    if (message != null) {
      xml.append(message.toStringXml());
    }
    xml.append("</sbt-ws-message>");
    return xml.toString();
  }

  /**
   * Método createObject.
   *
   * @param xml  as String
   * @param conf as TransactionObjectsDto
   * @return XmlResponse
   * @throws GeneralException error.
   */
  public static XmlResponse createObject(final String xml, final TransactionObjectsDto conf)
        throws GeneralException {
    XStream xstream = new XStream(new DomDriver()); // does not require XPP3 library
    if (xml == null || xml.isEmpty()) {
      throw new  GeneralException(CoreErrorServicesTable.NOT_ANSWER_PROVIDER);
    }
    if (!xml.contains(MSGWS)) {
      throwMissingParamException(MSGWS);
    }

    if (!xml.contains(HEAD)) {
      throwMissingParamException(HEAD);
    }

    xstream.alias(MSGWS, XmlResponse.class);
    xstream.aliasAttribute(XmlResponse.class, "version", "version");
    xstream.alias(HEAD, HeaderDto.class);
    xstream.aliasField("Resp-Code", HeaderDto.class, "respCode");
    xstream.aliasField("Resp-Message", HeaderDto.class, "respMessage");
    xstream.aliasField("LastServerKey", HeaderDto.class, "lastServerKey");
    xstream.aliasField("Type", HeaderDto.class, "type");
    xstream.aliasField("DeviceTime", HeaderDto.class, "deviceTime");
    xstream.aliasField("SerialPos", HeaderDto.class, "serialPos");
    xstream.aliasField("ClientID", HeaderDto.class, "clientID");
    xstream.aliasField("Stan", HeaderDto.class, "stan");
    xstream.omitField(HeaderDto.class, "request");

    xstream.alias("message", MessageDto.class);
    xstream.aliasField("Aut-Code", MessageDto.class, "autCode");
    xstream.aliasField("DataCipher", MessageDto.class, "dataCipher");
    xstream.aliasField("DataText", MessageDto.class, "dataText");
    xstream.aliasField("RefClientNum", MessageDto.class, "refClientNum");
    xstream.aliasField("RefSPNum", MessageDto.class, "refSPNum");
    xstream.aliasField("ServiceName", MessageDto.class, "serviceName");
    xstream.aliasField("DataEMV", MessageDto.class, "dataEMV");

    XmlResponse data = null;
    try {
      data = (XmlResponse) xstream.fromXML(xml);

      if (data != null) {
        data.parseMapList(data.getMessage().getDataText());
      } else {
        throw new SystemException(CoreErrorServicesTable.INVALID_RESPONSE);
      }
    } catch (Exception ex) {
      Console.writeException(conf.getIdOperation(), ex);
      data = new XmlResponse();
      data.getHeader().setRespCode(CoreErrorServicesTable.INVALID_RESPONSE.getErrorCode());
      data.getHeader().setRespMessage(CoreErrorServicesTable.INVALID_RESPONSE.getErrorMessage());
    }

    return data;
  }

  /**
   * Método throwMissingParamException.
   *
   * @param element as String
   * @throws GeneralException error.
   */
  private static void throwMissingParamException(final String element) throws GeneralException {
    String errorMessage = "MENSAJE MAL FORMADO, FALTA ELEMENTO '" + element + "'";
    throw new GeneralException(new MessageRespose(errorMessage, "96"));
  }

  /**
   * Método parseMapList.
   *
   * @param text as String
   */
  public void parseMapList(final String text) {
    String separator = "\r\n";

    if (text != null && !text.isEmpty()) {

      if (!text.contains("\r\n") && text.contains("\n")) {
        separator = "\n";
      }
      cicloparseMapList(text, separator);
    }
  }

  /**
   * Método agregado par emular el ciclo de parseMapList.
   *
   * @param text      as String
   * @param separator as String
   */
  public void cicloparseMapList(final String text, final String separator) {
    String paramName, paramValue;
    int beginIndex, posEq, posEnd;
    int posString = 0;
    while (posString < text.length()) {
      beginIndex = posString;
      posEq = text.indexOf('=', beginIndex);
      if (posEq > 0) {
        paramName = text.substring(beginIndex, posEq);
        posEnd = text.indexOf(separator, posEq + 1);
        if (posEnd > 0) {
          paramValue = text.substring(posEq + 1, posEnd);
          posString = posEnd + 1;
        } else {
          paramValue = text.substring(posEq + 1);
          posString = text.length();
        }
        addRequestParam(paramName, paramValue);
      } else {
        throw new IllegalArgumentException("FORMATO DE LISTA DE CAMPOS INCORRECTO");
      }
    }
  }

  /**
   * Método addRequestParam.
   *
   * @param paramName  as String
   * @param paramValue as String
   */
  private void addRequestParam(final String paramName, final String paramValue) {
    if (paramsMap == null) {
      paramsMap = new HashMap<>();
    }
    paramsMap.put(paramName, paramValue);
  }

  /**
   * Método getStringParameter.
   *
   * @param paramName as String
   * @return String
   */
  public String getStringParameter(final String paramName) {
    return paramsMap.containsKey(paramName) ? paramsMap.get(paramName).toString() : null;
  }

  /**
   * Getter intParameter.
   *
   * @param paramName    as String
   * @param defaultValue as int
   * @return int
   */
  public int getIntParameter(final String paramName, final int defaultValue) {
    int iret = defaultValue;
    try {
      iret = Integer.parseInt(paramsMap.get(paramName).toString());
    } catch (Exception e) {
      Console.writeException("", e);
      iret = defaultValue;
    }
    return iret;
  }

}
