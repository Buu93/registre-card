/*******************************************************************
 MessageUtil.java
 @author : Carlos A Lopez P
 @creationDate : 11/03/2019
 @specFile:
 @revisedBy : Carlos A Lopez P
 @date : 11/03/2019
 *******************************************************************/


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.service.clientmessage;

/**
 * Clase MessageUtil.java.
 *
 */
public final class MessageUtil {

  /**
   * constructor.
   * */
  private MessageUtil() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Método addValue.
   *
   * @param value            as String
   * @param requestFieldName as String
   * @param request          as XmlRequest
   * @param secure           as boolean
   */
  public static void addValue(final String value, final String requestFieldName, final XmlRequest request,
                              final boolean secure) {
    if (secure) {
      request.addCipherTextParam(requestFieldName, value);
    } else {
      request.addDataTextParam(requestFieldName, value);
    }
  }

}

//<editor-fold defaultstate="collapsed" desc="Modifications comments">

/************************************************************ 
 * @updateDate: dd/MM/yyyy
 * @author: ?
 * @revisedBy : ?
 *           @date : dd/MM/yyyy
 * @Description:
 *   (Write text here)
 * ************************************************************
 *  *
 *  *
 *  *
 * ************************************************************
 */

//</editor-fold>

