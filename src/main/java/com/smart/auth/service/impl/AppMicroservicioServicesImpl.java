package com.smart.auth.service.impl;

import com.smart.auth.config.cipher.ServiceEncryptionData;
import com.smart.auth.exception.*;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.logs.SystemLog;
import com.smart.auth.exception.message.CoreErrorServicesTable;
import com.smart.auth.exception.message.MessageRespose;
import com.smart.auth.model.Field;
import com.smart.auth.model.MicroserviceRequest;
import com.smart.auth.model.constants.ResponseCode;
import com.smart.auth.model.constants.TxnStatus;
import com.smart.auth.model.dto.MicroserviceResponseDTO;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.model.dto.TxnByDayDto;
import com.smart.auth.model.enumutils.TransactionType;
import com.smart.auth.service.AppMicroservicioServices;
import com.smart.auth.service.RequestServices;
import com.smart.auth.service.clientmessage.XmlRequest;
import com.smart.auth.service.impl.error.Management;
import com.smart.auth.service.impl.error.SystemSettings;
import com.smart.auth.service.impl.operation.*;
import com.smart.auth.model.enumutils.Number;

import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Calendar;
import java.util.regex.Pattern;

/**
 * Services AppMicroservicioServicesImpl.
 */
@Service("AppMicroservicioServices")
public class AppMicroservicioServicesImpl implements AppMicroservicioServices {

  /**
   * Valor global de la clase.
   */

  /**
   * Process services.
   * @param dto MicroserviceRequest.
   * @param request HttpServletRequest.
   * @return MicroserviceResponseDTO.
   */
  @Override
  public MicroserviceResponseDTO processRequest(final MicroserviceRequest dto, final HttpServletRequest request) {
    MicroserviceResponseDTO strXmlResponse = new MicroserviceResponseDTO();

    String idOperation = SystemLog.newTxnIdOperation();
    TransactionObjectsDto txnObjects = new TransactionObjectsDto(idOperation, request.isSecure());

    String logLine =
          "Petición " + request.getMethod() + " " + request.getProtocol() + " desde "
                + request.getRemoteAddr() + ":" + request.getRemotePort() + " a "
                + request.getServletPath();
    SystemLog.writeEntry(idOperation, logLine);
    String account = "";
    try {
      Management.acceptTxn();

      Console.writeln(Console.Level.INFO, idOperation,
            "XML Request (recibido):\n" + dto.toString());

      if (check(dto)) {
        account = dto.getNumTarjeta();
        RequestServices services = MapResponse.getInstance().getResponse(dto.getTipoOperacion());

        txnObjects.setGNPRequest(dto);
        txnObjects.setIdClient(dto.getClientID());

        strXmlResponse = requestLogon(txnObjects, idOperation, services);
      }
    } catch (MicroserviceException ex) { //Excepcion de InputStream
      strXmlResponse = fillMsgRespWithExceptionData(txnObjects, ex);
      Console.writeException(idOperation, ex);
    } catch (XmlException e) {
      strXmlResponse = fillMsgRespWithExceptionData(txnObjects, e);
      Console.writeException(idOperation, e);
    } catch (AppMicroserviceException e) {
      e.printStackTrace();
    } finally {
      printTramaToLog(account, strXmlResponse.toString(), txnObjects);
      /* validando y actualizacion de datos */
      approving(txnObjects);
      txnObjects.setEndTime(System.currentTimeMillis());
      Management.endTxn(txnObjects);
    }

    return responseHttp(strXmlResponse, idOperation);
  }

  /**
   * Proceso logon.
   * @param txnObjects TransactionObjectsDto.
   * @param idOperation String.
   * @param services RequestServices.
   * @return MicroserviceResponseDTO.
   * @throws Exception Exception.
   * @throws MicroserviceException MicroserviceException.
   */
  private static MicroserviceResponseDTO requestLogon(final TransactionObjectsDto txnObjects, final String idOperation,
                                             final RequestServices services)throws MicroserviceException {
    OperationResponse opResponse = null;
    try {
      RequestServices logon = new O04Logon();
      logon.processRequest(txnObjects, idOperation);
      opResponse = services.processRequest(txnObjects, idOperation); //Method
    } catch (CodeMessageException e) {
      Console.writeException(idOperation, e);
      throw new MicroserviceException(new MessageRespose(e.getLocalizedMessage(),
            e.getResponse().getResponse().getCodigoError()));
    }
    return opResponse.getResponse();
  }

  /**
   * Proceso de validacion logon.
   * @param request MicroserviceRequest.
   * @return Boolean.
   * @throws XmlException XmlException.
   */
  public Boolean check(final MicroserviceRequest request)throws XmlException {

    if (request != null) {
      if (request.getIdTerminalExterna() == null || request.getIdTerminalExterna().isEmpty() && request.getAliasTarjeta().isEmpty()) {
        throw new XmlException(XmlException.Error.ERRORPARAM, "idTerminalExterna");
      }
    } else {
      throw new XmlException(XmlException.Error.NOREAD, "Parse Error");
    }

    valCheck(request);

    return true;
  }

  /**
   * Separacion de metodo validacion sonar.
   * @param request as MicroserviceRequest
   * @throws XmlException XmlException
   */
  private static void valCheck(final MicroserviceRequest request)throws XmlException {
    if ((request.getImporte() == null || request.getImporte().isEmpty()
          || !checkValue(request.getImporte(), Field.DataType.NUMBER ))
          && !!request.getTipoOperacion().equals(Number.CODE60)) {
      throw new XmlException(XmlException.Error.ERRORPARAM, "importe");
    }

    if (request.getNumTarjeta() == null || request.getNumTarjeta().isEmpty()) {
      throw new XmlException(XmlException.Error.ERRORPARAM, "numTarjeta");
    }
  }

  /**
   * validando parametros.
   * @param value String.
   * @param type Field.DataType.
   * @return boolean.
   */
  public static Boolean checkValue(final String value, final Field.DataType type) {
    boolean match = false;
    if (type == Field.DataType.NUMBER) {
      Pattern pattern = Pattern.compile("[\\d]*([.][\\d]+)?");

      match = pattern.matcher(value).matches();
    }

    return match;
  }

  /**
   * Method fillMsgRespWithExceptionData.
   * @param dto TransactionObjectsDto.
   * @param ex Exception.
   * @return MicroserviceResponseDTO.
   */
  public MicroserviceResponseDTO fillMsgRespWithExceptionData(final TransactionObjectsDto dto,
                                                     final Exception ex) {
    TransactionObjectsDto response = dto;
    MicroserviceResponseDTO requestGnp = new MicroserviceResponseDTO();
    if (ex instanceof DbException) { // DBException
      response.setResponseCode(CoreErrorServicesTable.GRAL_ERROR.getErrorCode());
      response.setResponseMessage(ex.getMessage());

    } else if (ex instanceof TimeOutException) {
      doAutomaticReverse(response);
      response.setResponseCode(((TimeOutException) ex).getRespCode());
      response.setResponseMessage(((TimeOutException) ex).getRespMessage());
    } else if (ex instanceof CodeMessageException) {
      OperationResponse res = ((CodeMessageException) ex).getResponse();
      requestGnp = res.getResponse();
      response.setResponseCode(ResponseCode.VENDOR_ERROR);
      response.setResponseCode(res.getResponse().getCodigoError());
      response.setResponseMessage(res.getResponse().getDescripcionError());
    } else {
      response = fillMsgResp(response, ex);
    }

    requestGnp = validRequest(requestGnp, response);

    return requestGnp;
  }

  /**
   * validRequest generado por sonar.
   * @param repos as MicroserviceResponseDTO
   * @param dto as TransactionObjectsDto
   * @return MicroserviceResponseDTO
   */
  public MicroserviceResponseDTO validRequest(final MicroserviceResponseDTO repos, final TransactionObjectsDto dto) {
    TransactionObjectsDto response = dto;
    MicroserviceResponseDTO reposnse = repos;
    if (repos.getDescripcionError().isEmpty() || repos.getCodigoError().isEmpty()) {
      reposnse = printException(
            response.getResponseCode(),
            response.getResponseMessage(),
            response.getIdOperation(),
            repos
      );
    }

    return reposnse;
  }

  /**
   * Particion de metodo por validacion sonar en Cyclomatic Complexity.
   * @param dto as TransactionObjectsDto.
   * @param ex as Exception.
   * @return TransactionObjectsDto.
   */
  public static TransactionObjectsDto fillMsgResp(final TransactionObjectsDto dto,
                                                  final Exception ex) {
    TransactionObjectsDto response = dto;
    if (ex instanceof AppMicroserviceException) {
      response.setResponseCode(((AppMicroserviceException) ex).getCode());
      response.setResponseMessage(((AppMicroserviceException) ex).getCodeMessage());
    } else if (ex instanceof CommonOperationException) {
      response.setResponseCode(((CommonOperationException) ex).getRespCode());
      response.setResponseMessage(((CommonOperationException) ex).getRespMessage());
    } else if (ex instanceof XmlException) {
      response.setResponseCode(((XmlException) ex).getCode());
      response.setResponseMessage(((XmlException) ex).getCodeMessage());
    } else if (ex instanceof IOException) {
      response.setResponseCode(ResponseCode.GRAL_ERROR);
      response.setResponseMessage("Error al leer el XML. " + ex.getMessage());
    } else if (ex instanceof MicroserviceException) {
      response.setResponseMessage(((MicroserviceException) ex).getErrorMsg());
      response.setResponseCode(((MicroserviceException) ex).getErrorCode());
    } else {
      response.setResponseMessage(ex.getMessage());
      response.setResponseCode(ResponseCode.GRAL_ERROR);
    }

    return response;
  }

  /**
   * do Automatic Reverse.
   *
   * @param txnObjects "".
   */
  protected void doAutomaticReverse(final TransactionObjectsDto txnObjects) {
    String idOperation = SystemLog.newTxnIdOperation();
    TransactionObjectsDto txnObjectsRev = new TransactionObjectsDto(idOperation, false);
    MicroserviceRequest requestGnp = txnObjects.getGNPRequest();
    XmlRequest request = txnObjects.getRequestWS();
    txnObjectsRev.setGNPRequest(requestGnp);
    txnObjectsRev.setRequestWS(request);
    ReversalThread rt = new ReversalThread(txnObjectsRev);
    rt.setDaemon(true);
    rt.setName("GNPReversalThread, idTransaccion" + idOperation);
    rt.start();
  }

  /**
   * Method printException.
   * @param code String.
   * @param message String.
   * @param idOperation String.
   * @param res MicroserviceResponseDTO.
   * @return MicroserviceResponseDTO.
   */
  public static MicroserviceResponseDTO printException(final String code, final String message, final String idOperation,
                                              final MicroserviceResponseDTO res) {
    OperationResponse response = new OperationResponse(idOperation);
    CommonOperation.buildCodeAndMessage(response, code, message);

    res.setCodigoError(code);
    res.setDescripcionError(message);
    return res;

  }


  /**
   * Print info to Log.
   *
   * @param account        "".
   * @param xmlResponse "".
   * @param txnObjects     "".
   */
  public static void printTramaToLog(final String account, final String xmlResponse,
                                     final TransactionObjectsDto txnObjects) {

    String strXmlResponse = xmlResponse;

    if (SystemSettings.getInt("mask.data", 1) == 1 && strXmlResponse.contains(account)) {
      strXmlResponse = strXmlResponse.replace(account,
            ServiceEncryptionData.maskAccount(account));
    }
    Console.writeln(Console.Level.INFO, txnObjects.getIdOperation(),
          "XML Response:\n" + strXmlResponse);

  }

  /**
   * Method approving.
   * @param txnObjects TransactionObjectsDto.
   */
  public void approving(final TransactionObjectsDto txnObjects) {

    if (txnObjects == null) {
      return;
    }
    TxnByDayDto txnDay = txnObjects.getTxnByDay();
    String idOperation = txnObjects.getIdOperation();

    SystemLog.writeEntry(idOperation, "RESPONSE_CODE =" + txnObjects.getResponseCode());
    SystemLog.writeEntry(idOperation, "RESPONSE_MESSAGE =" + txnObjects.getResponseMessage());


    if (txnDay.getId() > 0L
          && !txnObjects.getTxnType().equals(TransactionType.REVERSAL)
          && !txnObjects.getTxnType().equals(TransactionType.VOID)) {

      txnDay.setResponseCode(txnObjects.getResponseCode());

      if (txnObjects.getResponseCode().equals(ResponseCode.OK)) {
        Management.getStatServ().setLastTxnDate(Calendar.getInstance());
        txnDay.setStatus(TxnStatus.APROVED);
        txnDay.setResponseMessage("APROBADA");
      } else {
        txnDay.setResponseMessage(txnObjects.getResponseMessage());
        txnDay.setStatus(TxnStatus.DENIED);
      }


      try {
        txnObjects.getTxnByDayDAO().createUpdate(txnObjects);
      } catch (Exception ex1) {
        SystemLog.writeException(idOperation, ex1);
        SystemLog.writeEntry(idOperation,
              CoreErrorServicesTable.DB_UPDATE_ERROR.getErrorMessage()
                    + " CODIGO DE RESPUESTA:" + txnObjects.getResponseCode());

      }

    }

  }

  /**
   * Method responseHttp.
   * @param strXmlResponse MicroserviceResponseDTO.
   * @param idOperation String.
   * @return RespuestaGenerica.
   */
  public MicroserviceResponseDTO responseHttp(final MicroserviceResponseDTO strXmlResponse, final String idOperation) {

    if (strXmlResponse.getCodigoError() != null) {
      Console.writeln(Console.Level.INFO, "Codigo de error: " + strXmlResponse.getCodigoError());
    }

    SystemLog.writeEntry(idOperation, "Termina operación");

    return strXmlResponse;
  }
}
