package com.smart.auth.service.impl;

import com.smart.auth.config.TemporalUtils;
import com.smart.auth.model.dto.StatisticsDto;
import com.smart.auth.model.enumutils.Number;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.LongAdder;


/**
 * Class StatisticService.
 */
public class StatisticService {
  /** * Date. */
  private Date startDate = Calendar.getInstance().getTime();
  /** * LongAdder. */
  private LongAdder transactionCount = new LongAdder();
  /** * LongAdder. */
  private LongAdder aliveTransactionCount = new LongAdder();
  /** * LongAdder. */
  private LongAdder[] arrTxnCount = new LongAdder[Number.CODE60.value()];
  /** * LongAdder. */
  private LongAdder[] arrTxnTime = new LongAdder[Number.CODE60.value()];
  /** * Calendar. */
  private Calendar currHitDate;
  /** * Calendar. */
  private Calendar lastHitDate;
  /** * Calendar. */
  private Calendar lastTxnDate;


  /**
   * Constructor init startDate.
   * @return Date.
   */
  public Date getStartDate() {
    return startDate;
  }

  /**
   * setStartDate.
   * @param startDate Date.
   */
  public void setStartDate(final Date startDate) {
    this.startDate = startDate;
  }

  /**
   * Increment trasaccion addTransaction.
   */
  public void addTransaction() {
    this.transactionCount.increment();
  }

  /**
   * Increment trasaccion addAliveTransaction.
   */
  public void addAliveTransaction() {
    aliveTransactionCount.increment();
  }

  /**
   * Decrement trasaccion removeAliveTransaction.
   */
  public void removeAliveTransaction() {
    aliveTransactionCount.decrement();
  }

  /**
   * Method getLastTxnDate.
   *
   * @return the lastTxnDate
   */
  public Calendar getLastTxnDate() {
    return lastTxnDate;
  }

  /**
   * Method setLastTxnDate.
   * @param lastTxnDate the lastTxnDate to set
   */
  public void setLastTxnDate(final Calendar lastTxnDate) {
    this.lastTxnDate = lastTxnDate;
  }


  /**
   * Method hit.duration.
   * @param duration long.
   */
  public void hit(final long duration) {

    Calendar cal = Calendar.getInstance();
    final int min = cal.get(Calendar.MINUTE);
    final int hour = cal.get(Calendar.HOUR_OF_DAY);

    LongAdder la = arrTxnCount[min];
    la = valArrTxnCount(la, min);
    la.increment();

    LongAdder lat = arrTxnTime[min];
    lat = valArrTxnTime(lat, min);
    lat.add(duration);

    if (!isSameMinute(min, hour)) {
      //Delete all before 2 seconds
      if (lastHitDate != null) {
        cal.add(Calendar.MINUTE, Number.CODE002.value());
        int lastMin = cal.get(Calendar.MINUTE);
        LongAdder laToReset = arrTxnCount[lastMin];
        valLaToReset(laToReset);

        LongAdder latToReset = arrTxnTime[lastMin];
        valLatToReset(latToReset);
      }
      lastHitDate = currHitDate;
      currHitDate = cal;

    }
  }

  /**
   * valArrTxnCount con validacion sonar.
   * @param lar as LongAdder.
   * @param min as int
   * @return LongAdder
   */
  public LongAdder valArrTxnCount(final LongAdder lar, final int min) {
    LongAdder la = lar;
    if (la == null) {
      arrTxnCount[min] = new LongAdder();
      la = arrTxnCount[min];
    }

    return la;
  }

  /**
   * valArrTxnTime valiacion causada por sonar.
   * @param lar as LongAdder
   * @param min as int
   * @return LongAdder
   */
  public LongAdder valArrTxnTime(final LongAdder lar, final int min) {
    LongAdder lat = lar;
    if (lat == null) {
      arrTxnTime[min] = new LongAdder();
      lat = arrTxnTime[min];
    }

    return lat;
  }

  /**
   * Validacion provocada por sonar.
   * @param laToReset as LongAdder
   * @return LongAdder
   */
  public LongAdder valLaToReset(final LongAdder laToReset) {
    if (laToReset != null) {
      laToReset.reset();
    }

    return laToReset;
  }

  /**
   * Validacion provocada por sonar.
   * @param latToReset as LongAdder
   * @return LongAdder
   */
  public LongAdder valLatToReset(final LongAdder latToReset) {
    if (latToReset != null) {
      latToReset.reset();
    }

    return latToReset;
  }

  /**
   * isSameMinute.
   * @param min2 int.
   * @param hour2 int.
   * @return boolean.
   */
  public boolean isSameMinute(final int min2, final int hour2) {
    if (currHitDate == null) {
      currHitDate = Calendar.getInstance();
    }
    final int min = currHitDate.get(Calendar.MINUTE);
    final int hour = currHitDate.get(Calendar.HOUR_OF_DAY);

    return hour == hour2 && min == min2;
  }

  /**
   * Method calculate.
   *
   * @return StatisticsDto.
   */
  public StatisticsDto calculate() {
    StatisticsDto stat = new StatisticsDto();
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.MINUTE, -1);
    final int min = cal.get(Calendar.MINUTE);
    long totalHits;
    long timeAvg;
    long duration;

    LongAdder la = arrTxnCount[min];
    totalHits = longValue(la);

    LongAdder lat = arrTxnTime[min];
    duration = longValue(lat);

    timeAvg = timeAvgs(totalHits, duration);

    if (lastTxnDate != null) {
      stat.setLastTxnDate(TemporalUtils.getDate("dd/MM/yyyy HH:mm:ss", lastTxnDate));
    }

    stat.setStartDate(startDate);
    stat.setTransactionCount(transactionCount.longValue());
    stat.setAliveTransactionCount(aliveTransactionCount.longValue());
    stat.setTotalAvailableMemory(Runtime.getRuntime().freeMemory() >> Number.CODE20.value());
    stat.setTotalUsedMemory(Runtime.getRuntime().totalMemory() >> Number.CODE20.value());
    stat.setTxnsPerMinuteCount(totalHits);
    stat.setResponseTimeAvg(timeAvg);
    return stat;

  }

  /**
   * set LongAdder.
   * @param la as LongAdder.
   * @return long
   */
  public long longValue(final LongAdder la) {
    return la != null ? la.longValue() : 0;
  }

  /**
   * set values.
   * @param totalHits as long
   * @param duration as long
   * @return long
   */
  public long timeAvgs(final long totalHits, final long duration) {
    return totalHits > 0 && duration > 0 ?  duration / totalHits : 0;
  }

  /**
   * Setter agregado para test.
   * @param currHitDate as Calendar
   * */
  public void setCurrHitDate(final Calendar currHitDate) {
    this.currHitDate = currHitDate;
  }

  /**
   * Setter agregado para test.
   * @param lastHitDate as Calendar
   * */
  public void setLastHitDate(final Calendar lastHitDate) {
    this.lastHitDate = lastHitDate;
  }

  /**
   * setter arrTxnCount.
   * @param arrTxnCount as LongAdder[]
   * */
  public void setArrTxnCount(final LongAdder[] arrTxnCount) {
    this.arrTxnCount = arrTxnCount;
  }
}
