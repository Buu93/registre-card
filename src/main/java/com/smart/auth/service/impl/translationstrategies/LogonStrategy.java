/**
 * ****************************************************************
 * LogonStrategy.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 - *****************************************************************
 */

package com.smart.auth.service.impl.translationstrategies;


import com.smart.auth.config.ConversionUtils;
import com.smart.auth.config.TripleDes;
import com.smart.auth.config.cipher.ServiceEncryptionData;
import com.smart.auth.model.constants.ProcessingCode;
import com.smart.auth.model.dto.PosDataDto;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.model.enumutils.TransactionType;
import com.smart.auth.service.clientmessage.XmlRequest;
import com.smart.auth.service.clientmessage.XmlResponse;

/**
 * Class LogonStrategy.
 * @author Jose A Rodriguez M
 */
public class LogonStrategy extends AppStrategyTranslator {

  /** * TransactionType. */
  private final TransactionType txnType;

  /**
   * Constructor.
   */
  public LogonStrategy() {
    this.txnType = TransactionType.LOGON;
  }


  /**
   * Method decode.
   * @param txnObjects TransactionObjectsDto.
   */
  @Override
  public void decode(final TransactionObjectsDto txnObjects) {
    setIdOperation(txnObjects.getIdOperation());
    txnObjects.setTxnType(txnType);
    decodeBridgeMessage(txnObjects);
  }

  /**
   * Process decodeBridgeMessage.
   * @param txnObjects TransactionObjectsDto.
   */
  private void decodeBridgeMessage(final TransactionObjectsDto txnObjects) {
    translateCommonFields(txnObjects);
    PosDataDto logonData = txnObjects.getLogonData();
    XmlRequest request = txnObjects.getRequestWS();
    request.setType(ProcessingCode.LOGON);
    request.setSerialPos(txnObjects.getGNPRequest().getIdTerminalExterna());
    request.addCipherTextParam("14", buildRandomKey());
    request.addCipherTextParam("12", logonData.getUserName());
    request.addCipherTextParam("13", getUserP(logonData));
    request.addCipherTextParam("19", request.getStan());
  }

  /**
   * Method encode.
   * @param txnObjects TransactionObjectsDto.
   */
  @Override
  public void encode(final TransactionObjectsDto txnObjects) {
    XmlResponse response = txnObjects.getResponseWS();
    byte[] serverKey = ConversionUtils.hexToBytes(response.getHeader().getLastServerKey());
    serverKey = TripleDes.stDecodeData(serverKey, txnObjects.getRequestWS().getCipherTextParam(
          "14"));
    String lastServerKey = ConversionUtils.bytesToHex(serverKey);
    txnObjects.setLastServerKey(lastServerKey);
  }

  /**
   * User getUserP.
   * @param logonData PosDataDto.
   * @return String.
   */
  private static String getUserP(final PosDataDto logonData) {
    String userP = logonData.getUserP();
    if (logonData.getStatusData() == 2) {
      userP = ServiceEncryptionData.decrypt(userP);
    }
    return userP;
  }

}
