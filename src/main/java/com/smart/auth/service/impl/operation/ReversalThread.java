/**
 * ****************************************************************
 * ReversalThread.java
 *
 * @author : Carlos A Lopez P
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Carlos A Lopez P
 * @date : 11/03/2019
 - *****************************************************************
 */

package com.smart.auth.service.impl.operation;

import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.service.impl.error.SystemSettings;
import com.smart.auth.model.enumutils.Number;

/**
 * Class ReversalThread.
 *
 */
public class ReversalThread extends Thread {

  /** * idOperation. */
  private String idOperation;
  /** * TransactionObjectsDto. */
  private TransactionObjectsDto txnObjects;
  /** * attemptReverseCount. */
  private int attemptReverseCount = 0;

  /**
   * Method ReversalThread.
   * @param txnObjects TransactionObjectsDto.
   */
  public ReversalThread(final TransactionObjectsDto txnObjects) {
    this.txnObjects = txnObjects;
    idOperation = txnObjects.getIdOperation();

  }

  /**
   * Init Thread.
   */
  @Override
  public void run() {

    int numAtTempTsReverse;
    long timeBetweenAtTempTs;

    OperationResponse processRequest;

    numAtTempTsReverse = validReverse(SystemSettings.getInt("max.time.to.reverse", Number.CODE3.value()));

    timeBetweenAtTempTs = SystemSettings.getInt("time.bet.attempt.to.reverse", Number.CODE5.value());
    while (true) {
      attemptReverseCount++;
      try {
        Thread.sleep(timeBetweenAtTempTs * Number.CODE1000.value());
      } catch (InterruptedException ex) {
        Console.writeln(Console.Level.INFO, idOperation, "GNPReversalThread : El hilo fue "
              + "interrumpido: transaccion:");
        Console.writeException(idOperation, ex);
        // Restore interrupted state...
        Thread.currentThread().interrupt();
      }
      try {
//        O05Reverse o05Reverse = new O05Reverse(TransactionType.REVERSAL);
//        processRequest = o05Reverse.processRequest(txnObjects, idOperation);
//
//        if (validprocessRequest(processRequest) || attemptReverseCount > numAtTempTsReverse) {
//          logreverso(attemptReverseCount, numAtTempTsReverse);
//          break;
//        }

      } catch (Exception ex) {
        Console.writeException(idOperation, ex);
      }

    }
  }

  /**
   * validReverse generado por sonar.
   * @param num int
   * @return int
   */
  public int validReverse(final int num) {
    int numAtTempTsReverse = num;
    if (numAtTempTsReverse == 0) {
      numAtTempTsReverse = 1;
    }

    return numAtTempTsReverse;
  }

  /**
   * logreverso.
   * @param attemptReverseCount as int
   * @param numAtTempTsReverse as int
   */
  public void logreverso(final int attemptReverseCount, final int numAtTempTsReverse) {
    if (attemptReverseCount > numAtTempTsReverse) {
      Console.writeln(Console.Level.INFO, idOperation, "GNPReversalThread : Numero maximo de "
            + "intentos alcanzado");
    }
  }

  /**
   * Valid parameterRequest.
   * @param processRequest as OperationResponse.
   * @return boolean.
   */
  public boolean validprocessRequest(final OperationResponse processRequest) {
    boolean isBreack = false;
    if (processRequest != null && processRequest.getResponse() != null) {
      String codigoError = processRequest.getResponse().getCodigoError();
      String codSuccess = SystemSettings.getString("error.code.00", "00");
      if (codigoError.equals(codSuccess)) {
        Console.writeln(Console.Level.INFO, idOperation, "GNPReversalThread : Transacción "
              + "reversada");
        isBreack = true;
      }
    }

    return isBreack;
  }
}

//<editor-fold defaultstate="collapsed" desc="Modifications comments">
/**
 * **********************************************************
 * @updateDate: dd/MM/yyyy
 * @author: ?
 * @revisedBy : ?
 * @date : dd/MM/yyyy
 * @Description: (Write text here)
 * ************************************************************ * * *
 * ************************************************************
 */
//</editor-fold>

