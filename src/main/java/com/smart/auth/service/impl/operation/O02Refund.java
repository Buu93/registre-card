/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.service.impl.operation;


import com.smart.auth.exception.CodeMessageException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.message.MessageRespose;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.service.error.IErrorServices;
import com.smart.auth.service.impl.translationstrategies.AppStrategyTranslator;
import com.smart.auth.service.impl.translationstrategies.RefundStrategy;

/**
 * Class O02Refund.
 * @author Carlos López
 */
public class O02Refund extends OperationController {


  /**
   * processRequest Refund.
   *
   * @param txnObjects TransactionObjectsDto.
   * @param idOperation String.
   * @return OperationResponse.
   * @throws CodeMessageException the code exception
   */
  @Override
  public OperationResponse processRequest(final TransactionObjectsDto txnObjects,
                           final String idOperation) throws CodeMessageException {
    OperationResponse response = CommonOperation.getOperationResponse(txnObjects);

    try {
      //1.Checar si ya se realizo logon.
      //2.Hacer la conversión de campos
      AppStrategyTranslator strategy = new RefundStrategy();

      //3.Crear el mensaje de envio
      //4.Enviar petición a la plataforma
      //5.Recibir Respuesta
      response = CommonOperation.generaResponse(response, strategy, txnObjects, idOperation);

    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
      IErrorServices error = new MessageRespose(ex.getMessage(), "96");
      throw new CodeMessageException(error);
    }
    return response;
  }

}
