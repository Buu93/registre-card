/*******************************************************************
 AppStrategyTranslator.java
 @author : Carlos A Lopez P
 @creationDate : 11/07/2018
 @specFile:
 @revisedBy : Carlos A Lopez P
 @date : 11/07/2018
 -*******************************************************************/

package com.smart.auth.service.impl.translationstrategies;

import com.smart.auth.config.ConversionUtils;
import com.smart.auth.config.cipher.ServiceEncryptionData;
import com.smart.auth.dao.SystemDao;
import com.smart.auth.exception.DbException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.MicroserviceRequest;
import com.smart.auth.model.constants.TxnStatus;
import com.smart.auth.model.dto.*;
import com.smart.auth.service.clientmessage.XmlRequest;
import com.smart.auth.service.clientmessage.XmlResponse;
import com.smart.auth.service.impl.error.SystemSettings;
import com.smart.auth.model.enumutils.Number;

import java.util.Calendar;
import java.util.Random;


/**
 * Class AppStrategyTranslator.
 *
 */
public abstract class AppStrategyTranslator {

  /**
   * idOperation.
   */
  private String idOperation = "";
  /**
   * CODE.
   */
  private static final String CODE = "%1$08X";
  /**
   * NUMBER.
   */
  private static final String NUMBER = "00";
  /** * vacio. */
  private static final String VACIO = "";

  /**
   * Constructor empty.
   */
  public AppStrategyTranslator() {
    //empty
  }


  /**
   * Decode.
   *
   * @param txnObjects the txn objects
   * @throws DbException the db smartMicroservicio.exception
   */
  public abstract void decode(TransactionObjectsDto txnObjects) throws DbException;


  /**
   * Method encode.
   *
   * @param txnObjects TransactionObjectsDto.
   * @throws DbException   Exception.
   */
  public abstract void encode(TransactionObjectsDto txnObjects)throws DbException;

  /**
   * getInfoForLoggedTxn.
   *
   * @param txnObjects TransactionObjectsDto.
   */
  protected static void getInfoForLoggedTxn(final TransactionObjectsDto txnObjects) {
    translateCommonFields(txnObjects);
  }

  /**
   * buildRandomKey.
   *
   * @return String
   */
  protected String buildRandomKey() {
    Random random = new Random();
    StringBuilder randomKey = new StringBuilder();
    randomKey.append(String.format(CODE, random.nextInt(Number.CODE0X7FFFFFFF.value())));
    randomKey.append(String.format(CODE, random.nextInt(Number.CODE0X7FFFFFFF.value())));
    randomKey.append(String.format(CODE, random.nextInt(Number.CODE0X7FFFFFFF.value())));
    randomKey.append(String.format(CODE, random.nextInt(Number.CODE0X7FFFFFFF.value())));

    return randomKey.toString();
  }

  /**
   * translateCommonFields.
   *
   * @param txnObjects TransactionObjectsDto.
   */
  protected static void translateCommonFields(final TransactionObjectsDto txnObjects) {
    MicroserviceRequest requestGnp = txnObjects.getGNPRequest();
    XmlRequest requestWS = txnObjects.getRequestWS();
    SystemDao stanDao = txnObjects.getSystemDAO();
    requestWS.setType("030100");
    requestWS.setStan(stanDao.findStan(new StanDto(requestGnp.getIdTerminalExterna()),
          txnObjects.getIdOperation()).getNumStan() + "");
    requestWS.setSerialPos(requestGnp.getIdTerminalExterna());

  }

  /**
   * Translate common fields response.
   *
   * @param txnObjects TransactionObjectsDto.
   */
  protected void translateCommonFieldsResponse(final TransactionObjectsDto txnObjects) {
    MicroserviceRequest requestGnp = txnObjects.getGNPRequest();
    XmlRequest requestWS = txnObjects.getRequestWS();
    XmlResponse response = txnObjects.getResponseWS();
    MicroserviceResponseDTO responseGnp = txnObjects.getGNPResponse();
    String rc = response.getHeader().getRespCode();

    String rcProvider = response.getStringParameter("15");
    if (rcProvider == null) {
      rcProvider = rc;
    }
    responseGnp.setCodigoError(SystemSettings.getString("error.code." + rcProvider, rcProvider));
    responseGnp.setDescripcionError(response.getHeader().getRespMessage());
    boolean existError = false;
    if (rcProvider != "" && !rcProvider.equals(NUMBER)) {
      existError = true;
    }
    responseGnp.setExisteError(existError + VACIO);
    responseGnp.setImporte(requestGnp.getImporte());
    responseGnp.setNumTarjeta(requestGnp.getNumTarjeta());
    responseGnp.setNumeroAutorizacion(response.getMessage().getAutCode());
    responseGnp.setNumeroSeguimiento(response.getMessage().getRefSPNum());
    // crear  UniqqueKey
    StringBuilder uniqueKey = new StringBuilder();
    uniqueKey.append(ConversionUtils.paddLeftZero(requestWS.getStan(), Number.CODE6.value()));
    uniqueKey.append(ConversionUtils.paddLeftZero(response.getMessage().getRefSPNum(), Number.CODE12.value()));
    uniqueKey.append(ConversionUtils.paddLeftZero(response.getMessage().getAutCode(), Number.CODE10.value()));

    responseGnp.setUniqueKey(uniqueKey.toString());
    validResponse(txnObjects);
  }

  /**
   * createTxnByDay.
   *
   * @param txnObjects TransactionObjectsDto.
   * @throws DbException DbException.
   */
  protected void createTxnByDay(final TransactionObjectsDto txnObjects) throws DbException {
    MicroserviceRequest requestGnp = txnObjects.getGNPRequest();
    XmlRequest request = txnObjects.getRequestWS();
    TxnByDayDto txnByDay = txnObjects.getTxnByDay();
    txnByDay.setIdOperation(txnObjects.getIdOperation());
    txnByDay.setType(txnObjects.getTxnType().getDbType());
    txnByDay.setTxnOrig(txnObjects.getTxnType().getDbType());
    txnByDay.setProcessingCode(request.getType());
    txnByDay.setTsn(request.getStan());
    txnByDay.setPosSerial(request.getSerialPos());
    txnByDay.setAmount(Integer.parseInt(requestGnp.getImporte().replace(".", "")));
    txnByDay.setStatus(TxnStatus.SENT_TO_VENDOR);
    txnByDay.setTxnDate(Calendar.getInstance().getTime());
    // cifra informacion sensible
    txnByDay.setReference(ServiceEncryptionData.maskAccount(requestGnp.getNumTarjeta()));
    txnByDay.setTxnReferenceDataHash(ServiceEncryptionData.getMaskedSha256Account(
          requestGnp.getNumTarjeta(), txnObjects.getIdOperation()));
    txnByDay.setExpDate(ServiceEncryptionData.encrypt(requestGnp.getFechaVencimiento()));

    String hashTxn = getHashTxn(txnObjects);
    txnByDay.setTxnHash(hashTxn);

    txnObjects.getTxnByDayDAO().createUpdate(txnObjects);
  }

  /**
   * Method updateTxnByDay.
   *
   * @param txnObjects TransactionObjectsDto.
   * @throws DbException as Exception.
   */
  public void updateTxnByDay(final TransactionObjectsDto txnObjects) throws DbException {
    try {
      XmlResponse response = txnObjects.getResponseWS();
      MicroserviceResponseDTO responseGnp = txnObjects.getGNPResponse();
      TxnByDayDto txnByDay = txnObjects.getTxnByDay();

      String rc = response.getHeader().getRespCode();
      String rcProvider = response.getStringParameter("15");
      if (rcProvider == null) {
        rcProvider = rc;
      }
      txnByDay.setResponseCode(rcProvider);
      txnByDay.setResponseMessage(responseGnp.getDescripcionError());
      txnByDay.setApprovalCode(responseGnp.getNumeroAutorizacion());
      txnByDay.setRefSPNum(response.getMessage().getRefSPNum());
      txnByDay.setStatus(TxnStatus.ATTENDED_BY_VENDOR);
      if (rc.equals(NUMBER)) {
        txnByDay.setStatus(TxnStatus.APROVED);
      }

      txnObjects.setResponseCode(rcProvider);
      txnObjects.setResponseMessage(responseGnp.getDescripcionError());
    } catch (Exception ex) {
      Console.writeException("", ex);
    }

  }

  /**
   * getIdOperation.
   *
   * @return String
   */
  public String getIdOperation() {
    return idOperation;
  }

  /**
   * setIdOperation.
   *
   * @param idOperation String
   */
  public void setIdOperation(final String idOperation) {
    this.idOperation = idOperation;
  }

  /**
   * Method validResponse.
   *
   * @param txnObjects "".
   */
  public void validResponse(final TransactionObjectsDto txnObjects) {
    try {
      PosDataDto logonData = txnObjects.getLogonData();
      if (logonData != null) {
        XmlResponse response = txnObjects.getResponseWS();
        String rc = response.getHeader().getRespCode();

        cicloValidResponse(response, rc, logonData, txnObjects);

      }
    } catch (Exception e) {
      Console.writeException(txnObjects.getIdOperation(), e);
    }

  }

  /**
   * Metodo que ejecuta el ciclo de validResponse.
   * @param response as XmlResponse
   * @param rc as String
   * @param logonData as PosDataDto
   * @param txnObjects as Dto
   * */
  public void cicloValidResponse(final XmlResponse response, final String rc,
                final PosDataDto logonData, final TransactionObjectsDto txnObjects) {
    String[] codes = SystemSettings.getString("codes.to.force.logon", "").split(",");

    for (String code : codes) {
      if (code != "" && code.equals(rc)) {
        String msg = response.getHeader().getRespMessage();
        if (msg != null && (msg.contains("LLAVE EXPIRADA, LOGON REQUERIDO")
              || msg.contains("RESPUESTA INVALIDA DE PLATAFORMA")
              || msg.contains("CantDecryptException"))) {
          // forzar logon proxima transacción
          logonData.setStatus(2);
          SystemDao lognDatadao = txnObjects.getSystemDAO();
          lognDatadao.updatePosDataStatus(logonData, txnObjects.getIdOperation());
          break;
        }
      }
    }
  }

  /**
   * getHashTxn.
   *
   * @param txnObjects TransactionObjectsDto
   * @return String.
   */
  protected String getHashTxn(final TransactionObjectsDto txnObjects) {
    TxnByDayDto txnByDay = txnObjects.getTxnByDay();

    return "" + txnByDay.getReference() + (char) Number.CODE0X1C.value()
          + txnByDay.getPosSerial() + (char) Number.CODE0X1C.value()
          + txnByDay.getAmount() + "";
  }

}
