/**
 * ---------------------------------------------------------------------
 * SystemSettings.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 * - --------------------------------------------------------------------
 */

package com.smart.auth.service.impl.error;


import com.smart.auth.config.ConversionUtils;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.logs.SystemLog;
import org.apache.commons.beanutils.BeanUtils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Class SystemSettings.
 *
 * @author Jose A Rodriguez M
 */
public class SystemSettings {
  /**
   * SystemSettings.
   */
  private static SystemSettings settings = null;
  /**
   * Integer.
   */
  private Integer id = 1;
  /**
   * Charset.
   */
  private static final Charset CHARSET = Charset.forName("UTF-8");

  /**
   * Valor constante de properties.
   */
  private static final String PROPERTIESSYSTEM = "/system.properties";
  /**
   * Properties.
   */
  private static Properties bundle;
  /**
   * ERRORPARAMETRO.
   */
  private static final String ERRORPARAMETRO = "ERROR AL OBTENER PARAMETRO ";
  /**
   * ARCHIVO.
   */
  private static final String ARCHIVO = " ARCHIVO : system.properties";

  /**
   * Contructor empty.
   */
  protected SystemSettings() {
    //empty
  }

  /**
   * Constructor set settings.
   *
   * @return SystemSettings.
   */
  public static SystemSettings getInstance() {
    return settings;
  }

  /**
   * Constructor set settings.
   *
   * @param stn as SystemSettings.
   */
  public static void getInstance(final SystemSettings stn) {
    settings = stn;
  }

  /**
   * Load SystemConstants properties file.
   */
  public static synchronized void load() {
    try {
      SystemLog.writeEntry("LoadSettings", "INICIANDO ARCHIVO DE PROPIEDADES system.properties");
      bundle = new Properties();
      bundle.load(SystemSettings.class.getResourceAsStream(PROPERTIESSYSTEM));
    } catch (Exception ex) {
      Console.writeException("", ex);
      SystemLog.writeEntry("", "ERROR AL INICIALIZAR ARCHIVO DE PROPIEDADES system.properties");
      SystemLog.writeException(ex);
    }
  }

  /**
   * Method loadSettingsFile.
   */
  public static void loadSettingsFile() {
    String pathFile = new File("").getAbsolutePath() + File.separator + "src/main/resources"
          + PROPERTIESSYSTEM;

    File file = new File(pathFile);

    try (FileInputStream fis = new FileInputStream(file)) {
      SystemLog.writeEntry("", "INICIANDO ARCHIVO DE PROPIEDADES system.properties");
      bundle = new Properties();
      bundle.load(SystemSettings.class.getResourceAsStream(PROPERTIESSYSTEM));

      if (file.exists()) {
        settings = new SystemSettings();

        Properties props = new Properties();
        props.load(fis);
        Iterator<Map.Entry<Object, Object>> iterator = props.entrySet().iterator();
        Map<String, Object> map = ciclo(iterator);
        BeanUtils.populate(settings, map);
      }
    } catch (Exception e) {
      Console.writeException(e);
    }
  }


  /**
   * Método agregado para la separación del ciclo.
   *
   * @param iterator the iterator
   * @return the map
   */
  public static Map<String, Object> ciclo(final Iterator<Map.Entry<Object, Object>> iterator) {
    Map.Entry<Object, Object> element;
    Map<String, Object> map = new HashMap<>();
    while (iterator.hasNext()) {
      element = iterator.next();
      String key = (String) element.getKey();
      if (isCalendarType(key)) {
        Calendar calValue = strToCalendar((String) element.getValue());
        map.put(key, calValue);
      } else {
        map.put((String) element.getKey(), element.getValue());
      }
    }
    return map;
  }

  /**
   * getSettings.
   *
   * @return Properties
   */
  public static Properties getSettings() {
    validProperties();
    return bundle;
  }

  /**
   * Get a string constant from SystemConstants properties file.
   *
   * @param stNamKey Constant identifier
   * @return Value from SystemConstants properties file or Undefined
   */
  public static String getString(final String stNamKey) {
    String stRet = "";
    validProperties();
    try {
      stRet = bundle.getProperty(stNamKey).trim();
    } catch (Exception ex) {
      Console.writeException("", ex);
      SystemLog.writeEntry("", ERRORPARAMETRO + stNamKey + ARCHIVO);
      stRet = null;
    }
    return stRet;
  }


  /**
   * Method getString.
   *
   * @param stNamKey     String.
   * @param defaultValue String.
   * @return String.
   */
  public static String getString(final String stNamKey, final String defaultValue) {
    String stRet;
    validProperties();
    try {
      stRet = bundle.getProperty(stNamKey).trim();
    } catch (Exception ex) {
      Console.writeException("", ex);
      SystemLog.writeEntry("", ERRORPARAMETRO + stNamKey + ARCHIVO);
      stRet = defaultValue;
    }
    return stRet;
  }

  /**
   * getValue Properties.
   *
   * @param propirdad String.
   * @return String.
   */
  private static String getValue(final String propirdad) {
    return System.getenv(propirdad).trim();
  }


  /**
   * validProperties.
   */
  public static void validProperties() {
    if (bundle == null) {
      try {
        load();
      } catch (Exception ex) {
        Console.writeException("", ex);
        SystemLog.writeException(ex);
      }
    }
  }

  /**
   * Get an int constant from SystemConstants properties file.
   *
   * @param stNamKey     Constant identifier
   * @param defaultValue int
   * @return Value from SystemConstants properties file or Undefined
   */
  public static int getInt(final String stNamKey, final int defaultValue) {
    String stRet = "";
    int valRet;
    validProperties();
    try {
      stRet = bundle.getProperty(stNamKey).trim();
      valRet = Integer.parseInt(stRet);
    } catch (Exception ex) {
      Console.writeException("", ex);
      SystemLog.writeEntry("", ERRORPARAMETRO + stNamKey + ARCHIVO);
      valRet = defaultValue;
    }
    return valRet;
  }

  /**
   * Method getAsBoolean.
   *
   * @param stNamKey     String.
   * @param defaultValue boolean.
   * @return boolean.
   */
  public static boolean getAsBoolean(final String stNamKey, final boolean defaultValue) {
    String stRet = "";
    boolean valRet = defaultValue;
    validProperties();
    try {
      stRet = bundle.getProperty(stNamKey).trim();
      if (stRet != null && "1".equals(stRet)) {
        valRet = true;
      }
    } catch (Exception ex) {
      Console.writeException("", ex);
      SystemLog.writeEntry("", ERRORPARAMETRO + stNamKey + ARCHIVO);
    }
    return valRet;
  }

  /**
   * strToCalendar.
   *
   * @param cal String.
   * @return Calendar.
   */
  public static Calendar strToCalendar(final String cal) {
    return ConversionUtils.stringToCalendar(cal, "0000");
  }

  /**
   * Method isCalendarType calendar.
   *
   * @param key String.
   * @return boolean.
   */
  public static boolean isCalendarType(final String key) {
    boolean result;
    switch (key) {
      case "nextClear4AmountByMonth":
      case "nextClear4AmountByDay":
      case "nextDailyMaintence":
      case "lastDailyMantenance":
        result = true;
        break;
      default:
        result = false;
        break;
    }

    return result;
  }

  /**
   * getId.
   *
   * @return Integer
   */
  public Integer getId() {
    return id;
  }

  /**
   * hashCode.
   *
   * @return int.
   */
  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }

  /**
   * equals.
   *
   * @param object Object.
   * @return boolean.
   */
  @Override
  public boolean equals(final Object object) {
    if (!(object instanceof SystemSettings)) {
      return false;
    }
    SystemSettings other = (SystemSettings) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  /**
   * toString package.
   *
   * @return String.
   */
  @Override
  public String toString() {
    return "com.smartbt.gnp.service[id=" + id + "]";
  }

  /**
   * getCHARSET.
   *
   * @return Charset.
   */
  public static Charset getCHARSET() {
    return CHARSET;
  }

  /**
   * getLocale.
   *
   * @return Locale.
   */
  public static Locale getLocale() {
    return Locale.getDefault();
  }

}
