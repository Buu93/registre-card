/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.service.impl.operation;

import com.smart.auth.service.RequestServices;

import java.util.HashMap;

/**
 * Class Operations.
 * @author Carlos López
 */
public final class Operations {
  /** * Operations. */
  private static Operations instance;
  /** * HashMap<String, RequestServices>. */
  private final HashMap<String, RequestServices> mapController;

  /**
   * Operations.
   */
  private Operations() {
    mapController = new HashMap();

    // Venta en línea
    mapController.put("60", new O01Aperture());
    // Devolución
    mapController.put("62", new O02Refund());
    // Reverso
//    mapController.put("61", new O05Reverse(TransactionType.REVERSAL));
    // Cancelación
//    mapController.put("63", new O03Void(TransactionType.VOID));

  }

  /**
   * Method Operations.
   * @return Operations.
   */
  public static synchronized Operations getInstance() {
    if (instance == null) {
      instance = new Operations();
    }

    return instance;
  }

  /**
   * getController.
   * @param type String.
   * @return RequestServices.
   */
  public RequestServices getController(final String type) {
    return mapController.get(type);
  }
}
