/**
 * ****************************************************************
 * O04Logon.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 - *****************************************************************
 */

package com.smart.auth.service.impl.operation;



import com.smart.auth.config.TemporalUtils;
import com.smart.auth.config.cipher.ServiceEncryptionData;
import com.smart.auth.dao.SystemDao;
import com.smart.auth.exception.CodeMessageException;
import com.smart.auth.exception.CommonOperationException;
import com.smart.auth.exception.SystemException;
import com.smart.auth.exception.TimeOutException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.logs.SystemLog;
import com.smart.auth.exception.message.CoreErrorServicesTable;
import com.smart.auth.model.constants.ResponseCode;
import com.smart.auth.model.dto.PosDataDto;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.service.RequestServices;
import com.smart.auth.service.clientmessage.MessageController;
import com.smart.auth.service.clientmessage.XmlResponse;
import com.smart.auth.service.impl.error.SystemSettings;
import com.smart.auth.service.impl.translationstrategies.AppStrategyTranslator;
import com.smart.auth.service.impl.translationstrategies.LogonStrategy;
import com.smart.auth.model.enumutils.Number;

import java.util.Calendar;

/**
 * Class O04Logon.
 * @author Jose A Rodriguez M
 */
public class O04Logon implements RequestServices {
  /** * TransactionType. */
  private String idOperation = "";

  /**
   * Method OperationResponse.
   *
   * @param txnObjects TransactionObjectsDto.
   * @param idOperation String.
   * @return OperationResponse.
   * @throws CodeMessageException the code message exception
   */
  @Override
  public OperationResponse processRequest(final TransactionObjectsDto txnObjects,
                           final String idOperation) throws CodeMessageException {
    this.idOperation = idOperation;
    XmlResponse response;
    OperationResponse responseGnp = CommonOperation.getOperationResponse(txnObjects);
    SystemDao dao = txnObjects.getSystemDAO();

    try {
      // Agregar metodo para buscar lastServerKey por punto de venta si no existe realizar logon
      PosDataDto logonData = dao.findPosData(new PosDataDto(txnObjects.getIdClient()), txnObjects.getIdOperation());
      txnObjects.setLogonData(logonData);

      if (validLogon(logonData, txnObjects)) {
        // asigna lastServerKey a objeto de transaccion
        txnObjects.setLastServerKey(logonData.getLastServerKey());
        txnObjects.getRequestWS().setLastServerKey(logonData.getLastServerKey());
        txnObjects.setTimeout(logonData.getTimeOut());
        responseGnp.getResponse().setCodigoError("00");
        responseGnp.getResponse().setDescripcionError("Aprobada");

        txnObjects.setLogonData(logonData);
        return responseGnp;
      }

      // asigna datos para Logon
      TransactionObjectsDto newtxnObjects = txnObjects;
      newtxnObjects.setGNPRequest(txnObjects.getGNPRequest());
      newtxnObjects.getGNPRequest().setIdTerminalExterna(logonData.getSerialPos());
      newtxnObjects.setAppgnpXml(txnObjects.getAppgnpXml());
      newtxnObjects.setLogonData(logonData);

      //2.Hacer la conversión de campos
      AppStrategyTranslator strategy = new LogonStrategy();
      strategy.decode(newtxnObjects);
      //3.Crear el mensaje de envio
      MessageController controller = new MessageController(idOperation);

      //4.Enviar petición a la plataforma
      response = controller.sendAndWaitForResponse(newtxnObjects);
      newtxnObjects.setResponseWS(response);
      //5.Recibir Respuesta
      //6.Decodificar respuesta y convertirla a respuesta para GNP
      strategy.encode(newtxnObjects);

      // asigna lastServerKey a objeto de transaccion
      txnObjects.setLastServerKey(newtxnObjects.getLastServerKey());
      txnObjects.getRequestWS().setLastServerKey(newtxnObjects.getLastServerKey());


      // valida response code y response msg
      if (newtxnObjects.getResponseWS().getHeader().getRespCode().equals(ResponseCode.OK)) {
        validKey(newtxnObjects, txnObjects);
        // Genera objeto datos de logon
        int timeOut = newtxnObjects.getResponseWS().getIntParameter("11", Number.CODE60.value());
        PosDataDto newLogonData = new PosDataDto();

        newLogonData.setId(logonData.getId());
        newLogonData.setClientId(SystemSettings.getString("sp.clientId"));
        newLogonData.setSerialPos(newtxnObjects.getRequestWS().getSerialPos());
        newLogonData.setLastServerKey(newtxnObjects.getLastServerKey());
        newLogonData.setTimeOut(timeOut);
        newLogonData.setStatus(1);
        dao.updatePosData(newLogonData, txnObjects.getIdOperation());
        txnObjects.setLogonData(newLogonData);
      } else {
        throw new SystemException(newtxnObjects.getResponseWS().getHeader().getRespCode(),
              newtxnObjects.getResponseWS().getHeader().getRespMessage());
      }

      responseGnp.setResponse(txnObjects.getGNPResponse());

    } catch (TimeOutException ex) {
      CommonOperation.buildException(responseGnp, ex.getRespCode(), ex.getRespMessage());
      Console.writeException(idOperation, ex);
    } catch (CommonOperationException ex) {
      CommonOperation.buildException(responseGnp, ex.getRespCode(), ex.getRespMessage());
      Console.writeException(idOperation, ex);
    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
      CommonOperation.buildException(responseGnp, ResponseCode.GRAL_ERROR,
            CoreErrorServicesTable.GRAL_ERROR.getErrorMessage());
    }
    return responseGnp;
  }

  /**
   * valid Key.
   * @param newtxnObjects as TransactionObjectsDto.
   * @param txnObjects as TransactionObjectsDto.
   * @throws SystemException SystemException.
   */
  public void validKey(final TransactionObjectsDto newtxnObjects,
                       final TransactionObjectsDto txnObjects) throws SystemException {
    if (newtxnObjects.getLastServerKey() == null
          || newtxnObjects.getLastServerKey().isEmpty()) {
      SystemLog.writeEntry(txnObjects.getIdOperation(), "**** Error en llave de logon");
      throw new SystemException(CoreErrorServicesTable.INVALID_RESPONSE);
    }
  }


  /**
   * Method validLogon.
   * @param posData PosDataDto.
   * @param txnObjects TransactionObjectsDto.
   * @return Boolean.
   * @throws CommonOperationException CommonOperationException.
   */
  public Boolean validLogon(final PosDataDto posData,
                            final TransactionObjectsDto txnObjects) throws CommonOperationException {
    Boolean boleanRet;
    // valida pos
    if (posData == null) {
      throw new SystemException(CoreErrorServicesTable.INVALID_POS);
    }
    // valida usuario y contraseña
    if (posData.getUserName() == null || posData.getUserName().isEmpty()
          || posData.getUserP() == null || posData.getUserP().isEmpty()) {
      throw new SystemException(CoreErrorServicesTable.POS_INCOMPLETE_PARAMETERS);
    }

      // valida fecha de ultimo logon sea de la misma fecha en curso, si ya es diferente se debe
      // generar logon
      String logonlastDate = posData.getLastDate();
      String nowDate = TemporalUtils.getDate("dd/MM/yyyy", Calendar.getInstance());
      // Si el estatus es igual a 2 se tiene que forzar el logon
      // o si la fecha de ultimo logon es diferente a la actual se debe hacer logon
      // o si la llave no esta configurada
      boleanRet = validSatus(posData, logonlastDate, nowDate);


    /**
     * status:1 = datos en claro
     * status:2 = datos cifrados
     */
    if (posData.getStatusData() == 1) {
      SystemDao dao = txnObjects.getSystemDAO();
      posData.setStatusData(2);
      posData.setUserP(ServiceEncryptionData.encrypt(posData.getUserP()));
      if (!dao.updatePosDataUserP(posData, idOperation)) {
        Console.writeln(Console.Level.DEBUG, idOperation, "*** Error al actualizar userP.");
      }
    }

    return boleanRet;
  }

  /**
   * valid status.
   * @param posData as PosDataDto.
   * @param logonlastDate as String.
   * @param nowDate as String.
   * @return as boolean.
   */
  public boolean validSatus(final PosDataDto posData, final String logonlastDate, final String nowDate) {

    return validaStatusA(posData, logonlastDate, nowDate)
          && validaStatusB(posData)
          ? true : false;
  }

  /**
   * validaStatusA.
   * @param posData as PosDataDto
   * @param logonlastDate as String
   * @param nowDate as String
   * @return boolean
   */
  public boolean validaStatusA(final PosDataDto posData, final String logonlastDate, final String nowDate) {
    return posData.getStatus() == 2 || !logonlastDate.equals(nowDate) ? false : true;

  }

  /**
   * validaStatusB.
   * @param posData as PosDataDto
   * @return boolean
   */
  public boolean validaStatusB(final PosDataDto posData) {
    return (posData.getLastServerKey() == null || posData.getLastServerKey().isEmpty()) ? false : true;
  }

}
