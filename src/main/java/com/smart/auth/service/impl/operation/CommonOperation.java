/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.service.impl.operation;


import com.smart.auth.exception.CodeMessageException;
import com.smart.auth.exception.DbException;
import com.smart.auth.exception.GeneralException;
import com.smart.auth.exception.MicroserviceException;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.message.MessageRespose;
import com.smart.auth.model.MicroserviceRequest;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.service.clientmessage.MessageController;
import com.smart.auth.service.clientmessage.XmlResponse;
import com.smart.auth.service.impl.translationstrategies.AppStrategyTranslator;

/**
 * Class CommonOperation.
 * @author Carlos Lopez
 */
public final class CommonOperation {

  /**
   * Method empty.
   */
  private CommonOperation() {
    //empty
  }

  /**
   * ethod getOperationResponse.
   * @param txnObjects TransactionObjectsDto.
   * @return OperationResponse.
   */
  public static OperationResponse getOperationResponse(final TransactionObjectsDto txnObjects) {
    OperationResponse response = new OperationResponse(txnObjects.getIdOperation());
    buildDefaultParams(response, txnObjects);
    return response;
  }

  /**
   * buildDefaultParams.
   * @param response OperationResponse.
   * @param txnObjects TransactionObjectsDto.
   */
  private static void buildDefaultParams(final OperationResponse response,
                                         final TransactionObjectsDto txnObjects) {
    MicroserviceRequest requestGnp = txnObjects.getGNPRequest();
    response.getResponse().setImporte(requestGnp.getImporte());
    response.getResponse().setNumTarjeta(requestGnp.getNumTarjeta());
  }


  /**
   * buildCodeAndMessage.
   * @param response OperationResponse.
   * @param code String.
   * @param message String.
   */
  public static void buildCodeAndMessage(final OperationResponse response, final String code, final String message) {
    response.getResponse().setCodigoError(code);
    response.getResponse().setDescripcionError(message);
  }

  /**
   * buildException.
   * @param response OperationResponse.
   * @param code String.
   * @param message String.
   * @throws CodeMessageException CodeMessageException.
   */
  public static void buildException(final OperationResponse response,
                                    final String code, final String message) throws CodeMessageException {
    buildCodeAndMessage(response, code, message);
    throw new CodeMessageException(response, message);
  }

  /**
   * generaResponse.
   * @param response as OperationResponse
   * @param strategy as AppStrategyTranslator
   * @param txnObjects as TransactionObjectsDto
   * @param idOperation as String
   * @return OperationResponse
   * @throws MicroserviceException MicroserviceException
   */
  public static OperationResponse generaResponse(final OperationResponse response,
                                                 final AppStrategyTranslator strategy, final TransactionObjectsDto txnObjects,
                                                 final String idOperation)throws MicroserviceException {
    try {
      txnObjects.getRequestWS().cleanCipherTextParam();
      strategy.decode(txnObjects);
      XmlResponse responsePlat;
      MessageController controller = new MessageController(txnObjects.getIdOperation());
      responsePlat = controller.sendAndWaitForResponse(txnObjects);
      txnObjects.setResponseWS(responsePlat);
      strategy.encode(txnObjects);
      response.setResponse(txnObjects.getGNPResponse());

    } catch (DbException e) {
       Console.writeException(idOperation, e);
       throw new MicroserviceException(new MessageRespose(e.getErrorMsg(), e.getErrorCode()), idOperation);
    } catch (GeneralException e) {
      Console.writeException(idOperation, e);
      throw new MicroserviceException(new MessageRespose(e.getRespMessage(), e.getRespCode()), idOperation);
    }
    return response;

  }
}
