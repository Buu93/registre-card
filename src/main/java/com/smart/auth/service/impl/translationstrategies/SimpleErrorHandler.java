package com.smart.auth.service.impl.translationstrategies;

/**
------------------------------------------------------------------
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
-------------------------------------------------------------------
 */

import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.logs.IdTraceManager;

/**
 * Class SimpleErrorHandler.
 * @author Jose A Rodriguez M
 */
public class SimpleErrorHandler implements Thread.UncaughtExceptionHandler {

  /** * variable idOperation. */
  private final String idOperation;
  /** * Thread.UncaughtExceptionHandler. */
  private final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler;

  /**
   * Constructor set variables.
   * @param idOperation String.
   * @param defaultUncaughtExceptionHandler Thread.UncaughtExceptionHandler.
   */
  public SimpleErrorHandler(final String idOperation,
                            final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler) {
    this.idOperation = idOperation;
    this.defaultUncaughtExceptionHandler = defaultUncaughtExceptionHandler;
  }


  /**
   * Method exceptions.
   * @param t Thread.
   * @param e Throwable.
   */
  @Override
  public void uncaughtException(final Thread t, final Throwable e) {
    Console.writeln(Console.Level.ERROR, idOperation, t.getName() + " - Excepción no controlada: "
          + e.getClass().getCanonicalName());
    String threadIdOp = IdTraceManager.getIdTrace();
    Console.writeException(threadIdOp != null ? threadIdOp : idOperation, e);
    defaultUncaughtExceptionHandler.uncaughtException(t, e);
  }
}
