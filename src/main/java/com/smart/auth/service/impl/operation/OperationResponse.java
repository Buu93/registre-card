/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.service.impl.operation;

import com.smart.auth.model.dto.MicroserviceResponseDTO;

import java.io.Serializable;

/**
 * Class OperationResponse.
 * @author Carlos Lopez
 */
public class OperationResponse implements Serializable {

  private static final long serialVersionUID = 1420672609912364060L;

  /** * idOperation. */
  private String idOperation;
  /** * MicroserviceResponseDTO. */
  private MicroserviceResponseDTO response = new MicroserviceResponseDTO();

  /**
   * Constructor set MicroserviceResponseDTO.
   */
  public OperationResponse() {
    this.response = new MicroserviceResponseDTO();
  }

  /**
   * OperationResponse.
   * @param idOperation String.
   */
  public OperationResponse(final String idOperation) {
    this.idOperation = idOperation;
    this.response = new MicroserviceResponseDTO();
  }

  /**
   * Method getResponse.
   * @return the response
   */
  public MicroserviceResponseDTO getResponse() {
    if (this.response == null) {
      this.response = new MicroserviceResponseDTO();
    }
    return response;
  }

  /**
   * Method setResponse.
   * @param response the response to set
   */
  public void setResponse(final MicroserviceResponseDTO response) {
    this.response = response;
  }

  /**
   * Getter idOperation.
   * @return String
   * */
  public String getIdOperation() {
    return idOperation;
  }
}
