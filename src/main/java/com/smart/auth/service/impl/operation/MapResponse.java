package com.smart.auth.service.impl.operation;


import com.smart.auth.exception.AppMicroserviceException;
import com.smart.auth.service.RequestServices;

/**
 * Class MapResponse.
 */
public class MapResponse {
  /**  * variable MapResponse.   */
  private static MapResponse instance;

  /**
   * Method empty.
   */
  public MapResponse() {
    //Used test
  }

  /**
   * setter Instance.
   * @param instance as MapResponse
   * */
  public void setInstance(final MapResponse instance) {
    synchronized (this) {
      MapResponse.instance = instance;
    }
  }

  /**
   * Method getInstance.
   * @return MapResponse.
   */
  public static synchronized MapResponse getInstance() {
    if (instance == null) {
      instance = new MapResponse();
    }

    return instance;
  }

  /**
   * Method getResponse.
   * @param optype String.
   * @return RequestServices.
   * @throws AppMicroserviceException MicroserviceException.
   */
  public RequestServices getResponse(final String optype) throws AppMicroserviceException {
    if (Operations.getInstance().getController(optype) == null) {
      throw new AppMicroserviceException(AppMicroserviceException.Error.INVALID_OPERATION);
    }

    return Operations.getInstance().getController(optype);
  }
}
