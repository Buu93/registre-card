/**
 * ****************************************************************
 * RefundStrategy.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 - *****************************************************************
 */

package com.smart.auth.service.impl.translationstrategies;

import com.smart.auth.config.TemporalUtils;
import com.smart.auth.exception.DbException;
import com.smart.auth.model.MicroserviceRequest;
import com.smart.auth.model.constants.ProcessingCode;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.model.enumutils.TransactionType;
import com.smart.auth.service.clientmessage.MessageUtil;
import com.smart.auth.service.clientmessage.XmlRequest;
import com.smart.auth.service.impl.error.SystemSettings;
import com.smart.auth.model.enumutils.Number;

import java.util.Calendar;

/**
 * Class RefundStrategy.
 * @author Jose A Rodriguez M.
 */
public class ApertureStrategy extends AppStrategyTranslator {

  /** * TransactionType. */
  private final TransactionType txnType;

  /**
   * Contructor set TransactionType.REFUND.
   */
  public ApertureStrategy() {
    this.txnType = TransactionType.APERTURE;
  }


  /**
   * Method decode.
   * @param txnObjects TransactionObjectsDto.
   * @throws DbException DbException.
   */
  @Override
  public void decode(final TransactionObjectsDto txnObjects) throws DbException {
    setIdOperation(txnObjects.getIdOperation());
    txnObjects.setTxnType(txnType);
    translateFields(txnObjects);
//    createTxnByDay(txnObjects);
  }

  /**
   * translateFields.
   * @param txnObjects TransactionObjectsDto.
   */
  private static void translateFields(final TransactionObjectsDto txnObjects) {
    translateCommonFields(txnObjects);
    MicroserviceRequest requestGnp = txnObjects.getGNPRequest();
    XmlRequest request = txnObjects.getRequestWS();
    request.setType(ProcessingCode.APERTURE);
    request.setSerialPos(txnObjects.getGNPRequest().getIdTerminalExterna());
    //Sensible data
    MessageUtil.addValue(requestGnp.getNumTarjeta(), "00", request, true);
    String expDate = requestGnp.getFechaVencimiento();
    if (expDate == null || expDate.isEmpty()) {
      Calendar now = Calendar.getInstance();
      now.add(Calendar.MONTH, SystemSettings.getInt("num.month.to.add.exp.date", Number.CODE3.value()));
      expDate = TemporalUtils.getDate("YYMM", now);
    }
    String appCode = requestGnp.getNumeroAutorizacion();
    if (appCode == null || appCode.isEmpty()) {
      appCode = "000000";
    }
    MessageUtil.addValue(expDate, "01", request, true);
    MessageUtil.addValue(requestGnp.getCodigoSeguridad(), "04", request, true);
    MessageUtil.addValue(request.getStan(), "19", request, true);
    MessageUtil.addValue(requestGnp.getImporte().replace(".", ""), "05", request, false);
    MessageUtil.addValue(SystemSettings.getString("entry.mode", "01"), "16", request, false);
    MessageUtil.addValue("08", "17", request, false);
    MessageUtil.addValue(SystemSettings.getString("country.code", "MX"), "64", request, false);
    MessageUtil.addValue(SystemSettings.getString("currency.code." + requestGnp.getMoneda(), "484"
    ), "65", request, false);
    MessageUtil.addValue(SystemSettings.getString("terminal.type", "3"), "68", request, false);
    request.setAutCode(appCode);

  }

  /**
   * Method encode.
   *
   * @param txnObjects TransactionObjectsDto.
   * @throws DbException the db smartMicroservicio.exception
   */
  @Override
  public void encode(final TransactionObjectsDto txnObjects) throws DbException {
    translateCommonFieldsResponse(txnObjects);
//    updateTxnByDay(txnObjects);
  }
}
