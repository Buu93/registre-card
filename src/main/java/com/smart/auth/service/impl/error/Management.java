/**
 * ---------------------------------------------------------------------
 * Management.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 * - ---------------------------------------------------------------------
 */

package com.smart.auth.service.impl.error;

import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.dto.TransactionObjectsDto;
import com.smart.auth.service.impl.StatisticService;
import com.smart.auth.service.impl.translationstrategies.SimpleErrorHandler;

import java.io.File;
import java.util.*;


/**
 * Class Management.
 * @author Jose A Rodriguez M
 */
public final class Management {
  /** * systemVersion. */
  private static String systemVersion = "1.0.1";
  /** * realPath. */
  private static String realPath;
  /** * basePath. */
  private static String basePath;
  /** * urlApp. */
  private static String urlApp;
  /** * StatisticService. */
  private static StatisticService statServ = new StatisticService();
  /** * SYSTEMETTING. */
  private static final String SYSTEMETTING = "SystemSettings:getPath";
  /** *SD.  */
  private static final String SD = "S/D";

  /** * Constructor empty. */
  private Management() {
    //empty
  }

  /**
   * Method start.
   * @param idOperation String.
   * @param realPath String.
   * @param basePath String.
   */
  public static void start(final String idOperation, final String realPath, final String basePath) {

    Thread.setDefaultUncaughtExceptionHandler(new SimpleErrorHandler(idOperation,
          Thread.getDefaultUncaughtExceptionHandler()));
    statServ.setStartDate(Calendar.getInstance().getTime());
    Management.realPath = realPath;
    Management.basePath = basePath;
    System.setProperty("appGNP.Dir", getPathFiles());
    SystemSettings.loadSettingsFile();
    Console.initialize();

    Console.writeln(Console.Level.INFO, idOperation,
          "Iniciando aplicación appGNP versión: " + systemVersion);

    Console.writeln(Console.Level.INFO, idOperation,
          "Configuración regional local: " + Locale.getDefault().getDisplayName());
    Locale.setDefault(new Locale("es", "MX"));
    Console.writeln(Console.Level.INFO, idOperation,
          "Configuración regional actualizada: " + Locale.getDefault().getDisplayName());
    try {
      startApplication(idOperation);
    } catch (Exception ex) {
      Console.writeException(idOperation, ex);
      Console.writeln(Console.Level.DEBUG, idOperation, "Aplicación no iniciada");
    }
  }

  /**
   * startApplication.
   * @param idOperation String
   */
  private static void startApplication(final String idOperation) {
    startDaemons(idOperation);
  }

  /**
   * startDaemons.
   * @param idOperation String.
   */
  private static synchronized void startDaemons(final String idOperation) {
    Console.writeln(Console.Level.INFO, idOperation, "Aplicación appGNP iniciada");
  }

  /**
   * acceptTxn.
   */
  public static void acceptTxn() {
    statServ.addTransaction();
    statServ.addAliveTransaction();
  }

  /**
   * endTxn.
   * @param txnObjects TransactionObjectsDto.
   */
  public static void endTxn(final TransactionObjectsDto txnObjects) {
    statServ.removeAliveTransaction();
    statServ.hit(txnObjects.getAtentionTime());
  }

  /**
   * Method getSystemProperties.
   *
   * @param reload boolean.
   * @return Properties
   */
  public static Properties getSystemProperties(final boolean reload) {
    if (reload) {
      SystemSettings.load();
    }

    Properties props = SystemSettings.getSettings();

    Iterator<Map.Entry<Object, Object>> it = props.entrySet().iterator();

    while (it.hasNext()) {
      Map.Entry<Object, Object> e = it.next();
      if (!e.getValue().toString().equalsIgnoreCase(SD)) {
        props.put(e.getKey(), getValue(e.getValue().toString()));
      }
    }

    return props;
  }

  /**
   * getValue properties by value.
   * @param propirdad String.
   * @return String.
   */
  private static String getValue(final String propirdad) {
    return System.getenv(propirdad).trim();
  }


  /**
   * getPhysicalPath.
   * @return String.
   */
  public static String getPhysicalPath() {
    return realPath;
  }

  /**
   * getBasePath.
   * @return String
   */
  public static String getBasePath() {
    return basePath;
  }

  /**
   * getUrlApp.
   * @return String.
   */
  public static String getUrlApp() {
    return urlApp;
  }

  /**
   * setUrlApp.
   * @param urlApp String.
   */
  public static void setUrlApp(final String urlApp) {
    Management.urlApp = urlApp;
  }

  /**
   * getSystemVersion.
   * @return String.
   */
  public static String getSystemVersion() {
    return systemVersion;
  }

  /**
   * getStatServ.
   * @return StatisticService.
   */
  public static StatisticService getStatServ() {
    return statServ;
  }

  /**
   * Method getPathFiles.
   * @return String.
   */
  public static String getPathFiles() {
    String stRet = "";
    try {
      stRet = new File("").getAbsolutePath() + File.separator + "config";

    } catch (Exception e) {
      Console.writeException(SYSTEMETTING, e);
    }

    Console.writeln(Console.Level.INFO, SYSTEMETTING, "PATH:" + stRet);

    return stRet;
  }
}
