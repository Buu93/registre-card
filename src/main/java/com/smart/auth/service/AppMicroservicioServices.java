package com.smart.auth.service;


import com.smart.auth.model.MicroserviceRequest;
import com.smart.auth.model.dto.MicroserviceResponseDTO;

import javax.servlet.http.HttpServletRequest;

/**
 * Interfaz AppMicroservicioServices.
 */
public interface AppMicroservicioServices {
  /**
   * Proceso servicio Gnp.
   * @param dto MicroserviceRequest.
   * @param request HttpServletRequest.
   * @return MicroserviceResponseDTO.
   */
  MicroserviceResponseDTO processRequest(MicroserviceRequest dto, HttpServletRequest request);


}
