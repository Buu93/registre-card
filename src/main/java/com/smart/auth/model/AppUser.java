package com.smart.auth.model;

public class AppUser {

  private String Clienteid;
  private String username;
  private String idmodulo;

  public AppUser(String clienteid, String username, String idmodulo, String pagina, String password) {
    Clienteid = clienteid;
    this.username = username;
    this.idmodulo = idmodulo;
    this.pagina = pagina;
    this.password = password;
  }

  public AppUser(){

  }

  public String getIdmodulo() {
    return idmodulo;
  }

  public void setIdmodulo(String idmodulo) {
    this.idmodulo = idmodulo;
  }

  public String getPagina() {
    return pagina;
  }

  public void setPagina(String pagina) {
    this.pagina = pagina;
  }

  private String pagina;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  private String password;


  public String getClienteid() {
    return Clienteid;
  }

  public void setClienteid(String clienteid) {
    Clienteid = clienteid;
  }
}
