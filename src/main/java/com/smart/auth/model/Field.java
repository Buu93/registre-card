/**
 * ****************************************************************
 * Field.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 - *****************************************************************
 */

package com.smart.auth.model;

import java.io.Serializable;

/**
 * Class Field.
 * @author Jose A Rodriguez M
 */
public class Field implements Serializable {

  private static final long serialVersionUID = 1420672609912364060L;

  /** * name. */
  private String name;
  /** * type. */
  private Type type;
  /** * dataType. */
  private DataType dataType;
  /** * noNull. */
  private Boolean noNull;
  /** * value. */
  private String value;

  /** * values type data in enum. */
  public enum DataType {
    /** * STRING. */
    STRING,
    /** * INTEGER. */
    INTEGER,
    /** * NUMBER. */
    NUMBER,
    /** * DATE. */
    DATE,
    /** * TIME. */
    TIME,
    /** * DATETIME. */
    DATETIME,
    /** * CDATA. */
    CDATA,
    /** * IMAGE. */
    IMAGE,
    /** * BOOLEAN. */
    BOOLEAN,
    /** * EMAIL. */
    EMAIL,
    /** * PHONE. */
    PHONE;
  }

  /**
   * parameter.
   */
  public enum Type {
    /** * HEADER. */
    HEADER,
    /** * PARAM. */
    PARAM,
    /** * TEXT. */
    TEXT;
  }

  /**
   * Constructor used test.
   * @param name String.
   * @param type Type.
   * @param dataType DataType.
   */
  public Field(final String name, final Type type, final DataType dataType) {
    this.name = name;
    this.type = type;
    this.dataType = dataType;
    this.noNull = true;
  }

  /**
   * Method Field with data null.
   * @param name String.
   * @param type Type.
   * @param dataType DataType.
   * @param noNull Boolean.
   */
  public Field(final String name, final Type type, final DataType dataType, final Boolean noNull) {
    this.name = name;
    this.type = type;
    this.dataType = dataType;
    this.noNull = noNull;
  }

  /**
   * used test values.
   * @param name String.
   * @param type Type.
   * @param value String.
   */
  public Field(final String name, final Type type, final String value) {
    this.name = name;
    this.type = type;
    this.value = value;
  }

  /**
   * Contructor empty.
   */
  public Field() {
    //empty
  }

  /**
   * getName.
   * @return String.
   */
  public String getName() {
    return name;
  }

  /**
   * setName.
   * @param name String.
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * getType.
   * @return Type.
   */
  public Type getType() {
    return type;
  }

  /**
   * setType.
   * @param type Type.
   */
  public void setType(final Type type) {
    this.type = type;
  }

  /**
   * getDataType.
   * @return DataType.
   */
  public DataType getDataType() {
    return dataType;
  }

  /**
   * setDataType.
   * @param dataType DataType.
   */
  public void setDataType(final DataType dataType) {
    this.dataType = dataType;
  }

  /**
   * getNoNull.
   * @return Boolean.
   */
  public Boolean getNoNull() {
    return noNull;
  }

  /**
   * setNoNull.
   * @param noNull Boolean.
   */
  public void setNoNull(final Boolean noNull) {
    this.noNull = noNull;
  }

  /**
   * getValue.
   * @return String.
   */
  public String getValue() {
    return value;
  }

  /**
   * setValue.
   * @param value String.
   */
  public void setValue(final String value) {
    this.value = value;
  }
}
