package com.smart.auth.model.enumutils;

/**
 * Class number magic.
 */
public enum Number {
  /** * 0XF0. */
  CODE0XF0(0xF0),
  /** * 0x0F. */
  CODE0X0F(0x0F),
  /** * 0xFF. */
  CODE0XFF(0xFF),
  /** * 0x1C.*/
  CODE0X1C(0x1C),
  /** * 0x100.*/
  CODE0X100(0x100),
  /** * 0x7FFFFFFF.*/
  CODE0X7FFFFFFF(0x7FFFFFFF),
  /** * 0xff00. */
  CODE0XFF00(0xFF00),
  /** * 0x00ff. */
  CODE0X00FF(0x00FF),
  /** * -2.*/
  CODE002(-2),
  /** * 0. */
  CODE0(0),
  /** * 3. */
  CODE3(3),
  /** * 4. */
  CODE4(4),
  /** * 5. */
  CODE5(5),
  /** * 6. */
  CODE6(6),
  /** * 8. */
  CODE8(8),
  /** * 10. */
  CODE10(10),
  /** * 12. */
  CODE12(12),
  /** * 15. */
  CODE15(15),
  /** * 16. */
  CODE16(16),
  /** * 18. */
  CODE18(18),
  /** * 19. */
  CODE19(19),
  /** * 20. */
  CODE20(20),
  /** * 22. */
  CODE22(22),
  /** * 24. */
  CODE24(24),
  /** * 28. */
  CODE28(28),
  /** * 32. */
  CODE32(32),
  /** * 40. */
  CODE40(40),
  /** * 45. */
  CODE45(45),
  /** * 60. */
  CODE60(60),
  /** * 100. */
  CODE100(100),
  /** * 200. */
  CODE200(200),
  /** * 254. */
  CODE254(254),
  /** * 256. */
  CODE256(256),
  /** * 409. */
  CODE409(409),
  /** * 503. */
  CODE503(503),
  /** * 400. */
  CODE400(400),
  /** * 401. */
  CODE401(401),
  /** * 415. */
  CODE415(415),
  /** * 500. */
  CODE500(500),
  /** * 9999. */
  CODE9999(9999),
  /** * 1000. */
  CODE1000(1000),
  /** * 1024. */
  CODE1024(1024),
  /** * 3600. */
  CODE3600(3600),
  /** * 86400. */
  CODE86400(86400);


  /**
   * Value numberMagic.
   */
  private int value;

  /**
   * Constructor.
   * @param value int.
   */
  Number(final int value) {
    this.value = value;
  }

  /**
   * get data value.
   * @return int.
   */
  public int value() {
    return value;
  }
}
