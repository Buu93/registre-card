/**
 -------------------------------------------------------------------
 * TransactionType.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
--------------------------------------------------------------------
 */

package com.smart.auth.model.enumutils;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class TransactionType.
 * @author Jose A Rodriguez M
 */
public enum TransactionType {
  /** * Enum UNKNOWN. */
  UNKNOWN(Number.CODE100.value(), false, "DESCONOCIDA"),
  /** * Enum AUTHENTICATION. */
  AUTHENTICATION(0, false, "AUTENTICACIÓN"),
  /** * Enum SALE. */
  SALE(1, false, "VENTA"),
  /** * Enum REFUND. */
  REFUND(2, false, "DEVOLUCION"),
  /** * Enum VOID. */
  VOID(3, false, "CANCELACION"),
  /** * Enum REVERSAL. */
  REVERSAL(Number.CODE4.value(), false, "REVERSO"),
  /** * Enum LOGON. */
  LOGON(5, false, "LOGON"),

  APERTURE(6, false, "ALTA")
  ;

  /**
   * Declaración dbType. */
  private int dbType = Number.CODE100.value();
  /**
   * Declaración isSystemType. */
  private boolean isSystemType;
  /**
   * Declaración strType. */
  private String strType = "";
  /**
   * Declaración LOOKUP. */
  private static final Map<String, TransactionType> LOOKUP = new HashMap<>();
  /**
   * Declaración NAMETOTXNTYPE. */
  private static final Map<String, TransactionType> NAMETOTXNTYPE =
        new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

  static {
    for (TransactionType txnType : TransactionType.values()) {
      LOOKUP.put("TXN_" + txnType.getDbType(), txnType);
    }

    for (TransactionType txnType : TransactionType.values()) {
      NAMETOTXNTYPE.put(txnType.getStrType(), txnType);

      for (int i = Number.CODE4.value(); i < txnType.getStrType().length(); i++) {
        String aux = txnType.getStrType().substring(0, i);

        if (!NAMETOTXNTYPE.containsKey(aux)) {
          NAMETOTXNTYPE.put(aux, txnType);
        }
      }
    }
  }

  /**
   * Constructor.
   * @param dbType as int
   * @param isSystemType as boolean
   * @param strType as String
   * */
  TransactionType(final int dbType, final boolean isSystemType, final String strType) {
    this.dbType = dbType;
    this.isSystemType = isSystemType;
    this.strType = strType;
  }

  /**
   * Getter DbType.
   * @return int
   * */
  public int getDbType() {
    return dbType;
  }

  /**
   * Method getStrType.
   * @param type as int
   * @return String
   */
  public static String getStrType(final int type) {
    String strType = "DESCONOCIDA";

    TransactionType objTxn = LOOKUP.get("TXN_" + type);

    if (objTxn != null) {
      strType = objTxn.getStrType();
    }

    return strType;
  }

  /**
   * Getter StrType.
   * @return String
   * */
  public String getStrType() {
    return strType;
  }

  /**
   * Método isSystemType.
   * @return boolean
   * */
  public boolean isSystemType() {
    return isSystemType;
  }


}