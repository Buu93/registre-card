package com.smart.auth.model.enumutils;

/**
 * Clase ErrorType.java.
 * */
public enum ErrorType {
  /** * Enum CONNECTOR_ERROR. */
  CONNECTOR_ERROR,
  /** * Enum CORE_ERROR. */
  CORE_ERROR,
  /** * Enum DB_ERROR. */
  DB_ERROR,
  /** * Enum PERSISTENCE_ERROR. */
  PERSISTENCE_ERROR,
  /** * Enum SERVICE_ERROR. */
  SERVICE_ERROR,
  /** * Enum INTERNAL_ERROR. */
  INTERNAL_ERROR,
  /** * Enum ACTIVE_DIRECTORY_ERROR. */
  ACTIVE_DIRECTORY_ERROR,
  /** * Enum TYPE. */
  TYPE;

  /**
   * Obtener etiqueta.
   *
   * @return String
   */
  public String getLabel() {
    String value;
    switch (this) {
      case CONNECTOR_ERROR:
        return "CONNECTOR_ERROR";
      case CORE_ERROR:
        return "CORE_ERROR";
      case DB_ERROR:
        return "DB_ERROR";
      default:
        value = thisValies();
        break;
    }


    return value;
  }

  /**
   * separacion devalores reglas sonas.
   * @return as String
   */
  public String thisValies() {
    switch (this) {
      case PERSISTENCE_ERROR:
        return "PERSISTENCE_ERROR";
      case SERVICE_ERROR:
        return "SERVICE_ERROR";
      case ACTIVE_DIRECTORY_ERROR:
        return "ACTIVE_DIRECTORY_ERROR";
      default:
        break;
    }

    return "";
  }
}
