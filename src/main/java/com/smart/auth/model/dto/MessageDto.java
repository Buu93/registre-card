/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.model.dto;

/**
 * Class message.java.
 *
 * @author Jose A Rodriguez M
 */
public class MessageDto {

  /**
   * Declaración dataCipher. */
  private String dataCipher = "";
  /**
   * Declaración dataText. */
  private String dataText = "";
  /**
   * Declaración refClientNum. */
  private String refClientNum = "";
  /**
   * Declaración RefSPNum. */
  private String refSPNum = "";
  /**
   * Declaración serviceName. */
  private String serviceName = "";
  /**
   * Declaración autCode. */
  private String autCode = "";
  /**
   * Declaración dataEmv. */
  private String dataEmv = "";

  /**
   * Getter dataCipher.
   * @return String*/
  public String getDataCipher() {
    return "<![CDATA[" + dataCipher + "]]>";
  }

  /**
   * Setter dataCipher.
   * @param dataCipher as String*/
  public void setDataCipher(final String dataCipher) {
    this.dataCipher = "<![CDATA[" + dataCipher + "]]>";
  }

  /**
   * Getter  dataText.
   * @return as String */
  public String getDataText() {
    return dataText;
  }

  /**
   * Setter dataText.
   * @param dataText as String*/
  public void setDataText(final String dataText) {
    this.dataText = dataText;
  }

  /**
   *Método toString.
   * @return String
   * */
  @Override
  public String toString() {
    return "MessageDto [dataCipher = " + dataCipher + ", dataText = " + dataText + "]";
  }

  /**
   * Cadena smartMicroservicio.xml.
   *
   * @return smartMicroservicio.xml.toString()
   */
  public String toStringXml() {
    StringBuilder xml = new StringBuilder();
    xml.append("<message>");
    if (!dataCipher.isEmpty()) {
      xml.append("<DataCipher><![CDATA[").append(dataCipher).append("]]></DataCipher>");
    }
    if (!dataText.isEmpty()) {
      xml.append("<DataText>").append(dataText).append("</DataText>");
    }
    xml.append("</message>");
    return xml.toString();
  }

  /**
   * Getter getRefClientNum.
   *
   * @return the RefClientNum
   */
  public String getRefClientNum() {
    return refClientNum;
  }

  /**
   * Setter RefClientNum.
   *
   * @param refClientNum to set
   */
  public void setRefClientNum(final String refClientNum) {
    this.refClientNum = refClientNum;
  }

  /**
   * Getter refSPNum.
   *
   * @return the refSPNum
   */
  public String getRefSPNum() {
    if (refSPNum == null) {
      refSPNum = "";
    }
    return refSPNum;
  }

  /**
   * Setter refSPNum.
   *
   * @param refSPNum to set
   */
  public void setRefSPNum(final String refSPNum) {
    this.refSPNum = refSPNum;
  }

  /**
   * Getter serviceName.
   *
   * @return the ServiceName
   */
  public String getServiceName() {
    return serviceName;
  }

  /**
   * Setter serviceName.
   *
   * @param serviceName to set
   */
  public void setServiceName(final String serviceName) {
    this.serviceName = serviceName;
  }

  /**
   * Getter autCode.
   *
   * @return the AutCode
   */
  public String getAutCode() {
    if (autCode == null) {
      autCode = "";
    }
    return autCode;
  }

  /**
   * Setter autCode.
   *
   * @param autCode to set
   */
  public void setAutCode(final String autCode) {
    this.autCode = autCode;
  }

  /**
   * Getter dataEmv.
   *
   * @return the DataEMV
   */
  public String getDataEmv() {
    return dataEmv;
  }

  /**
   * Setter dataEmv.
   *
   * @param dataEmv to set
   */
  public void setDataEmv(final String dataEmv) {
    this.dataEmv = dataEmv;
  }


}
