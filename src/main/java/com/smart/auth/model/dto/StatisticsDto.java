package com.smart.auth.model.dto;

import java.util.Calendar;
import java.util.Date;

/**
 * Calse StatisticsDto.java.
 * */
public class StatisticsDto {

  /**
   * Declaración startDate. */
  private Date startDate = Calendar.getInstance().getTime();
  /**
   * Declaración transactionCount. */
  private Long transactionCount = 0L;
  /**
   * Declaración aliveTransactionCount. */
  private Long aliveTransactionCount = 0L;
  /**
   * Declaración txnsPerMinuteCount. */
  private Long txnsPerMinuteCount = 0L;
  /**
   * Declaración responseTimeAvg. */
  private Long responseTimeAvg = 0L;
  /**
   * Declaración totalUsedMemory. */
  private Long totalUsedMemory = 0L;
  /**
   * Declaración totalAvailableMemory. */
  private Long totalAvailableMemory = 0L;
  /**
   * Declaración lastTxnDate. */
  private String lastTxnDate = "";

  /**
   * Constructor.*/
  public StatisticsDto() {
    //Using fo a test
  }

  /**
   * Getter startDate.
   * @return Date
   * */
  public Date getStartDate() {
    return startDate;
  }

  /**
   * Setter startDate.
   * @param  startDate as Date
   * */
  public void setStartDate(final Date startDate) {
    this.startDate = startDate;
  }

  /**
   * Getter transactionCount.
   * @return Long
   * */
  public Long getTransactionCount() {
    return transactionCount;
  }

  /**
   * Setter transactionCount.
   * @param transactionCount as Long
   * */
  public void setTransactionCount(final Long transactionCount) {
    this.transactionCount = transactionCount;
  }

  /**
   * Getter aliveTransactionCount.
   * @return Long
   * */
  public Long getAliveTransactionCount() {
    return aliveTransactionCount;
  }

  /**
   * Setter aliveTransactionCount.
   * @param aliveTransactionCount as Long
   * */
  public void setAliveTransactionCount(final Long aliveTransactionCount) {
    this.aliveTransactionCount = aliveTransactionCount;
  }

  /**
   * Getter txnsPerMinuteCount.
   * @return Long
   * */
  public Long getTxnsPerMinuteCount() {
    return txnsPerMinuteCount;
  }

  /**
   * Setter txnsPerMinuteCount.
   * @param txnsPerMinuteCount as Long
   * */
  public void setTxnsPerMinuteCount(final Long txnsPerMinuteCount) {
    this.txnsPerMinuteCount = txnsPerMinuteCount;
  }

  /**
   * Getter responseTimeAvg.
   * @return Long
   * */
  public Long getResponseTimeAvg() {
    return responseTimeAvg;
  }

  /**
   * Setter responseTimeAvg.
   * @param responseTimeAvg as Long
   * */
  public void setResponseTimeAvg(final Long responseTimeAvg) {
    this.responseTimeAvg = responseTimeAvg;
  }

  /**
   * Getter totalUsedMemory.
   * @return Long
   * */
  public Long getTotalUsedMemory() {
    return totalUsedMemory;
  }

  /**
   * Setter totalUsedMemory.
   * @param totalUsedMemory as Long
   * */
  public void setTotalUsedMemory(final Long totalUsedMemory) {
    this.totalUsedMemory = totalUsedMemory;
  }

  /**
   * Getter totalAvailableMemory.
   * @return Long
   * */
  public Long getTotalAvailableMemory() {
    return totalAvailableMemory;
  }

  /**
   * Setter totalAvailableMemory.
   * @param totalAvailableMemory as Long
   * */
  public void setTotalAvailableMemory(final Long totalAvailableMemory) {
    this.totalAvailableMemory = totalAvailableMemory;
  }

  /**
   * Getter lastTxnDate.
   * @return String
   * */
  public String getLastTxnDate() {
    return lastTxnDate;
  }

  /**
   * Setter lastTxnDate.
   * @param lastTxnDate as String
   * */
  public void setLastTxnDate(final String lastTxnDate) {
    this.lastTxnDate = lastTxnDate;
  }

}
