package com.smart.auth.model.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * Objeto de respuesta para los servicios web. <br>
 * <p>
 * La infomación es almacenada en una estructura de datos tipo Mapa (string,Object) [{@link Map}].
 * donde.
 * la llave del mapa es el nombre de la variable y el objeto es el valor de la variable.
 * </p>
 * <p>
 * El valor de la variable puede ser de cualquier tipo, desde un
 * <code>String,Integer,Boolean,long </code> hasta una estructura compuesta como
 * <code>List,Set incluso otros Mapas</code>
 * </p>
 * <strong>ejemplo:</strong> Suponga que un servicio retorna tramiteId,folio y idTarea.<br>
 * La representación de la informacion en notación JSON : <br>
 * <code>{<br>
 * "estatus": 1
 * "mensaje": "tarea liberada "
 * "informacion":{<br>
 * "tramiteId":4,<br>
 * "folio":"555555",<br>
 * "idTarea":2<br>
 * }<br>
 * }<br></code> <br>
 * La forma para obtener esos valores por medio del objeto Response es la siguiente:<br>
 * <strong> Long tramiteId = null; <br>
 * tramiteId = response.getInformacion.get("tramiteId");<br>
 * ... </strong><br>
 */
public class RespuestaGenerica {

  /**
   * Declaración estatus. */
  private int estatus;
  /**
   * Declaración mensaje. */
  private String mensaje;
  /**
   * Declaración informacion. */
  private Map<String, Object> informacion;

  /**
   * Constructor.
   */
  public RespuestaGenerica() {
    setInformacion(new HashMap<String, Object>());
  }

  /**
   * Instantiates a new respuesta generica.
   *
   * @param estatus the estatus
   * @param mensaje the mensaje
   * @param informacion the informacion
   */
  public RespuestaGenerica(final int estatus, final String mensaje, final Map<String, Object> informacion) {
    this.estatus = estatus;
    this.mensaje = mensaje;
    this.setInformacion(informacion);
  }

  /**
   * Constructor.
   * @param estatus as int
   * @param mensaje as String
   */
  public RespuestaGenerica(final int estatus, final String mensaje) {
    this.estatus = estatus;
    this.mensaje = mensaje;
  }

  /**
   * El atributo estatus.
   * @return int
   */
  public int getEstatus() {
    return estatus;
  }

  /**
   * Parametro estatus a actualizar.
   * @param estatus as int
   */
  public void setEstatus(final int estatus) {
    this.estatus = estatus;
  }

  /**
   * El atributo mensaje.
   * @return String
   */
  public String getMensaje() {
    return mensaje;
  }

  /**
   * Parametro mensaje a actualizar.
   * @param mensaje as String
   */
  public void setMensaje(final String mensaje) {
    this.mensaje = mensaje;
  }

  /**
   * El atributo informacion.
   * @return Map
   */
  public Map<String, Object> getInformacion() {
    return informacion;
  }

  
  /**
   * Sets the informacion.
   *
   * @param informacion the informacion
   */
  public void setInformacion(final Map<String, Object> informacion) {
    this.informacion = informacion;
  }

}
