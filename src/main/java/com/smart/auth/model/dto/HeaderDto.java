/**
 * HeaderDto.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.model.dto;

/**
 * Class header.
 *
 * @author Jose A Rodriguez M
 */
public class HeaderDto {
  /**
   * Declaración lastServerKey. */
  private String lastServerKey = "";
  /**
   * Declaración type. */
  private String type = "";
  /**
   * Declaración deviceTime. */
  private String deviceTime = "";
  /**
   * Declaración respMessage. */
  private String respMessage = "";
  /**
   * Declaración serialPos. */
  private String serialPos = "";
  /**
   * Declaración clientID. */
  private String clientID = "";
  /**
   * Declaración stan. */
  private String stan = "";
  /**
   * Declaración respCode. */
  private String respCode = "";
  /**
   * Declaración request. */
  private boolean request = false;

  /**
   * Constructor.
   * */
  public HeaderDto() {
    //is empty for test.
  }

  /**
   * constructor.
   * @param request as boolean
   * */
  public HeaderDto(final boolean request) {
    this.request = request;
  }

  /**
   * Getter lastServerKey.
   * @return String*/
  public String getLastServerKey() {
    return lastServerKey;
  }

  /**
   * Setter lastServerKey.
   * @param lastServerKey as String*/
  public void setLastServerKey(final String lastServerKey) {
    this.lastServerKey = lastServerKey;
  }

  /**
   * Getter type.
   * @return String*/
  public String getType() {
    return type;
  }

  /**
   * Setter type.
   * @param type as String */
  public void setType(final String type) {
    this.type = type;
  }

  /**
   * Getter deviceTime.
   * @return String*/
  public String getDeviceTime() {
    return deviceTime;
  }

  /**
   * Setter deviceTime.
   * @param deviceTime as String
   * */
  public void setDeviceTime(final String deviceTime) {
    this.deviceTime = deviceTime;
  }

  /**
   * Getter respMessage.
   * @return String*/
  public String getRespMessage() {
    return respMessage;
  }

  /**
   * Setter respMessage.
   * @param respMessage as String*/
  public void setRespMessage(final String respMessage) {
    this.respMessage = respMessage;
  }

  /**
   * Getter serialPos.
   * @return String*/
  public String getSerialPos() {
    return serialPos;
  }

  /**
   * Setter serialPos.
   * @param serialPos as String */
  public void setSerialPos(final String serialPos) {
    this.serialPos = serialPos;
  }

  /**
   * Getter clientID.
   * @return String*/
  public String getClientID() {
    return clientID;
  }

  /**
   * Setter clientID.
   * @param clientID as String*/
  public void setClientID(final String clientID) {
    this.clientID = clientID;
  }

  /**
   * Getter stan.
   * @return String*/
  public String getStan() {
    return stan;
  }

  /**
   * Setter stan.
   * @param stan as String
   * */
  public void setStan(final String stan) {
    this.stan = stan;
  }

  /**
   * Obtiene la repsuesta del código.
   * @return respCode
   */
  public String getRespCode() {
    if (respCode == null) {
      respCode = "";
    }
    return respCode;
  }

  /**
   * Setter respCode.
   * @param respCode as String
   * */
  public void setRespCode(final String respCode) {
    this.respCode = respCode;
  }

  /**
   * Método toString.
   * @return String
   * */
  @Override
  public String toString() {
    return "HeaderDto{"
          + "lastServerKey='" + lastServerKey + '\''
          + ", type='" + type + '\''
          + ", deviceTime='" + deviceTime + '\''
          + ", respMessage='" + respMessage + '\''
          + ", serialPos='" + serialPos + '\''
          + ", clientID='" + clientID + '\''
          + ", stan='" + stan + '\''
          + ", respCode='" + respCode + '\''
          + ", request=" + request
          + '}';
  }

  /**
   * Cadena smartMicroservicio.xml.
   *
   * @return smartMicroservicio.xml.toString()
   */
  public String toStringXml() {
    StringBuilder xml = new StringBuilder();
    xml.append("<header>");
    xml.append("<Type>").append(type).append("</Type>");
    xml.append("<ClientID>").append(clientID).append("</ClientID>");
    xml.append("<SerialPos>").append(serialPos).append("</SerialPos>");
    xml.append("<Stan>").append(stan).append("</Stan>");
    xml.append("<DeviceTime>").append(deviceTime).append("</DeviceTime>");
    if (request) {
      xml.append("<LastServerKey>").append(lastServerKey).append("</LastServerKey>");
      xml.append("<Resp-Code>").append(respCode).append("</Resp-Code>");
      xml.append("<Resp-Message>").append(respMessage).append("</Resp-Message>");
    }
    xml.append("</header>");
    return xml.toString();
  }

  /**
   * Método isRequest.
   * @return boolean
   * */
  public boolean isRequest() {
    return request;
  }

  /**
   * Setter request.
   * @param request as boolean
   * */
  public void setRequest(final boolean request) {
    this.request = request;
  }

}
