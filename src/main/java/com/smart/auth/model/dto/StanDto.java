/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.model.dto;

import com.smart.auth.model.enumutils.Number;

import java.io.Serializable;

/**
 * Class StanDto.java.
 *
 * @author Jose A Rodriguez M
 */
public class StanDto implements Serializable {

  /**serialización.*/
  private static final long serialVersionUID = 1L;
  /**
   * Declaración id. */
  private Long id = 0L;
  /**
   * Declaración numStan. */
  private Long numStan = 0L;
  /**
   * Declaración serialPos. */
  private String serialPos = "";

  /**
   * Constructor.*/
  public StanDto() {
    //empty for test.
  }

  /**
   * constructor.
   * @param serialPos as String*/
  public StanDto(final String serialPos) {
    this.serialPos = serialPos;
  }


  /**
   * Getter serialPos.
   *
   * @return the serialPos
   */
  public String getSerialPos() {
    return serialPos;
  }

  /**
   * Setter serialPos.
   *
   * @param serialPos the serialPos to set
   */
  public void setSerialPos(final String serialPos) {
    this.serialPos = serialPos;
  }

  /**
   * Incrementar variable numStan.
   */
  public void incrementStan() {
    numStan = numStan + 1L;
    if (numStan == Number.CODE9999.value()) {
      numStan = 1L;
    }
  }


  /**
   * Getter id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Setter id.
   *
   * @param id the id to set
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * Getter numStan.
   *
   * @return the numStan
   */
  public Long getNumStan() {
    return numStan;
  }

  /**
   * Setter numStan.
   *
   * @param numStan the numStan to set
   */
  public void setNumStan(final Long numStan) {
    this.numStan = numStan;
  }

}
