/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.model.dto;

import com.smart.auth.config.cipher.ServiceEncryptionData;
import com.smart.auth.model.enumutils.Number;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Class PosDataDto.java.
 *
 * @author Jose A Rodriguez M
 */
public class PosDataDto implements Serializable {
  /**
   * serialización.*/
  private static final long serialVersionUID = 1L;
  /**
   * Declaración id. */
  private Long id = 0L;
  /**
   * Declaración serialPos. */
  private String serialPos = "";
  /**
   * Declaración clientId. */
  private String clientId = "";
  /**
   * Declaración userName. */
  private String userName = "";
  /**
   * Declaración userP. */
  private String userP = "";
  /**
   * Declaración logonlastDate. */
  private Date logonlastDate = Calendar.getInstance().getTime();
  /**
   * Declaración lastServerKey. */
  private String lastServerKey = "";
  /**
   * Declaración timeOut. */
  private int timeOut = Number.CODE45.value();
  /**
   * Declaración status. */
  private int status = 1;
  /**
   * Declaración lastDate. */
  private String lastDate = "";
  /**
   * Declaración statusData. */
  private int statusData = 1;

  public PosDataDto() {
  }

  /**
   * Constructor.
   * */
  public PosDataDto(final String clientId) {
    this.clientId = clientId;
  }


  /**
   * constructor.
   * @param clientId as String
   * @param serialPos as String
   * */
  public PosDataDto(final String clientId, final String serialPos) {
    this.clientId = clientId;
    this.serialPos = serialPos;
  }

  /**
   * Getter lastDate.
   * @return String*/
  public String getLastDate() {
    return lastDate;
  }

  /**
   * Setter lastDate.
   * @param lastDate as String*/
  public void setLastDate(final String lastDate) {
    this.lastDate = lastDate;
  }

  /**
   * Getter lastServerKey.
   *
   * @return the lastServerKey
   */
  public String getLastServerKey() {
    return lastServerKey;
  }

  /**
   * Setter lastServerKey.
   *
   * @param lastServerKey the lastServerKey to set
   */
  public void setLastServerKey(final String lastServerKey) {
    this.lastServerKey = lastServerKey;
  }

  /**
   * Getter timeOut.
   *
   * @return the timeOut
   */
  public int getTimeOut() {
    return timeOut;
  }

  /**
   * Setter timeOut.
   *
   * @param timeOut the timeOut to set
   */
  public void setTimeOut(final int timeOut) {
    this.timeOut = timeOut;
  }

  /**
   * Getter serialPos.
   *
   * @return the serialPos
   */
  public String getSerialPos() {
    return serialPos;
  }

  /**
   * Setter serialPos.
   *
   * @param serialPos the serialPos to set
   */
  public void setSerialPos(final String serialPos) {
    this.serialPos = serialPos;
  }

  /**
   * Getter clientId.
   *
   * @return the clientId
   */
  public String getClientId() {
    return clientId;
  }

  /**
   * Setter clientId.
   *
   * @param clientId the clientId to set
   */
  public void setClientId(final String clientId) {
    this.clientId = clientId;
  }


  /**
   * Getter status.
   *
   * @return the status
   */
  public int getStatus() {
    return status;
  }

  /**
   * Setter status.
   *
   * @param status the status to set
   */
  public void setStatus(final int status) {
    this.status = status;
  }

  /**
   * Getter id.
   *
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * Setter id.
   *
   * @param id the id to set
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * Getter userName.
   *
   * @return the userName
   */
  public String getUserName() {
    return userName;
  }

  /**
   * Setter userName.
   *
   * @param userName the userName to set
   */
  public void setUserName(final String userName) {
    this.userName = userName;
  }

  /**
   * Getter logonlastDate.
   *
   * @return the logonlastDate
   */
  public Date getLogonlastDate() {
    return logonlastDate;
  }

  /**
   * Setter logonlastDate.
   *
   * @param logonlastDate the logonlastDate to set
   */
  public void setLogonlastDate(final Date logonlastDate) {
    this.logonlastDate = logonlastDate;
  }

  /**
   * Getter userP.
   *
   * @return the userP
   */
  public String getUserP() {
    return userP;
  }

  /**
   * Setter userP.
   *
   * @param userP the userP to set
   */
  public void setUserP(final String userP) {
    this.userP = userP;
  }

  /**
   * Getter statusData.
   *
   * @return the statusData
   */
  public int getStatusData() {
    return statusData;
  }

  /**
   * Setter statusData.
   *
   * @param statusData the statusData to set
   */
  public void setStatusData(final int statusData) {
    this.statusData = statusData;
  }

  /**
   * Método toString.
   * @return String*/
  @Override
  public String toString() {
    return "PosDataDto{"
          + "id=" + id
          + ", serialPos='" + serialPos + '\''
          + ", clientId='" + clientId + '\''
          + ", userName='" + userName + '\''
          + ", logonlastDate=" + logonlastDate
          + ", lastServerKey='" + lastServerKey + '\''
          + ", timeOut=" + timeOut
          + ", status=" + status
          + ", statusData=" + statusData
          + ", lastDate='" + lastDate + '\''
          + '}';
  }

  /**
   * to String de toda la clase.
   *
   * @return String
   */
  public String toStringAll() {
    return "PosDataDto{"
          + "id=" + id
          + ", serialPos='" + serialPos + '\''
          + ", clientId='" + clientId + '\''
          + ", userName='" + userName + '\''
          + ", userP='" + getDecryptData() + '\''
          + ", logonlastDate=" + logonlastDate
          + ", lastServerKey='" + lastServerKey + '\''
          + ", timeOut=" + timeOut
          + ", status=" + status
          + ", statusData=" + statusData
          + ", lastDate='" + lastDate + '\''
          + '}';
  }

  /**
   * Método getDecryptData.
   * @return String*/
  private String getDecryptData() {
    String sret = userP;
    if (statusData == 2 && userP != null && !userP.isEmpty()) {
      sret = ServiceEncryptionData.decrypt(userP);
    }
    return sret;
  }

}
