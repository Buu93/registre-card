/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.model.dto;


import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.constants.ResponseCode;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Clase MicroserviceResponseDTO.java.
 *
 * @author Jose A Rodriguez M
 */
public class MicroserviceResponseDTO implements Serializable {

  /**
   * Declaración codigoError. */
  private String codigoError = "";
  /**
   * Declaración descripcionError. */
  private String descripcionError = "";
  /**
   * Declaración existeError. */
  private String existeError = "";
  /**
   * Declaración importe. */
  private String importe = "";
  /**
   * Declaración numTarjeta. */
  private String numTarjeta = "";
  /**
   * Declaración numeroAutorizacion. */
  private String numeroAutorizacion = "";
  /**
   * Declaración numeroSeguimiento. */
  private String numeroSeguimiento = "";
  /**
   * Declaración uniqueKey. */
  private String uniqueKey = "";
  /**
   * Declaracioón vacio. */
  private String vacio = "";

  /**
   * Getter CodigoError.
   *
   * @return the codigoError
   */
  public String getCodigoError() {
    return codigoError;
  }

  /**
   * Setter Codigo Error.
   *
   * @param codigoError the codigoError to set
   */
  public void setCodigoError(final String codigoError) {
    this.codigoError = codigoError;
  }

  /**
   * Getter Descripcion Error.
   *
   * @return the descripcionError
   */
  public String getDescripcionError() {
    return descripcionError;
  }

  /**
   * Setter DescripcionError.
   *
   * @param descripcionError the descripcionError to set
   */
  public void setDescripcionError(final String descripcionError) {
    this.descripcionError = descripcionError;
  }

  /**
   * Getter ExistError.
   *
   * @return the existeError
   */
  public String getExisteError() {
    if (existeError == null || existeError.isEmpty()) {
      existeError = true + vacio;
      if (codigoError != null && codigoError.equals(ResponseCode.OK)) {
        existeError = false + vacio;
      }
    }
    return existeError;
  }

  /**
   * Setter ExisteError.
   *
   * @param existeError the existeError to set
   */
  public void setExisteError(final String existeError) {
    this.existeError = existeError;
  }

  /**
   * Getter Importe.
   *
   * @return the importe
   */
  public String getImporte() {
    return importe;
  }

  /**
   * Setter importe.
   *
   * @param importe the importe to set
   */
  public void setImporte(final String importe) {
    this.importe = importe;
  }

  /**
   * Getter NumTarjeta.
   * @return the numTarjeta
   */
  public String getNumTarjeta() {
    return numTarjeta;
  }

  /**
   * Setter NumTarjeta.
   * @param numTarjeta the numTarjeta to set
   */
  public void setNumTarjeta(final String numTarjeta) {
    this.numTarjeta = numTarjeta;
  }

  /**
   * Getter numeroAutorizacion.
   * @return the numeroAutorizacion
   */
  public String getNumeroAutorizacion() {
    return numeroAutorizacion;
  }

  /**
   * Setter numeroAutorizacion.
   * @param numeroAutorizacion the numeroAutorizacion to set
   */
  public void setNumeroAutorizacion(final String numeroAutorizacion) {
    this.numeroAutorizacion = numeroAutorizacion;
  }

  /**
   * Getter numeroSeguimiento.
   * @return the numeroSeguimiento
   */
  public String getNumeroSeguimiento() {
    return numeroSeguimiento;
  }

  /**
   * Setter numeroSeguimiento.
   * @param numeroSeguimiento the numeroSeguimiento to set
   */
  public void setNumeroSeguimiento(final String numeroSeguimiento) {
    this.numeroSeguimiento = numeroSeguimiento;
  }

  /**
   * Getter uniqueKey.
   * @return the uniqueKey
   */
  public String getUniqueKey() {
    return uniqueKey;
  }

  /**
   * Setter uniqueKey.
   * @param uniqueKey the uniqueKey to set
   */
  public void setUniqueKey(final String uniqueKey) {
    this.uniqueKey = uniqueKey;
  }

  /**
   * toXmlString.
   * @return String
   */
  public String toXMLString() {
    StringBuilder xml = new StringBuilder();
    try {
      getExisteError();
      XStream xstream = new XStream(new DomDriver()); // does not require XPP3 library
      xstream.alias("return", MicroserviceResponseDTO.class);
      xml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">");
      xml.append("\n<soapenv:Body>");
      xml.append("\n<ns2:cobranzaResponse xmlns:ns2=\"http://services.ibm.gnp.com/\">");
      xml.append(xstream.toXML(this));
      xml.append("\n</ns2:cobranzaResponse>");
      xml.append("\n</soapenv:Body>");
      xml.append("\n</soapenv:Envelope>");
    } catch (Exception e) {
      Console.writeException(e);
      xml = new StringBuilder();
      xml.append(getXmlError());
    }
    return xml.toString();
  }

  /**
   * Método GetXmlError.
   * @return String
   */
  private String getXmlError() {
    return getXmlError(codigoError, descripcionError);
  }

  /**
   * Método getXmlError.
   * @param error as String
   * @param msg   as String
   * @return String
   */
  public static String getXmlError(final String error, final String msg) {
    StringBuilder xml = new StringBuilder();
    xml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">");
    xml.append("\n<soapenv:Body>");
    xml.append("\n<ns2:cobranzaResponse xmlns:ns2=\"http://services.ibm.gnp.com/\">");
    xml.append(toXMLStringLocal(error, msg));
    xml.append("\n</ns2:cobranzaResponse>");
    xml.append("\n</soapenv:Body>");
    xml.append("\n</soapenv:Envelope>");
    return xml.toString();
  }

  /**
   * Método toXMLStringLocal.
   * @param error as String
   * @param msg   as String
   * @return String
   */
  private static String toXMLStringLocal(final String error, final String msg) {
    return "<return>\n"
          + "            <codigoError>" + error + "</codigoError>\n"
          + "            <descripcionError>" + msg + "</descripcionError>\n"
          + "            <existeError>true</existeError>\n"
          + "            <importe></importe>\n"
          + "            <numTarjeta></numTarjeta>\n"
          + "            <numeroAutorizacion></numeroAutorizacion> \n"
          + "            <numeroSeguimiento></numeroSeguimiento> \n"
          + "            <uniqueKey></uniqueKey> \n"
          + "         </return>";
  }

  /**
   * Método toXmlMap.
   * @return Map
   */
  public Map<String, Object> toXmlMap() {
    Map<String, Object> map = new HashMap<>();

    map.put("codigoError", codigoError);
    map.put("descripcionError", descripcionError);
    map.put("existeError", existeError);
    map.put("importe", importe);
    map.put("numTarjeta", numTarjeta);
    map.put("numeroAutorizacion", numeroAutorizacion);
    map.put("numeroSeguimiento", numeroSeguimiento);
    map.put("uniqueKey", uniqueKey);

    return map;
  }
}
