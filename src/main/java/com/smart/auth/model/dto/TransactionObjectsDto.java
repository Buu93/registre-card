/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.model.dto;

import com.smart.auth.config.BeanUtil;
import com.smart.auth.config.ConversionUtils;
import com.smart.auth.controller.xml.MicoserviceXml;
import com.smart.auth.dao.SystemDao;
import com.smart.auth.dao.SystemDaoImpl;
import com.smart.auth.dao.TxnByDayDao;
import com.smart.auth.dao.TxnByDayDaoImpl;
import com.smart.auth.exception.logs.Console;
import com.smart.auth.exception.logs.IdTraceManager;
import com.smart.auth.exception.message.MessageData;
import com.smart.auth.model.MicroserviceRequest;
import com.smart.auth.model.enumutils.TransactionType;
import com.smart.auth.service.clientmessage.XmlRequest;
import com.smart.auth.service.clientmessage.XmlResponse;
import com.smart.auth.model.enumutils.Number;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Clase TransactionObjectsDto.java.
 * @author Jose A Rodriguez M
 */
public class TransactionObjectsDto {

  /**
   * Declaración microserviceRequest. */
  private MicroserviceRequest microserviceRequest;
  /**
   * Declaración microserviceResponseDTO. */
  private MicroserviceResponseDTO microserviceResponseDTO;
  /**
   * Declaración MicoserviceXml. */
  private MicoserviceXml micoserviceXml;
  /**
   * Declaración requestWS. */
  private XmlRequest requestWS;
  /**
   * Declaración responseWS. */
  private XmlResponse responseWS;
  /**
   * Declaración idOperation. */
  private String idOperation;
  /**
   * Declaración txnType. */
  private TransactionType txnType;
  /**
   * Declaración request. */
  private MessageData request;
  /**
   * Declaración response. */
  private MessageData response;
  /**
   * Declaración sessionObjects. */
  private Map<String, Object> sessionObjects; //Objetos que permanecen por el tiempo que dure el
  // canal abierto
  /**
   * Declaración sessionObjects. */
  private PosDataDto logonData;
  /**
   * Declaración lastServerKey. */
  private String lastServerKey = "";
  /**
   * Declaración timeout. */
  private int timeout = 0;
  /**
   * Declaración secureChannel. */
  private boolean secureChannel;
  /**
   * Declaración relatedExistentTxnId. */
  private Long relatedExistentTxnId = 0L;
  /**
   * Declaración idService. */
  private int idService = 0;
  /**
   * Declaración sentToClient. */
  private boolean sentToClient = false;
  /**
   * Declaración attendedByVendor. */
  private boolean attendedByVendor = false;
  /**
   * Declaración startTime. */
  private long startTime;
  /**
   * Declaración endTime. */
  private long endTime;

  //Objetos a revisar
  /**
   * Declaración tsn. */
  private String tsn = "0";
  /**
   * Declaración posSerial. */
  private String posSerial = "";
  /**
   * Declaración idClient. */
  private String idClient = "0";
  /**
   * Declaración idClientForTxn. */
  private String idClientForTxn = "";
  /**
   * Declaración idPos. */
  private String idPos = "0";
  /**
   * Declaración txnReference. */
  private String txnReference = "000000000000"; //Number.CODE12.value() digits
  /**
   * Declaración responseCode. */

  private String responseCode = "";
  /**
   * Declaración responseMessage. */
  private String responseMessage = "";
  /**
   * Declaración connectionScheme. */
  private int connectionScheme = 1;
  /**
   * Declaración txnHash. */
  private String txnHash = "";
  /**
   * Declaración hashTsnDayPos. */
  private String hashTsnDayPos = "";
  /**
   * Declaración approvalCode. */
  private String approvalCode = "";
  /**
   * Declaración targetReference. */
  private String targetReference = "";
  /**
   * Declaración targetReference. */
  private Calendar timeStamp;
  /**
   * Declaración timeStamp. */
  private boolean closeChannelAtEnd = true;
  /**
   * Declaración closeChannelAtEnd. */
  private String originalIdOperation;
  /**
   * Declaración originalIdOperation. */
  private String ip;
  /**
   * Declaración ip. */
  private byte cipherType;
  /**
   * Declaración cipherType. */
  private TxnByDayDto txnByDay;
  /**
   * Declaración txnByDay. */
  private TxnByDayDto txnByDayOri;
  /**
   * Declaración txnByDayOri. */
  private TxnByDayDao txnByDayDAO;
  /**
   * Declaración txnByDayDAO. */
  private SystemDao systemDAO;

  /**
   * Constructor TransactionObjectsDto.
   * @param other as TransactionObjectsDto
   * */
  public TransactionObjectsDto(final TransactionObjectsDto other) {

    this.idOperation = other.getIdOperation();
    this.originalIdOperation = other.originalIdOperation;
    Console.writeln(Console.Level.INFO, idOperation,
          "Inicia transacción en canal de " + originalIdOperation);
    this.secureChannel = other.secureChannel;
    this.request = new MessageData(idOperation);
    this.response = new MessageData(idOperation);
    this.closeChannelAtEnd = other.closeChannelAtEnd;
    this.startTime = System.currentTimeMillis();
    this.timeStamp = other.timeStamp;
    this.sessionObjects = other.sessionObjects;
  }

  /**
   * Constructor TransactionObjectsDto.
   * @param idOperation as String
   * @param secureChannel as boolean
   * */
  public TransactionObjectsDto(final String idOperation, final boolean secureChannel) {

    this.idOperation = idOperation;
    this.originalIdOperation = idOperation;

    this.secureChannel = secureChannel;
    this.request = new MessageData(idOperation);
    this.response = new MessageData(idOperation);
    this.sessionObjects = new HashMap<>();
    this.startTime = System.currentTimeMillis();
    this.timeStamp = Calendar.getInstance();
  }

  /**
   * Constructor.*/
  public TransactionObjectsDto() {
    //empty for test.
  }

  /**
   * Getter requestWS.
   * @return the requestWS
   */
  public XmlRequest getRequestWS() {
    if (requestWS == null) {
      requestWS = new XmlRequest();
    }
    return requestWS;
  }

  /**
   * Setter requestWS.
   * @param requestWS the requestWS to set
   */
  public void setRequestWS(final XmlRequest requestWS) {
    this.requestWS = requestWS;
  }

  /**
   * Getter responseWS.
   * @return the responseWS
   */
  public XmlResponse getResponseWS() {
    if (responseWS == null) {
      responseWS = new XmlResponse();
    }
    return responseWS;
  }

  /**
   * Setter responseWS.
   * @param responseWS the responseWS to set
   */
  public void setResponseWS(final XmlResponse responseWS) {
    this.responseWS = responseWS;
  }

  /**
   * Getter lastServerKey.
   * @return the lastServerKey
   */
  public String getLastServerKey() {
    return lastServerKey;
  }

  /**
   * Setter lastServerKey.
   * @param lastServerKey the lastServerKey to set
   */
  public void setLastServerKey(final String lastServerKey) {
    this.lastServerKey = lastServerKey;
  }

  /**
   * Getter timeout.
   * @return the timeout
   */
  public int getTimeout() {
    return timeout;
  }

  /**
   * Setter timeout.
   * @param timeout the timeout to set
   */
  public void setTimeout(final int timeout) {
    this.timeout = timeout;
  }

  /**
   * Setter txnType.
   * @param txnType as TransactionType*/
  public void setTxnType(final TransactionType txnType) {
    this.txnType = txnType;
  }

  /**
   * Getter txnType.
   * @return TransactionType
   * */
  public TransactionType getTxnType() {
    if (txnType == null) {
      txnType = TransactionType.UNKNOWN;
    }
    return txnType;
  }

  /**
   * Getter LogonData.
   * @return the logonData
   */
  public PosDataDto getLogonData() {
    return logonData;
  }

  /**
   * Setter LogonData.
   * @param logonData the logonData to set
   */
  public void setLogonData(final PosDataDto logonData) {
    this.logonData = logonData;
  }


  /**
   * Getter Request.
   * @return MessageData*/
  public MessageData getRequest() {
    return request;
  }

  /**
   * getter response.
   * @return MessageData*/
  public MessageData getResponse() {
    return response;
  }

  /**
   * Getter AppgnpXml.
   * @return the appgnpXml
   */
  public MicoserviceXml getAppgnpXml() {
    return micoserviceXml;
  }

  /**
   * Setter AppgnpXml.
   * @param micoserviceXml the appgnpXml to set
   */
  public void setAppgnpXml(final MicoserviceXml micoserviceXml) {
    this.micoserviceXml = micoserviceXml;
  }

  /**
   * Getter idOperation.
   * @return String*/
  public String getIdOperation() {
    return idOperation;
  }

  /**
   * Setter idOperation.
   * @param idOperation as String*/
  public void setIdOperation(final String idOperation) {
    this.idOperation = IdTraceManager.normalize(idOperation);
  }

  /**
   * Método isSecureChannel.
   * @return boolean*/
  public boolean isSecureChannel() {
    return secureChannel;
  }

  /**
   * Getter relatedExistentTxn.
   * @return Long*/
  public Long getRelatedExistentTxnId() {
    return relatedExistentTxnId;
  }

  /**
   * Setter relatedExistentTxn.
   * @param relatedExistentTxn as Long*/
  public void setRelatedExistentTxnId(final Long relatedExistentTxn) {
    this.relatedExistentTxnId = relatedExistentTxn;
  }

  /**
   * Getter idService.
   * @return int*/
  public int getIdService() {
    return idService;
  }

  /**
   * Setter idService.
   * @param idService as int*/
  public void setIdService(final int idService) {
    this.idService = idService;
  }

  /**
   * Setter ApprovalCode.
   * @param approvalCode as String
   * */
  public void setApprovalCode(final String approvalCode) {
    String approvalCode1 = approvalCode;
    if (approvalCode1 == null) {
      return;
    }

    if (approvalCode1.contains("\"")) {
      approvalCode1 = approvalCode1.replaceAll("\"", "");
    }

    this.approvalCode = approvalCode1;
  }

  /**
   * getApprovalCode.
   * @return String.*/
  public String getApprovalCode() {
    return approvalCode;
  }

  /**
   * isSentToClient.
   * @return boolean*/
  public boolean isSentToClient() {
    return sentToClient;
  }

  /**
   * Método setSentToClient.*/
  public void setSentToClient() {
    this.sentToClient = true;
  }

  /**
   * Método isAttendedByVendor.
   * @return boolean*/
  public boolean isAttendedByVendor() {
    return attendedByVendor;
  }

  /**
   * Setter attendedByVendor.*/
  public void setAttendedByVendor() {
    this.attendedByVendor = true;
  }


  /**
   * Setter secureChannel.
   * @param secureChannel as boolean*/
  public void setSecureChannel(final boolean secureChannel) {
    this.secureChannel = secureChannel;
  }

  /**
   * Setter timeStamp.
   * @param txnDate as Calendar*/
  public void setCalendar(final Calendar txnDate) {
    timeStamp = txnDate;
  }

  /**
   * Getter timeStamp.
   * @return Calendar*/
  public Calendar getCalendar() {
    return timeStamp;
  }

  /**
   * Getter posSerial.
   * @return String*/
  public String getPosSerial() {
    return posSerial;
  }

  /**
   * Setter posSerial.
   * @param posSerial as String*/
  public void setPosSerial(final String posSerial) {
    this.posSerial = posSerial;
  }

  /**
   * Getter tsn.
   * @return String*/
  public String getTsn() {
    return tsn;
  }

  /**
   * Setter tsn.
   * @param tsn as String*/
  public void setTsn(final String tsn) {
    this.tsn = tsn;
  }

  /**
   * Getter idClient.
   * @return String*/
  public String getIdClient() {
    return idClient;
  }

  /**
   * Setter idClient.
   * @param idClient as String*/
  public void setIdClient(final String idClient) {
    this.idClient = idClient;
  }

  /**
   * Getter idPos.
   * @return String*/
  public String getIdPos() {
    return idPos;
  }

  /**
   * Setter idPos.
   * @param idPos as String*/
  public void setIdPos(final String idPos) {
    this.idPos = idPos;
  }

  /**
   * Getter idClientForTxn.
   * @return String*/
  public String getIdClientForTxn() {
    return idClientForTxn;
  }

  /**
   * Setter idClientXTxn.
   * @param idClientXTxn as String*/
  public void setIdClientForTxn(final String idClientXTxn) {
    this.idClientForTxn = idClientXTxn;
  }

  /**
   * Getter txnReference.
   * @return String*/
  public String getTxnReference() {
    return txnReference;
  }

  /**
   * Setter TxnReference.
   * @param txnReference as Long
   * */
  public void setTxnReference(final Long txnReference) {
    String ref;
    ref = ConversionUtils.paddLeftZero("" + txnReference, Number.CODE12.value());
    this.txnReference = ref;
  }

  /**
   * Setter TxnReference.
   * @param txnReference as String
   * */
  public void setTxnReference(final String txnReference) {
    String ref = txnReference;
    if (ref.length() < Number.CODE12.value()) {
      ref = ConversionUtils.paddLeftZero(ref, Number.CODE12.value());
    }
    this.txnReference = ref;
  }

  /**
   * Getter responseCode.
   * @return String*/
  public String getResponseCode() {
    return responseCode;
  }

  /**
   * Setter responseCode.
   * @param responseCode as String*/
  public void setResponseCode(final String responseCode) {
    this.responseCode = responseCode;

  }

  /**
   * Getter ResponseMessage.
   * @return String
   * */
  public String getResponseMessage() {
    String sret = "";
    if (!responseMessage.isEmpty()) {
      sret = responseMessage;
    } else if (responseCode != null && "00".equals(responseCode)) {
      sret = "APROBADA";
    }
    return sret;
  }

  /**
   * Setter responseMessage.
   * @param responseMessage as String*/
  public void setResponseMessage(final String responseMessage) {
    this.responseMessage = responseMessage != null ? responseMessage : "";
  }

  /**
   * Getter connectionScheme.
   * @return int*/
  public int getConnectionScheme() {
    return connectionScheme;
  }

  /**
   * Setter connectionScheme.
   * @param connectionScheme as int*/
  public void setConnectionScheme(final int connectionScheme) {
    this.connectionScheme = connectionScheme;
  }

  /**
   * Getter txnHash.
   * @return String*/
  public String getTxnHash() {
    return txnHash;
  }

  /**
   * Setter txnHash.
   * @param txnHash as STring*/
  public void setTxnHash(final String txnHash) {
    this.txnHash = txnHash;
  }

  /**
   * Getter hashTsnDayPos.
   * @return String*/
  public String getHashTsnDayPos() {
    return hashTsnDayPos;
  }

  /**
   * Setter hashTsnDayPos.
   * @param hashTsnDayPos as String*/
  public void setHashTsnDayPos(final String hashTsnDayPos) {
    this.hashTsnDayPos = hashTsnDayPos;
  }

  /**
   * Getter targetReference.
   * @return String
   * */
  public String getTargetReference() {
    return targetReference;
  }

  /**
   * Setter targetReference.
   * @param targetReference as String*/
  public void setTargetReference(final String targetReference) {
    this.targetReference = targetReference;
  }

  /**
   * Método hasToCloseChanel.
   * @return boolean
   * */
  public boolean hasToCloseChanel() {
    return closeChannelAtEnd;
  }

  /**
   * Setter closeChannelAtEnd.
   * @param closeChannelAtEnd as boolean*/
  public void setCloseChannelAtEnd(final boolean closeChannelAtEnd) {
    this.closeChannelAtEnd = closeChannelAtEnd;
  }

  /**
   * Método printToConsole.
   * */
  public void printToConsole() {
    Console.writeln(Console.Level.INFO, idOperation, "IDCLIENT=<" + idClient + ">");
  }

  /**
   * Método GetSessionParam.
   * @param paramName as String
   * @return Object
   * */
  public Object getSessionParam(final String paramName) {
    Object paramValue = null;

    if (sessionObjects.containsKey(paramName)) {
      paramValue = sessionObjects.get(paramName);
    }

    return paramValue;
  }

  /**
   * Setter SessionParam.
   * @param paramName as String
   * @param paramValue as Object
   * */
  public void setSessionParam(final String paramName, final Object paramValue) {
    sessionObjects.put(paramName, paramValue);
  }

  /**
   * Getter ip.
   * @return String*/
  public String getIp() {
    return ip;
  }

  /**
   * Setter ip.
   * @param ip as String*/
  public void setIp(final String ip) {
    this.ip = ip;
  }

  /**
   * Método disponse.
   * */
  public void dispose() {
    idOperation = null;
    request = null;
    response = null;
    sessionObjects = null;
    relatedExistentTxnId = null;
    tsn = null;
    posSerial = null;
    idClient = null;
    idClientForTxn = null;
    idPos = null;
    txnReference = null;
    responseCode = null;
    responseMessage = null;
    txnHash = null;
    hashTsnDayPos = null;
    approvalCode = null;
    targetReference = null;
    timeStamp = null;
    originalIdOperation = null;
  }

  /**
   * Getter cipherType.
   * @return byte*/
  public byte getCipherType() {
    return cipherType;
  }

  /**
   * Setter cipherType.
   * @param cipherType as byte*/
  public void setCipherType(final byte cipherType) {
    this.cipherType = cipherType;
  }

  /**
   * Getter GNPRequest.
   * @return MicroserviceRequest
   * */
  public MicroserviceRequest getGNPRequest() {
    return microserviceRequest;
  }

  /**
   * Setter microserviceRequest.
   * @param microserviceRequest as MicroserviceRequest.*/
  public void setGNPRequest(final MicroserviceRequest microserviceRequest) {
    this.microserviceRequest = microserviceRequest;
  }

  /**
   * Getter GNPResponse.
   * @return MicroserviceResponseDTO
   * */
  public MicroserviceResponseDTO getGNPResponse() {
    if (microserviceResponseDTO == null) {
      microserviceResponseDTO = new MicroserviceResponseDTO();
    }
    return microserviceResponseDTO;
  }

  /**
   * Setter microserviceResponseDTO.
   * @param microserviceResponseDTO as MicroserviceResponseDTO*/
  public void setGNPResponse(final MicroserviceResponseDTO microserviceResponseDTO) {
    this.microserviceResponseDTO = microserviceResponseDTO;
  }

  /**
   * Getter TxnByDay.
   * @return TsnbyDayDto
   * */
  public TxnByDayDto getTxnByDay() {
    if (txnByDay == null) {
      txnByDay = new TxnByDayDto();
    }
    return txnByDay;
  }

  /**
   * Setter txnByDay.
   * @param txnByDay as TxnByDayDto*/
  public void setTxnByDay(final TxnByDayDto txnByDay) {
    this.txnByDay = txnByDay;
  }

  /**
   * Getter TxnByDayDao.
   * @return SystemDao
   * */
  public TxnByDayDao getTxnByDayDAO() {
    if (txnByDayDAO == null) {
      txnByDayDAO = BeanUtil.getBean(TxnByDayDaoImpl.class);
    }

    return txnByDayDAO;
  }

  /**
   * Getter SystemDao.
   * @return SystemDao
   * */
  public SystemDao getSystemDAO() {
    if (systemDAO == null) {
      systemDAO = BeanUtil.getBean(SystemDaoImpl.class);
    }
    return systemDAO;
  }

  /**
   * Getter txnByDayOri.
   * @return TxnByDayDto
   * */
  public TxnByDayDto getTxnByDayOri() {
    return txnByDayOri;
  }

  /**
   * Setter txnByDayOri.
   * @param txnByDayOri as TxnByDayDto
   * */
  public void setTxnByDayOri(final TxnByDayDto txnByDayOri) {
    this.txnByDayOri = txnByDayOri;
  }

  /**
   * Getter startTime.
   * @return long
   * */
  public long getStartTime() {
    return startTime;
  }

  /**
   * Setter startTime.
   * @param startTime as long
   * */
  public void setStartTime(final long startTime) {
    this.startTime = startTime;
  }

  /**
   * Getter endTime.
   * @return long
   * */
  public long getEndTime() {
    return endTime;
  }

  /**
   * Setter endTime.
   * @param endTime as long
   * */
  public void setEndTime(final long endTime) {
    this.endTime = endTime;
  }

  /**
   * Getter AtentionTime.
   * @return long
   * */
  public long getAtentionTime() {
    return endTime - startTime;
  }

  /**
   * Setter txnByDayDAO.
   * @param txnByDayDAO as TxnByDayDao
   * */
  public void setTxnByDayDAO(final TxnByDayDao txnByDayDAO) {
    this.txnByDayDAO = txnByDayDAO;
  }

  /**
   * Setter systemDAO.
   * @param systemDAO as SystemDao
   * */
  public void setSystemDAO(final SystemDao systemDAO) {
    this.systemDAO = systemDAO;
  }

  /**
   * Setter originalIdOperation.
   * @param originalIdOperation as String
   * */
  public void setOriginalIdOperation(final String originalIdOperation) {
    this.originalIdOperation = originalIdOperation;
  }

  /**
   * Setter request.
   * @param request as MessageData
   * */
  public void setRequest(final MessageData request) {
    this.request = request;
  }
}
