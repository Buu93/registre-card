package com.smart.auth.model.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Clase TxnByDayDto.java.
 *
 * @author Jose A Rodriguez M
 */
public class TxnByDayDto implements Serializable {
  /**
   * Serilización.
   */
  private static final long serialVersionUID = 1L;
  /**
   * Declaración id.
   */
  private Long id = 0L;
  /**
   * Declaración txnHash.
   */
  private String txnHash = "";
  /**
   * Declaración type.
   */
  private Integer type;
  /**
   * Declaración txnOrig.
   */
  private Integer txnOrig;
  /**
   * Declaración processingCode.
   */
  private String processingCode = "";
  /**
   * Declaración typeReverse.
   */
  private String typeReverse = "";
  /**
   * Declaración amount.
   */
  private Integer amount; //Con dos decimales
  /**
   * Declaración responseCode.
   */
  private String responseCode = "";
  /**
   * Declaración responseMessage.
   */
  private String responseMessage = "";
  /**
   * Declaración status.
   */
  private String status = "";
  /**
   * Declaración idOperation.
   */
  private String idOperation = "";
  /**
   * Declaración posSerial.
   */
  private String posSerial = "";
  /**
   * Declaración tsn.
   */
  private String tsn = "";
  /**
   * Declaración tresp.
   */
  private Long tresp = 0L;
  /**
   * Declaración txnReference.
   */
  private Long txnReference = 0L;
  /**
   * Declaración txnDate.
   */
  private Date txnDate = Calendar.getInstance().getTime();
  /**
   * Declaración reference.
   */
  private String reference = "";
  /**
   * Declaración approvalCode.
   */
  private String approvalCode = "";
  /**
   * Declaración RefSPNum.
   */
  private String refSPNum = "";
  /**
   * Declaración expDate.
   */
  private String expDate = "";
  /**
   * Declaración txnReferenceDataHash.
   */
  private String txnReferenceDataHash = "";

  /**
   * Getter Amount.
   *
   * @return Integer
   */
  public Integer getAmount() {
    return amount;
  }

  /**
   * Setter amount.
   *
   * @param amount as Integer
   */
  public void setAmount(final Integer amount) {
    this.amount = amount;
  }

  /**
   * obtiene el código de respuesta.
   *
   * @return responseCode
   */
  public String getResponseCode() {
    if (!responseCode.isEmpty() && responseCode.length() == 1) {
      responseCode = "0" + responseCode;
    }
    return responseCode;
  }

  /**
   * Setter responseCode.
   *
   * @param responseCode as String
   */
  public void setResponseCode(final String responseCode) {
    this.responseCode = responseCode != null ? responseCode : "";
  }

  /**
   * Getter Status.
   *
   * @return String
   */
  public String getStatus() {
    return status;
  }

  /**
   * Setter Status.
   *
   * @param s as String
   */
  public void setStatus(final String s) {
    this.status = s;
  }

  /**
   * Getter TxnHash.
   *
   * @return String
   */
  public String getTxnHash() {
    return txnHash;
  }

  /**
   * Setter txnHash.
   *
   * @param txnHash as String
   */
  public void setTxnHash(final String txnHash) {
    this.txnHash = txnHash;
  }

  /**
   * Getter Type.
   *
   * @return Integer
   */
  public Integer getType() {
    return type == null ? Integer.valueOf(0) : type;
  }

  /**
   * Setter type.
   *
   * @param type as Integer
   */
  public void setType(final Integer type) {
    this.type = type;
  }

  /**
   * Getter id.
   *
   * @return Long
   */
  public Long getId() {
    return id;
  }

  /**
   * Setter id.
   *
   * @param id as Long
   */
  public void setId(final Long id) {
    this.id = id;
  }

  /**
   * Getter PosSerial.
   *
   * @return String
   */
  public String getPosSerial() {
    return posSerial;
  }

  /**
   * Setter posSerial.
   *
   * @param posSerial as String
   */
  public void setPosSerial(final String posSerial) {
    this.posSerial = posSerial;
  }

  /**
   * Getter Tsn.
   *
   * @return String
   */
  public String getTsn() {
    return tsn;
  }

  /**
   * Setter tsn.
   *
   * @param tsn as String
   */
  public void setTsn(final String tsn) {
    this.tsn = tsn;
  }

  /**
   * Getter TxnDate.
   *
   * @return Date
   */
  public Date getTxnDate() {
    return txnDate;
  }

  /**
   * Setter txnDate.
   *
   * @param txnDate as Date
   */
  public void setTxnDate(final Date txnDate) {
    this.txnDate = txnDate;
  }

  /**
   * Getter TxnReference.
   *
   * @return Long
   */
  public Long getTxnReference() {
    return txnReference;
  }

  /**
   * Setter txnReference.
   *
   * @param txnReference as Long
   */
  public void setTxnReference(final Long txnReference) {
    this.txnReference = txnReference;
  }

  /**
   * Getter Reference.
   *
   * @return String
   */
  public String getReference() {
    return reference;
  }

  /**
   * Setter reference.
   *
   * @param reference as String
   */
  public void setReference(final String reference) {
    this.reference = reference;
  }

  /**
   * Getter IdOperation.
   *
   * @return String
   */
  public String getIdOperation() {
    return idOperation;
  }

  /**
   * Setter idOperation.
   *
   * @param idOperation as String
   */
  public void setIdOperation(final String idOperation) {
    this.idOperation = idOperation;
  }

  /**
   * Getter ResponseMessage.
   *
   * @return String
   */
  public String getResponseMessage() {
    return responseMessage;
  }

  /**
   * Setter responseMessage.
   *
   * @param responseMessage as String
   */
  public void setResponseMessage(final String responseMessage) {
    this.responseMessage = responseMessage;
  }

  /**
   * Getter tResp.
   *
   * @return Long
   */
  public Long gettResp() {
    return tresp;
  }

  /**
   * Setter tresp.
   *
   * @param tresp as Long
   */
  public void settResp(final Long tresp) {
    this.tresp = tresp;
  }

  /**
   * Getter ApprovalCode.
   *
   * @return String
   */
  public String getApprovalCode() {
    return approvalCode;
  }

  /**
   * Setter approvalCode.
   *
   * @param approvalCode as String
   */
  public void setApprovalCode(final String approvalCode) {
    this.approvalCode = approvalCode;
  }

  /**
   * Getter TxnOrig.
   *
   * @return Integer
   */
  public Integer getTxnOrig() {
    return txnOrig;
  }

  /**
   * Setter txnOrig.
   *
   * @param txnOrig as Integer
   */
  public void setTxnOrig(final Integer txnOrig) {
    this.txnOrig = txnOrig;
  }

  /**
   * Getter ProcessingCode.
   *
   * @return String
   */
  public String getProcessingCode() {
    return processingCode;
  }

  /**
   * Setter processingCode.
   *
   * @param processingCode as String
   */
  public void setProcessingCode(final String processingCode) {
    this.processingCode = processingCode;
  }

  /**
   * Getter TypeReverse.
   *
   * @return String
   */
  public String getTypeReverse() {
    return typeReverse;
  }

  /**
   * Setter typeReverse.
   *
   * @param typeReverse as String
   */
  public void setTypeReverse(final String typeReverse) {
    this.typeReverse = typeReverse;
  }

  /**
   * Método toString.
   *
   * @return String
   */
  @Override
  public String toString() {
    return "com.smartbt.gnp.beans.TxnByDayDto[id=" + id + "]";
  }

  /**
   * Getter refSPNum.
   *
   * @return the refSPNum
   */
  public String getRefSPNum() {
    return refSPNum;
  }

  /**
   * Setter RefSPNum.
   *
   * @param refSPNum the RefSPNum to set
   */
  public void setRefSPNum(final String refSPNum) {
    this.refSPNum = refSPNum;
  }

  /**
   * Getter expDate.
   *
   * @return the expDate
   */
  public String getExpDate() {
    return expDate;
  }

  /**
   * Setter expDate.
   *
   * @param expDate the expDate to set
   */
  public void setExpDate(final String expDate) {
    this.expDate = expDate;
  }

  /**
   * Getter txnReferenceDataHash.
   *
   * @return the txnReferenceDataHash
   */
  public String getTxnReferenceDataHash() {
    return txnReferenceDataHash;
  }

  /**
   * Setter txnReferenceDataHash.
   *
   * @param txnReferenceDataHash the txnReferenceDataHash to set
   */
  public void setTxnReferenceDataHash(final String txnReferenceDataHash) {
    this.txnReferenceDataHash = txnReferenceDataHash;
  }
}
