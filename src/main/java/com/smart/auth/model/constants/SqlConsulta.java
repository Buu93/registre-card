package com.smart.auth.model.constants;

/**
 * Clase sqlConsulta.
 * */
public class SqlConsulta {

  /**
   * QUERYS TXXBYDAY_INSERT.
   */
  public static final String TXXBYDAY_INSERT = "INSERT INTO gnp_TxnByDay(txnHash , type , txnOrig"
        + " , processingCode , typeReverse , amount , responseCode , responseMessage , status , "
        + "idOperation , posSerial , tsn , tResp , txnReference,txndate,reference,approvalCode,"
        + "RefSPNum , expDate, txnReferenceDataHash )"
        + "     VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

  /**
   * Query TXXBYDAY_UPDATE.
   * */
  public static final String TXXBYDAY_UPDATE = "UPDATE gnp_TxnByDay "
        + "   SET  txnHash  = ?, type  = ? , txnOrig  = ?, processingCode  = ?, typeReverse  = ?, "
        + "amount  = ?, responseCode  = ? , responseMessage  = ?, status  = ?, idOperation  = ?, "
        + "posSerial  = ?"
        + "      , tsn  =? , tResp  = ?, txnReference  =?, txnDate  = ?, reference  = ? , "
        + "approvalCode  = ?, RefSPNum  = ?, expDate  = ?, txnReferenceDataHash =?  "
        + "  WHERE  id= ?";

  /**
   * Query TXXBYDAY_SELECT.
   * */
  public static final String TXXBYDAY_SELECT = "SELECT * FROM gnp_TxnByDay WHERE txnHash= ? AND "
        + "type in (1,2) AND txnReferenceDataHash= ?  order by id desc LIMIT 1";
  //AND txnReferenceHash


  /**
   * QUERYS POS_INSERT.
   */
  public static final String POS_INSERT = "INSERT INTO gnp_posdata (serialPos,clientId,userName,"
        + "userP,logonlastDate,lastServerKey,timeOut,status,lastDate,statusData) "
        + "     VALUES ( ?, ?, ?, ?, now(), ?, ? , ? ,? ,? )";

  /**
   * Query POS_UPDATE_ALL.
   * */
  public static final String POS_UPDATE_ALL = "UPDATE gnp_PosData"
        + "   SET userName = ?"
        + "      ,userP = ?"
        + "      ,timeOut = ?"
        + "      ,status = ?"
        + "      ,lastDate = ?"
        + "      ,statusData = ?"
        + " WHERE id=?";

  /**
   * Query POS_UPDATE.
   * */
  public static final String POS_UPDATE = "UPDATE gnp_PosData"
        + "   SET logonlastDate = now()"
        + "      ,lastServerKey = ?"
        + "      ,timeOut = ?"
        + "      ,status = ?"
        + "      ,lastDate = ?"
        + " WHERE id=?";

  /**
   * Query POS_UPDATE_STATUS.
   * */
  public static final String POS_UPDATE_STATUS = "UPDATE gnp_PosData "
        + "   SET status = ?"
        + "   WHERE id=?";

  /**
   * Query POS_UPDATE_USER_P.
   * */
  public static final String POS_UPDATE_USER_P = "UPDATE gnp_PosData "
        + "   SET statusData = ?, userP=? "
        + "   WHERE id=?";


  /**
   * Query POS_SELECT.
   * */
  public static final String POS_SELECT =
        "SELECT  * from gnp_PosData where clientId=? order by id desc LIMIT 1";

  /**
   * QUERYS STAN.
   */
  public static final String STAN_INSERT = "INSERT INTO gnp_Stan "
        + "           (serialPos,numStan)"
        + "     VALUES(?,?)";

  /**
   * Query STAN_UPDATE.
   * */
  public static final String STAN_UPDATE = "UPDATE gnp_Stan   SET numStan = ?  WHERE id=? ";

  /**
   * Query STAN_SELECT.
   * */
  public static final String STAN_SELECT = "SELECT * FROM gnp_Stan WHERE serialPos= ? order by id"
        + " desc LIMIT 1";

  /**
   * constructor.
   * */
  protected SqlConsulta() {
    //empty fo test
  }

}
