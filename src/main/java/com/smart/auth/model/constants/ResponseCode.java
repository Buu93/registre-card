/**
-----------------------------------------------------------------------
 * ResponseCode.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
-----------------------------------------------------------------------
 */

package com.smart.auth.model.constants;
/**
 * Clase ResponseCode.java.
 * */
public class ResponseCode {

  /**
   * Constante OK.
   * */
  public static final String OK = "00";
  /**
   * Constante OK_GNP.
   * */
  public static final String OK_GNP = "00";
  /**
   * Constante GRAL_ERROR.
   * */
  public static final String GRAL_ERROR = "99";
  /**
   * Constante VENDOR_ERROR.
   * */
  public static final String VENDOR_ERROR = "01";
  /**
   * Constante SERVICE_ERROR.
   * */
  public static final String SERVICE_ERROR = "02";
  /**
   * Constante CONNECTOR_ERROR.
   * */
  public static final String CONNECTOR_ERROR = "02";

  /**
   * Constructor.
   * */
  protected ResponseCode() {
    //is empty for test
  }
}
