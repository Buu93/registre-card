package com.smart.auth.model.constants;

/**
 * Class TxnStatus.
 *
 * @author Grevam
 */
public class TxnStatus {  

  /**
   * Constante APROVED.
   * */
  public static final String APROVED = "00";
  /**
   * Constante REVERSING.
   * */
  public static final String REVERSING = "01";
  /**
   * Constante DENIED.
   * */
  public static final String DENIED = "02";
  /**
   * Constante REVERSED.
   * */
  public static final String REVERSED = "03";
  /**
   * Constante REVERSE_ANSWERED.
   * */
  public static final String REVERSE_ANSWERED = "04";    //Si el proveedor no responde
  /**
   * Constante DENIED_ANSWERED_REVERSE.
   * */
  public static final String DENIED_ANSWERED_REVERSE = "05"; //Cuando el servicio o el proveedor
  // deniega el reverso la transacción queda en este estatus, el cambio a este estatus ocurre
  // en ReversalDaemon
  /**
   * Constante SENT_TO_VENDOR.
   * */
  public static final String SENT_TO_VENDOR = "06";
  /**
   * Constante REVERSE_NO_ANSWERED.
   * */
  public static final String REVERSE_NO_ANSWERED = "07";    //Si la respuesta no pudo se
  // entregada al cliente
  /**
   * Constante FAILED_NO_ANSWERED_REVERSE.
   * */
  public static final String FAILED_NO_ANSWERED_REVERSE = "08"; //Este estatus
  /**
   * Constante ATTENDED_BY_VENDOR.
   * */
  public static final String ATTENDED_BY_VENDOR = "09";
  /**
   * Constante BALANCE_COMMITED.
   * */
  public static final String BALANCE_COMMITED = "10";
  /**
   * Constante /**.
   * */
  public static final String CANCELED = "11";
  /**
   * Constante CANCELED.
   * */
  public static final String INITIAL_STATUS = "12";
  /**
   * Constante INITIAL_STATUS.
   * */
  public static final String TAE_SUSPICIUS = "13";
  /**
   * Constante TAE_SUSPICIUS.
   * */
  public static final String INVALIDATED_BY_POS_REQUEST = "21";
  /**
   * Constante INVALIDATED_BY_POS_REQUEST.
   * */
  public static final String DUPLICATE_TRANSACTION = "12";

  /**
   * Constructor.
   * */
  protected TxnStatus() {
    //empty for a test.
  }

  /**
   * Method getStatusName.
   * @param txnStatus as String
   * @return String
   */
  public static String getStatusName(final String txnStatus) {
    String txnName;

    switch (txnStatus) {
      case "0":
        txnName = "APROBADA";
        break;
      case APROVED:
        txnName = "APROBADA";
        break;
      case REVERSING:
        txnName = "REVERSANDO";
        break;
      case DENIED:
        txnName = "DENEGADA";
        break;
      case REVERSED:
        txnName = "REVERSADA";
        break;
      case REVERSE_ANSWERED:
        txnName = "REVERSAR - APROBADA";
        break;
      case DENIED_ANSWERED_REVERSE:
        txnName = "REVERSO DENEGADO";
        break;
      case SENT_TO_VENDOR:
        txnName = "ENVIADA A PROVEEDOR";
        break;
      case REVERSE_NO_ANSWERED:
        txnName = "REVERSAR - NO CONTESTADA";
        break;
      default:
        txnName = txtNemeData(txnStatus);
        break;
    }

    return txnName;

  }

  /**
   * Particion de valores por validacion ciclomatica.
   * @param txnStat as String.
   * @return as String.
   */
  public static String txtNemeData(final String txnStat) {
    String txn;

    switch (txnStat) {
      case FAILED_NO_ANSWERED_REVERSE:
        txn = "REVERSO FALLIDO";
        break;
      case ATTENDED_BY_VENDOR:
        txn = "ATENDIDA POR PROVEEDOR";
        break;
      case BALANCE_COMMITED:
        txn = "SALDO COMPROMETIDO";
        break;
      case CANCELED:
        txn = "CANCELADA";
        break;
      case INITIAL_STATUS:
        txn = "NO REALIZADA";
        break;
      case TAE_SUSPICIUS:
        txn = "SIN RESPUESTA";
        break;
      case INVALIDATED_BY_POS_REQUEST:
        txn = "INVALIDA POR PETICION DE POS";
        break;
      default:
        txn = "DESCONOCIDO";
        break;
    }

    return txn;
  }

  /**
   * Method isFinalStatus.
   * @param status as String
   * @return boolean
   */
  public static boolean isFinalStatus(final String status) {
    if (status.equals(APROVED) || status.equals(DENIED)
          || status.equals(REVERSED)) {
      return true;
    } else {
      return status.equals(DENIED_ANSWERED_REVERSE)
            || status.equals(CANCELED)
            || status.equals(TAE_SUSPICIUS)
            || status.equals(DUPLICATE_TRANSACTION);
    }

  }
}
