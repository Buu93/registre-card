/**
 * -----------------------------------------------------------------------
 * ProcessingCode.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 */

package com.smart.auth.model.constants;

/**
 * Clase ProcessingCode.
 * @author Jose A Rodriguez M
 */
public class ProcessingCode {

  /**
   * Declaración variable LOGON.
   * */
  public static final String LOGON = "000100";
  /**
   * Declaración variable SALE.
   * */
  public static final String SALE = "031100";
  /**
   * Declaración variable REVERSAL.
   * */
  public static final String REVERSAL = "030800";
  /**
   * Declaración variable CANCELATION.
   * */
  public static final String CANCELATION = "030700";
  /**
   * Declaración variable REFUND.
   * */
  public static final String REFUND = "030200";
  /**
   * Constante para el tipo de operación alta de cuenta.
   * */
  public static final String APERTURE = "031200";

  /**
   * Constructor.
   * */
  protected ProcessingCode() {
    //is empty for test.
  }
}
