/**
 * ****************************************************************
 * MicroserviceRequest.java
 *
 * @author : Jose Antonio Rodriguez Martinez
 * @creationDate : 11/03/2019
 * @specFile:
 * @revisedBy : Jose Antonio Rodriguez Martinez
 * @date : 11/03/2019
 - *****************************************************************
 */

package com.smart.auth.model;

import com.smart.auth.config.StringVerify;
import com.smart.auth.config.cipher.ServiceEncryptionData;
import com.smart.auth.exception.logs.Console;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * Class MicroserviceRequest.
 * @author Jose A Rodriguez M
 */
public class MicroserviceRequest {

  /** * reintento. */
  private String reintento = "";
  /** * numRecibo. */
  private String numRecibo = "";
  /** * numeroSecuencialTerminal. */
  private String numeroSecuencialTerminal = "";
  /** * oficina. */
  private String oficina = "";
  /** * numTarjeta. */
  private String numTarjeta = "";
  /** * moneda. */
  private String moneda = "";
  /** * idTerminalExterna. */
  private String idTerminalExterna = "";
  /** * subTipoOperacion. */
  private String subTipoOperacion = "";
  /** * mesesDiferidos. */
  private String mesesDiferidos = "";
  /** * numPoliza. */
  private String numPoliza = "";
  /** * reverso. */
  private String reverso = "";
  /** * tipoOperacion. */
  private String tipoOperacion = "";
  /** * lineaNegocio. */
  private String lineaNegocio = "";
  /** * codigoSeguridad. */
  private String codigoSeguridad = "";
  /** * fechaVencimiento. */
  private String fechaVencimiento = "";
  /** * claveAgente. */
  private String claveAgente = "";
  /** * uniqueKey. */
  private String uniqueKey = "";
  /** * prodCom. */
  private String prodCom = "";
  /** * descProd. */
  private String descProd = "";
  /** * banco. */
  private String banco = "";
  /** * idOriginal. */
  private String idOriginal = "";
  /** * importe. */
  private String importe = "";
  /** * tipoPlain. */
  private String tipoPlain = "";
  /** * idExternaComercio. */
  private String idExternaComercio = "";
  /** * condicion1. */
  private String condicion1 = "";
  /** * prodTec. */
  private String prodTec = "";
  /** * idInternaComercio. */
  private String idInternaComercio = "";
  /** * campaign. */
  private String campaign = "";
  /** * caja. */
  private String caja = "";
  /** * cantidadPagos. */
  private String cantidadPagos = "";
  /** * idTerminalOrigen. */
  private String idTerminalOrigen = "";
  /** * referencia. */
  private String referencia = "";
  /** * username. */
  private String username = "";
  /** * numeroAutorizacion. */
  private String numeroAutorizacion = "";

  //Agergados para alta de cuenta

  private String cvv = "";

  private String aliasTarjeta = "";

  private String clientID = "";

  /**
   * Constructor empty.
   */
  public MicroserviceRequest() {
    //empty
  }

  /**
   * MicroserviceRequest.
   * @param reintento String.
   * @param numTarjeta String.
   */
  public MicroserviceRequest(final String reintento, final String numTarjeta) {
    this.reintento = reintento;
    this.numTarjeta = numTarjeta;
  }

  /**
   * MicroserviceRequest.
   * @param tipoOperacion String.
   */
  public MicroserviceRequest(final String tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
  }

  /**
   * getReintento.
   * @return String.
   */
  public String getReintento() {
    return reintento;
  }

  /**
   * setReintento.
   * @param reintento String.
   */
  public void setReintento(final String reintento) {
    this.reintento = reintento;
  }

  /**
   * getNumRecibo.
   * @return String.
   */
  public String getNumRecibo() {
    return numRecibo;
  }

  /**
   * setNumRecibo.
   * @param numRecibo String.
   */
  public void setNumRecibo(final String numRecibo) {
    this.numRecibo = numRecibo;
  }

  /**
   * getNumeroSecuencialTerminal.
   * @return String.
   */
  public String getNumeroSecuencialTerminal() {
    return numeroSecuencialTerminal;
  }

  /**
   * setNumeroSecuencialTerminal.
   * @param numeroSecuencialTerminal String.
   */
  public void setNumeroSecuencialTerminal(final String numeroSecuencialTerminal) {
    this.numeroSecuencialTerminal = numeroSecuencialTerminal;
  }

  /**
   * getOficina.
   * @return String.
   */
  public String getOficina() {
    return oficina;
  }

  /**
   * setOficina.
   * @param oficina String.
   */
  public void setOficina(final String oficina) {
    this.oficina = oficina;
  }

  /**
   * getNumTarjeta.
   * @return String.
   */
  public String getNumTarjeta() {
    return numTarjeta;
  }

  /**
   * setNumTarjeta.
   * @param numTarjeta String.
   */
  public void setNumTarjeta(final String numTarjeta) {
    this.numTarjeta = numTarjeta;
  }

  /**
   * getMoneda.
   * @return String.
   */
  public String getMoneda() {
    return moneda;
  }

  /**
   * setMoneda.
   * @param moneda String.
   */
  public void setMoneda(final String moneda) {
    this.moneda = moneda;
  }

  /**
   * getIdTerminalExterna.
   * @return String.
   */
  public String getIdTerminalExterna() {
    return idTerminalExterna;
  }

  /**
   * setIdTerminalExterna.
   * @param idTerminalExterna String.
   */
  public void setIdTerminalExterna(final String idTerminalExterna) {
    this.idTerminalExterna = idTerminalExterna;
  }

  /**
   * getSubTipoOperacion.
   * @return String.
   */
  public String getSubTipoOperacion() {
    return subTipoOperacion;
  }

  /**
   * setSubTipoOperacion.
   * @param subTipoOperacion String.
   */
  public void setSubTipoOperacion(final String subTipoOperacion) {
    this.subTipoOperacion = subTipoOperacion;
  }

  /**
   * getMesesDiferidos.
   * @return String.
   */
  public String getMesesDiferidos() {
    return mesesDiferidos;
  }

  /**
   * setMesesDiferidos.
   * @param mesesDiferidos String.
   */
  public void setMesesDiferidos(final String mesesDiferidos) {
    this.mesesDiferidos = mesesDiferidos;
  }

  /**
   * getNumPoliza.
   * @return String.
   */
  public String getNumPoliza() {
    return numPoliza;
  }

  /**
   * setNumPoliza.
   * @param numPoliza String.
   */
  public void setNumPoliza(final String numPoliza) {
    this.numPoliza = numPoliza;
  }

  /**
   * getReverso.
   * @return String.
   */
  public String getReverso() {
    return reverso;
  }

  /**
   * setReverso.
   * @param reverso String.
   */
  public void setReverso(final String reverso) {
    this.reverso = reverso;
  }

  /**
   * getTipoOperacion.
   * @return String.
   */
  public String getTipoOperacion() {
    return tipoOperacion;
  }

  /**
   * setTipoOperacion.
   * @param tipoOperacion String.
   */
  public void setTipoOperacion(final String tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
  }

  /**
   * getLineaNegocio.
   * @return String.
   */
  public String getLineaNegocio() {
    return lineaNegocio;
  }

  /**
   * setLineaNegocio.
   * @param lineaNegocio String.
   */
  public void setLineaNegocio(final String lineaNegocio) {
    this.lineaNegocio = lineaNegocio;
  }

  /**
   * getCodigoSeguridad.
   * @return String.
   */
  public String getCodigoSeguridad() {
    return codigoSeguridad;
  }

  /**
   * setCodigoSeguridad.
   * @param codigoSeguridad String.
   */
  public void setCodigoSeguridad(final String codigoSeguridad) {
    this.codigoSeguridad = codigoSeguridad;
  }

  /**
   * getFechaVencimiento.
   * @return String.
   */
  public String getFechaVencimiento() {
    return fechaVencimiento;
  }

  /**
   * setFechaVencimiento.
   * @param fechaVencimiento String.
   */
  public void setFechaVencimiento(final String fechaVencimiento) {
    this.fechaVencimiento = fechaVencimiento;
  }

  /**
   * getClaveAgente.
   * @return String.
   */
  public String getClaveAgente() {
    return claveAgente;
  }

  /**
   * setClaveAgente.
   * @param claveAgente String.
   */
  public void setClaveAgente(final String claveAgente) {
    this.claveAgente = claveAgente;
  }

  /**
   * getUniqueKey.
   * @return String.
   */
  public String getUniqueKey() {
    return uniqueKey;
  }

  /**
   * setUniqueKey.
   * @param uniqueKey String.
   */
  public void setUniqueKey(final String uniqueKey) {
    this.uniqueKey = uniqueKey;
  }

  /**
   * getProdCom.
   * @return String.
   */
  public String getProdCom() {
    return prodCom;
  }

  /**
   * setProdCom.
   * @param prodCom String.
   */
  public void setProdCom(final String prodCom) {
    this.prodCom = prodCom;
  }

  /**
   * getDescProd.
   * @return String.
   */
  public String getDescProd() {
    return descProd;
  }

  /**
   * setDescProd.
   * @param descProd String.
   */
  public void setDescProd(final String descProd) {
    this.descProd = descProd;
  }

  /**
   * getBanco.
   * @return String.
   */
  public String getBanco() {
    return banco;
  }

  /**
   * setBanco.
   * @param banco String.
   */
  public void setBanco(final String banco) {
    this.banco = banco;
  }

  /**
   * getIdOriginal.
   * @return String.
   */
  public String getIdOriginal() {
    return idOriginal;
  }

  /**
   * setIdOriginal.
   * @param idOriginal String.
   */
  public void setIdOriginal(final String idOriginal) {
    this.idOriginal = idOriginal;
  }

  /**
   * getImporte.
   * @return String.
   */
  public String getImporte() {
    return importe;
  }

  /**
   * setImporte.
   * @param importe String.
   */
  public void setImporte(final String importe) {
    this.importe = importe;
  }

  /**
   * getTipoPlain.
   * @return String.
   */
  public String getTipoPlain() {
    return tipoPlain;
  }

  /**
   * setTipoPlain.
   * @param tipoPlain String.
   */
  public void setTipoPlain(final String tipoPlain) {
    this.tipoPlain = tipoPlain;
  }

  /**
   * getIdExternaComercio.
   * @return String.
   */
  public String getIdExternaComercio() {
    return idExternaComercio;
  }

  /**
   * setIdExternaComercio.
   * @param idExternaComercio String.
   */
  public void setIdExternaComercio(final String idExternaComercio) {
    this.idExternaComercio = idExternaComercio;
  }

  /**
   * getCondicion1.
   * @return String.
   */
  public String getCondicion1() {
    return condicion1;
  }

  /**
   * setCondicion1.
   * @param condicion1 String.
   */
  public void setCondicion1(final String condicion1) {
    this.condicion1 = condicion1;
  }

  /**
   * getProdTec.
   * @return String.
   */
  public String getProdTec() {
    return prodTec;
  }

  /**
   * setProdTec.
   * @param prodTec String.
   */
  public void setProdTec(final String prodTec) {
    this.prodTec = prodTec;
  }

  /**
   * getCampaign.
   * @return String.
   */
  public String getCampaign() {
    return campaign;
  }

  /**
   * setCampaign.
   * @param campaign String.
   */
  public void setCampaign(final String campaign) {
    this.campaign = campaign;
  }

  /**
   * getIdInternaComercio.
   * @return String.
   */
  public String getIdInternaComercio() {
    return idInternaComercio;
  }

  /**
   * setIdInternaComercio.
   * @param idInternaComercio String.
   */
  public void setIdInternaComercio(final String idInternaComercio) {
    this.idInternaComercio = idInternaComercio;
  }

  /**
   * getCaja.
   * @return String.
   */
  public String getCaja() {
    return caja;
  }

  /**
   * setCaja.
   * @param caja String.
   */
  public void setCaja(final String caja) {
    this.caja = caja;
  }

  /**
   * getCantidadPagos.
   * @return String.
   */
  public String getCantidadPagos() {
    return cantidadPagos;
  }

  /**
   * setCantidadPagos.
   * @param cantidadPagos String.
   */
  public void setCantidadPagos(final String cantidadPagos) {
    this.cantidadPagos = cantidadPagos;
  }

  /**
   * getIdTerminalOrigen.
   * @return String.
   */
  public String getIdTerminalOrigen() {
    return idTerminalOrigen;
  }

  /**
   * setIdTerminalOrigen.
   * @param idTerminalOrigen String.
   */
  public void setIdTerminalOrigen(final String idTerminalOrigen) {
    this.idTerminalOrigen = idTerminalOrigen;
  }

  /**
   * getReferencia.
   * @return String.
   */
  public String getReferencia() {
    return referencia;
  }

  /**
   * setReferencia.
   * @param referencia String.
   */
  public void setReferencia(final String referencia) {
    this.referencia = referencia;
  }

  /**
   * getUsername.
   * @return String.
   */
  public String getUsername() {
    return username;
  }

  /**
   * setUsername.
   * @param username String.
   */
  public void setUsername(final String username) {
    this.username = username;
  }

  /**
   * getNumeroAutorizacion.
   * @return String.
   */
  public String getNumeroAutorizacion() {
    return numeroAutorizacion;
  }

  /**
   * setNumeroAutorizacion.
   * @param numeroAutorizacion String.
   */
  public void setNumeroAutorizacion(final String numeroAutorizacion) {
    this.numeroAutorizacion = numeroAutorizacion;
  }

  /**
   * getXmlMsg.
   * @return String.
   */
  public String getXmlMsg() {
    return getXmlMsg(false);
  }

  /**
   * Method getXmlMsg.
   * @param tramaToLog boolean.
   * @return String.
   */
  public String getXmlMsg(final boolean tramaToLog) {
    XStream xstream = new XStream(new DomDriver()); // does not require XPP3 library
    if (tramaToLog) {
      maskData();
    }
    xstream.alias("arg0", MicroserviceRequest.class);
    StringBuilder xml = new StringBuilder();
    xml.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
          + "xmlns:ser=\"http://services.ibm.gnp.com/\">");
    xml.append("\n<soapenv:HeaderDto/>");
    xml.append("\n<soapenv:Body>");
    xml.append("\n<ser:cobranza>\n");
    xml.append(xstream.toXML(this));
    xml.append("\n</ser:cobranza>");
    xml.append("\n</soapenv:Body>");
    xml.append("\n</soapenv:Envelope>");
    return xml.toString();
  }


  /**
   * Method createObject.
   * @param xmlData String.
   * @param idOperation String.
   * @return MicroserviceRequest
   */
  public static MicroserviceRequest createObject(final String xmlData, final String idOperation) {
    XStream xstream = new XStream(new DomDriver()); // does not require XPP3 library
    xstream.alias("arg0", MicroserviceRequest.class);
    MicroserviceRequest data = null;
    try {
      data = (MicroserviceRequest) xstream.fromXML(xmlData);
    } catch (Exception e) {
      Console.writeException(idOperation, e);
    }
    return data;
  }

  /**
   * Values de pojo MicroserviceRequest.
   * @return String
   */
  @Override
  public String toString() {
    return "MicroserviceRequest{"
          + "reintento='" + reintento + '\'' 
          + ", numRecibo='" + numRecibo + '\'' 
          + ", numeroSecuencialTerminal='" + numeroSecuencialTerminal + '\'' 
          + ", oficina='" + oficina + '\'' 
          + ", numTarjeta='" + numTarjeta + '\'' 
          + ", moneda='" + moneda + '\'' 
          + ", idTerminalExterna='" + idTerminalExterna + '\'' 
          + ", subTipoOperacion='" + subTipoOperacion + '\'' 
          + ", mesesDiferidos='" + mesesDiferidos + '\'' 
          + ", numPoliza='" + numPoliza + '\'' 
          + ", reverso='" + reverso + '\'' 
          + ", tipoOperacion='" + tipoOperacion + '\'' 
          + ", lineaNegocio='" + lineaNegocio + '\'' 
          + ", codigoSeguridad='" + codigoSeguridad + '\'' 
          + ", fechaVencimiento='" + fechaVencimiento + '\'' 
          + ", claveAgente='" + claveAgente + '\'' 
          + ", uniqueKey='" + uniqueKey + '\'' 
          + ", prodCom='" + prodCom + '\'' 
          + ", descProd='" + descProd + '\'' 
          + ", banco='" + banco + '\'' 
          + ", idOriginal='" + idOriginal + '\'' 
          + ", importe='" + importe + '\'' 
          + ", tipoPlain='" + tipoPlain + '\'' 
          + ", idExternaComercio='" + idExternaComercio + '\'' 
          + ", condicion1='" + condicion1 + '\'' 
          + ", prodTec='" + prodTec + '\'' 
          + ", idInternaComercio='" + idInternaComercio + '\'' 
          + ", campaign='" + campaign + '\'' 
          + ", caja='" + caja + '\'' 
          + ", cantidadPagos='" + cantidadPagos + '\'' 
          + ", idTerminalOrigen='" + idTerminalOrigen + '\'' 
          + ", referencia='" + referencia + '\'' 
          + ", username='" + username + '\'' 
          + ", numeroAutorizacion='" + numeroAutorizacion + '\'' 
          + '}';
  }

  /**
   * Method maskData.
   */
  private void maskData() {
    if (numTarjeta != null && !numTarjeta.isEmpty()) {
      numTarjeta = ServiceEncryptionData.maskAccount(numTarjeta);
    }

    if (codigoSeguridad != null && !codigoSeguridad.isEmpty()) {
      codigoSeguridad = StringVerify.getFormatAlpha("X", codigoSeguridad.length(), "X", true);
    }

    if (fechaVencimiento != null && !fechaVencimiento.isEmpty()) {
      fechaVencimiento = StringVerify.getFormatAlpha("X", fechaVencimiento.length(), "X", true);
    }
  }

  public String getCvv() {
    return cvv;
  }

  public void setCvv(String cvv) {
    this.cvv = cvv;
  }

  public String getAliasTarjeta() {
    return aliasTarjeta;
  }

  public void setAliasTarjeta(String aliasTarjeta) {
    this.aliasTarjeta = aliasTarjeta;
  }

  public String getClientID() {
    return clientID;
  }

  public void setClientID(String clientID) {
    this.clientID = clientID;
  }
}
