/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception;

import com.smart.auth.service.error.IErrorServices;
import com.smart.auth.service.impl.operation.OperationResponse;

/**
 * Class CodeMessageException.java.
 *
 * @author Carlos Lopez
 */
public class CodeMessageException extends Exception {

  /** * detail. */
  private final String detail;
  /** * OperationResponse. */
  private final OperationResponse response;



  /**
   * constructor.
   * @param response OperationResponse.
   * @param message String.
   */
  public CodeMessageException(final OperationResponse response, final String message) {

    super(message);
    this.response = response;
    detail = "";
  }

  /**
   * constructor.
   * @param response OperationResponse.
   * @param message String.
   * @param detail String.
   */
  public CodeMessageException(final OperationResponse response, final String message, final String detail) {
    super(message);
    this.detail = detail;
    this.response = response;
  }


  /**
   * constructor.
   * @param error Error.
   *
   */
  public CodeMessageException(final IErrorServices error) {
    super(error.getErrorMessage());
    this.response = null;
    detail = "";

  }

  /**
   * getDetail.
   * @return String.
   */
  public String getDetail() {
    return detail;
  }

  /**
   * getResponse.
   * @return OperationResponse.
   */
  public OperationResponse getResponse() {
    return response;
  }

}
