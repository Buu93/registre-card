/*******************************************************************
 SystemException.java
 @author : Carlos A Lopez P
 @creationDate : 08/01/2019
 @specFile:
 @revisedBy : Carlos A Lopez P
 @date : 08/01/2019
 *******************************************************************/


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception;

import com.smart.auth.service.error.IErrorServices;

/**
 * Class SystemException.java.
 *
 */
public class SystemException extends CommonOperationException {
  /**
   * Constructor SystemException.
   * @param code String.
   * @param message String.
   */
  public SystemException(final String code, final String message) {
    super(code, message);
  }

  /**
   * Constructor SystemException.
   * @param error IErrorServices.
   */
  public SystemException(final IErrorServices error) {
    super(error.getErrorCode(), error.getErrorMessage());
  }
}

//<editor-fold defaultstate="collapsed" desc="Modifications comments">

/************************************************************
 * @updateDate: dd/MM/yyyy
 * @author: ?
 * @revisedBy : ?
 *           @date : dd/MM/yyyy
 * @Description:
 *   (Write text here)
 * ************************************************************
 *  *
 *  *
 *  *
 * ************************************************************
 */

//</editor-fold>

