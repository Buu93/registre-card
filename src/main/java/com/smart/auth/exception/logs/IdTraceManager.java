/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception.logs;

import com.smart.auth.model.enumutils.Number;
import java.util.Calendar;

/**
 * Class IdTraceManager.
 * @author Jose A Rodriguez M
 */
public final class IdTraceManager {

  /** * Map THREADIDOPERATION. */
  private static final ThreadLocal<String> THREADIDOPERATION;

  /** * Constructor empty. */
  private IdTraceManager() {
    //empty
  }

  static {
    THREADIDOPERATION = new ThreadLocal<>();
  }

  /**
   * Method setIdTrace.
   * @param idTrace String.
   */
  public static void setIdTrace(final String idTrace) {
    String cadena;
    if (idTrace == null) {
      cadena = String.format("S%1$td%1$tm%1$tY%1$tH%1$tM%1$tS%2$04d", Calendar.getInstance(), 1);
    } else {
      cadena = idTrace.length() < Number.CODE20.value() ? idTrace : idTrace.substring(0, Number.CODE19.value());
    }
    THREADIDOPERATION.set(cadena);
  }

  /**
   * getIdTrace.
   * @return String.
   */
  public static String getIdTrace() {
    return THREADIDOPERATION.get();
  }

  /**
   * Method normalize.
   * @param idOperation String.
   * @return String.
   */
  public static String normalize(final String idOperation) {
    String idOpThread = getIdTrace();

    if (idOperation != null) {
      return idOperation.length() < Number.CODE20.value() ? idOperation
            : idOperation.substring(0, Number.CODE19.value());
    }

    return  idOpThread != null ? idOpThread : String.format("S"
            + "%1$td%1$tm%1$tY%1$tH%1$tM%1$tS%2$04d", Calendar.getInstance(), 1);
  }
}
