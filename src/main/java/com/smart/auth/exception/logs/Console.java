/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception.logs;

import com.smart.auth.service.impl.error.SystemSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;


/**
 * Class Console.
 *
 * @author Jose A Rodriguez M
 */

public final class Console {
  /**
   * Instancia de registro de eventos.
   */
  private static final Logger LOGGER = LogManager.getLogger(Console.class);

  /**
   * declaración variable vacio.
   */
  private static String vacio = "";

  /**
   * CHANGLINE.
   */
  private static final String CHANGLINE = vacio + ((char) 13) + ((char) 10);
  /**
   * knowexceptions.
   */
  private static Set<String> knowexceptions = new HashSet<>();
  /**
   * STLEVELS.
   */
  private static final String STLEVELS = "INFO,ERROR,FATAL,DEBUG,WARNING";
  /**
   * LINE.
   */
  private static final String LINE =
        "-------------------------------------------------------------------------------";
  /**
   * INFO.
   */
  private static final String INFO = "INFO";
  /**
   * WARNING.
   */
  private static final String WARNING = "WARNING";
  /**
   * ERROR.
   */
  private static final String ERROR = "ERROR";
  /**
   * FATAL.
   */
  private static final String FATAL = "FATAL";
  /**
   * DEBUG.
   */
  private static final String DEBUG = "DEBUG";


  /**
   * Values message.
   */
  public enum Level {
    /**
     * evento DEBUG.
     */
    DEBUG,
    /**
     * evento INFO.
     */
    INFO,
    /**
     * evento ERROR.
     */
    ERROR,
    /**
     * evento defaul.
     */
    DEFAULT
  }

  /**
   * Constructor empty.
   * */
  protected Console() {
    //empty
  }

  /**
   * Constructor.
   *
   * @param hashSet as HashSet.
   */
  protected Console(final HashSet hashSet) {
    knowexceptions = hashSet;
  }

  /**
   * Configuracion de properties.
   */
  public static void initialize() {
    //empty for test
  }


  /**
   * Method writeln.
   *
   * @param level Level.
   * @param msg   String.
   */
  public static void writeln(final Level level, final String msg) {
    writeln(level, "", msg);
  }

  /**
   * Method writeln.
   *
   * @param level       Level.
   * @param idOPeration String.
   * @param msg         String.
   */
  public static void writeln(final Level level, final String idOPeration, final String msg) {
    switch (level) {
      case DEBUG:
        debug(msg, idOPeration);
        break;
      case INFO:
        add(msg, INFO, idOPeration);
        break;
      case ERROR:
        add(msg, ERROR, idOPeration);
        break;
      default:
        break;
    }
  }

  /**
   * writeException.
   *
   * @param ex Throwable.
   */
  public static void writeException(final Throwable ex) {
    logException(null, ex);
  }

  /**
   * writeException.
   *
   * @param idOperation String.
   * @param ex          Throwable.
   */
  public static void writeException(final String idOperation, final Throwable ex) {
    logException(idOperation, ex);
  }

  /**
   * Adds a smartMicroservicio.exception record to error logger.
   *
   * @param idOperation String.
   * @param ex          Exception error.
   */
  static synchronized void logException(final String idOperation, final Throwable ex) {
    logException(null, ex, idOperation);
  }

  //fin de quitar

  /**
   * Adds a smartMicroservicio.exception record to error logger.
   *
   * @param msg     MessageDto for display error
   * @param thex    Exception error
   * @param idOprtn Error generate by
   */
  public static synchronized void logException(final String msg, final Throwable thex, final String idOprtn) {

    String idOperation = idOprtn;
    Throwable ex = thex;

    if (msg == null || ex == null) {
      return;
    }

    if (idOperation == null) {
      idOperation = "";
    }

    String exName = ex.getClass().getSimpleName();
    String canonicalName = ex.getClass().getCanonicalName();
    String exMessage = ex.getMessage();
    String fullExMessage = ex.toString();

    StringBuilder stErrorM = new StringBuilder();

    if (knowexceptions.contains(fullExMessage) || knowexceptions.contains(exName)
          || knowexceptions.contains(canonicalName) || knowexceptions.contains(exMessage)) {
      validException(msg, stErrorM, ex, idOperation);
    } else {
      validMsg(msg, stErrorM, ex, idOperation);

      stErrorM.append("[").append(idOperation).append("] " + LINE);
    }
    LOGGER.fatal(stErrorM);

  }

  /**
   * validException.
   * @param msg as String
   * @param stErrorM as StringBuilder
   * @param throwable as Throwable
   * @param idOperation as String
   */
  public static void validException(final String msg, final StringBuilder stErrorM,
                                     final Throwable throwable, final String idOperation) {
    Throwable ex = throwable;
    stErrorM.append("[").append(idOperation).append("] " + "-- Known smartMicroservicio.exception:  [")
          .append(ex.toString()).append("]");
    if (msg != null) {
      stErrorM.append(" - ").append(msg);
    }
  }

  /**
   * metodo separado por validacion de sonar.
   * @param msg as String
   * @param stErrorM as StringBuilder
   * @param throwable as Throwable
   * @param idOperation as String
   */
  public static void validMsg(final String msg, final StringBuilder stErrorM,
                               final Throwable throwable, final String idOperation) {
    Throwable ex = throwable;
    if (msg != null) {
      stErrorM.append("[").append(idOperation).append("] " + LINE).append(CHANGLINE);
      stErrorM.append("[").append(idOperation).append("] " + "-- Exception Msg:  [")
            .append(msg).append("]").append(CHANGLINE);
      stErrorM.append("[").append(idOperation).append("] " + LINE).append(CHANGLINE);
    }
    if (ex != null) {
      logStackTrace(ex, idOperation, stErrorM);
      ex = ex.getCause();
      while (ex != null) {
        stErrorM.append("[")
              .append(idOperation)
              .append("] " + "-- Caused by:  [")
              .append(ex.getClass().getCanonicalName())
              .append(" - ")
              .append(ex.getMessage())
              .append("]")
              .append(CHANGLINE);
        logStackTrace(ex, idOperation, stErrorM);
        ex = ex.getCause();
      }

    }
  }

  /**
   * logStackTrace.
   *
   * @param ex          Throwable.
   * @param idOperation String.
   * @param message     StringBuilder.
   */
  private static void logStackTrace(final Throwable ex, final String idOperation, final StringBuilder message) {
    StackTraceElement[] exceptionTrace = ex.getStackTrace();

    message.append("[")
          .append(idOperation)
          .append("] "
                + "-------------------------------------------------------------------------------")
          .append(CHANGLINE);
    message.append("[")
          .append(idOperation)
          .append("] " + "-- Exception Error: [")
          .append(ex)
          .append("]")
          .append(CHANGLINE);

    for (StackTraceElement exceptionTrace1 : exceptionTrace) {
      message.append("[")
            .append(idOperation)
            .append("]" + "-- Exception Metod: [")
            .append(exceptionTrace1.getClassName())
            .append(":")
            .append(exceptionTrace1.getMethodName())
            .append("]-" + "[")
            .append(exceptionTrace1.getLineNumber())
            .append("]").append(CHANGLINE);
    }

  }

  /**
   * Verifies if error level logger is in time, if time into file log/log.lsg.
   * is same or less than current date, log/log.lsg will be deleted and logger.
   * error doesn't add record.
   *
   * @return True if is valid
   */
  /**
   * Add a debug record to error logger.
   *
   * @param stMessage  MessageDto to display
   * @param stElemento MessageDto generate by
   */
  private static synchronized void debug(final String stMessage, final String stElemento) {
    add(stMessage, DEBUG, stElemento);
  }

  /**
   * Add a new record to SmartGate logger.
   *
   * @param stMessage  MessageDto to add
   * @param stLog      Logging level if is error or debug level record is added
   *                   to error logger
   * @param stelemento MessageDto generate by
   */
  public static synchronized void add(final String stMessage, final String stLog, final String stelemento) {
    String stLogType = stLog;
    String stElemento = stelemento;

    if (stElemento == null) {
      stElemento = "";
    }
    stLogType = validate(stLogType);

    if (stLogType.equals(INFO)) {
      LOGGER.info("[" + stElemento + "] " + stMessage);
    } else if (stLogType.equals(WARNING)) {
      LOGGER.warn("[" + stElemento + "] " + stMessage);
    } else if (stLogType.equals(ERROR)) {
      LOGGER.error("[" + stElemento + "] " + stMessage);
    } else if (stLogType.equals(FATAL)) {
      LOGGER.fatal("[" + stElemento + "] " + stMessage);
    } else if (stLogType.equals(DEBUG)) {
      LOGGER.info("[" + stElemento + "] " + stMessage);
    }
  }

  /**
   * Validates if is a correct loggin level.
   *
   * @param stLogType Loggin level to verify
   * @return True if is valid
   */
  public static synchronized String validate(final String stLogType) {
    if (STLEVELS.contains(stLogType.toUpperCase(SystemSettings.getLocale()))) {
      return stLogType.toUpperCase(SystemSettings.getLocale());
    } else {
      return INFO;
    }
  }

}
