/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception.logs;

import com.smart.auth.model.enumutils.Number;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Class SystemLog.
 *
 * @author Jose A Rodriguez M
 */
public class SystemLog implements Serializable {

  private static final long serialVersionUID = 1L;
  /**
   * ID.
   */
  private Integer id;
  /**
   * idOperation.
   */
  private String idOperation;
  /**
   * counter.
   */
  private static int counter = 1;

  /**
   * Constructor empty.
   */
  public SystemLog() {
    //Method empty
  }

  /**
   * Method newIdOperation.
   *
   * @return String
   */
  public static synchronized String newIdOperation() {
    if (counter > Number.CODE9999.value()) {
      counter = 1;
    }
    IdTraceManager.setIdTrace(String.format("S%1$td%1$tm%1$tY%1$tH%1$tM%1$tS%2$04d",
          Calendar.getInstance(), counter++));
    return IdTraceManager.getIdTrace();
  }

  /**
   * Method newTxnIdOperation.
   *
   * @return String
   */
  public static synchronized String newTxnIdOperation() {
    if (counter > Number.CODE9999.value()) {
      counter = 1;
    }
    IdTraceManager.setIdTrace(String.format("T%1$td%1$tm%1$tY%1$tH%1$tM%1$tS%2$04d",
          Calendar.getInstance(), counter++));
    return IdTraceManager.getIdTrace();
  }

  /**
   * setIdOperation.
   *
   * @param idOperation as String.
   */
  public void setIdOperation(final String idOperation) {
    this.idOperation = IdTraceManager.normalize(idOperation);
  }

  /**
   * getId.
   *
   * @return Integer.
   */
  public Integer getId() {
    return id;
  }

  /**
   * getIdOperation.
   *
   * @return String.
   */
  public String getIdOperation() {
    return idOperation;
  }

  /**
   * setId.
   *
   * @param id Integer.
   */
  public void setId(final Integer id) {
    this.id = id;
  }

  /**
   * Method writeException.
   *
   * @param ex Throwable.
   */
  public static void writeException(final Throwable ex) {
    String idOp = IdTraceManager.getIdTrace();

    if (idOp == null) {
      idOp = "DefaultOp";
    }

    writeException(idOp, ex);
  }

  /**
   * Method writeException.
   *
   * @param operation as String.
   * @param ex        as Throwable.
   */
  public static void writeException(final String operation, final Throwable ex) {
    String idOperation = operation;
    if (idOperation == null || idOperation.isEmpty()) {
      idOperation = IdTraceManager.getIdTrace();
    }
    Console.writeln(Console.Level.ERROR, idOperation, String.format("%1$td/%1$tm/%1$tY "
                + "%1$tH:%1$tM:%1$tS - (" + idOperation + ") - Comienza Excepción",
          Calendar.getInstance()));
    Console.writeException(idOperation, ex);
  }

  /**
   * writeEntry.
   *
   * @param idOperation as String.
   * @param entry       as String.
   */
  public static void writeEntry(final String idOperation, final String entry) {
    Console.writeln(Console.Level.INFO, idOperation, entry);
  }


  /**
   * hashCode.
   *
   * @return int.
   */
  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  /**
   * Method Valid equals.
   *
   * @param object as Object.
   * @return boolean.
   */
  @Override
  public boolean equals(final Object object) {

    if (!(object instanceof SystemLog)) {
      return false;
    }
    SystemLog other = (SystemLog) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  /**
   * Method toString.
   *
   * @return String.
   */
  @Override
  public String toString() {
    return "com.smartbt.gnp.smartMicroservicio.logs.SystemLog[id=" + id + "]";
  }

  /**
   * setCounter.
   *
   * @param counter as int.
   */
  public static void setCounter(final int counter) {
    SystemLog.counter = counter;
  }
}
