package com.smart.auth.exception;


import com.smart.auth.exception.message.CoreErrorServicesTable;
import com.smart.auth.service.error.IErrorServices;

/**
 * Class DbException.
 */
public class DbException extends MicroserviceException {

  /**
   * Constructor set idOperation.
   * @param idOperation String.
   */
  public DbException(final String idOperation) {
    super(CoreErrorServicesTable.DB_ERROR, idOperation);
  }

  /**
   * Constructor set IErrorServices,idOperation.
   * @param error IErrorServices.
   * @param idOperation String.
   */
  public DbException(final IErrorServices error, final String idOperation) {
    super(error, idOperation);
  }

  /**
   * Constructor set IErrorServices,idOperation.
   * @param error IErrorServices.
   *
   */
  public DbException(final IErrorServices error) {
    super(error);
  }

}
