/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception;

/**
 * Class AppGnpException.java.
 *
 * @author Carlos Lopez
 */
public class AppMicroserviceException extends Exception {

  /** * Error. */
  private final Error error;
  /** * idOperation. */
  private final String idOperation;

  /**
   * constructor.
   * @param error Error.
   */
  public AppMicroserviceException(final Error error) {
    super(error.message);
    this.error = error;
    idOperation = "";
  }

  /**
   * Contructor vacio.
   */
  public AppMicroserviceException() {
    this.idOperation = null;
    this.error = Error.INTERNAL_ERROR;
  }
  /**
   * constructor.
   * @param cause Throwable.
   * @param idOperation String.
   */
  public AppMicroserviceException(final Throwable cause, final String idOperation) {
    super(Error.INTERNAL_ERROR.message, cause);
    this.idOperation = idOperation;
    this.error = Error.INTERNAL_ERROR;
  }

  /**
   * Code error enum.
   */
  public enum Error {

    /** * INVALID_OPERATION. */
    INVALID_OPERATION("01", "Operación no soportada"),
    /** * INTERNAL_ERROR. */
    INTERNAL_ERROR("02", "Error interno");

    /** * message. */
    private final String message;
    /** * errorCode. */
    private final String errorCode;

    /**
     * SubMetodo Error.
     * @param errorCode String.
     * @param message String.
     */
    Error(final String errorCode, final String message) {
      this.message = message;
      this.errorCode = errorCode;
    }
  }

  /**
   * getCode.
   * @return String.
   */
  public String getCode() {
    return error.errorCode;
  }

  /**
   * getCodeMessage.
   * @return String.
   */
  public String getCodeMessage() {
    return error.message;
  }

  /**
   * getIdOperation.
   * @return String.
   */
  public String getIdOperation() {
    return idOperation;
  }
}
