/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception.message;


import com.smart.auth.config.HashMapSTypes;

/**
 * Class MessageData.java.
 *
 * @author Jose A Rodriguez M
 */
public class MessageData extends HashMapSTypes {

  /** * AMOUNT. */
  public static final String AMOUNT = "AMOUNT";
  /** * POS_KEY. */
  public static final String POS_KEY = "MAC_ADDRESS";
  /** * CRYPTO_KEY. */
  public static final String CRYPTO_KEY = "CRYPTO_KEY";
  /** * DATE. */
  public static final String DATE = "DATE";
  /** * TIME. */
  public static final String TIME = "TIME";
  /** * DATETIME. */
  public static final String DATETIME = "DATETIME"; //DDMMYYHHmmss


  /**
   * Constructor MessageData set idOperation.
   * @param idOperation String.
   */
  public MessageData(final String idOperation) {
    super(idOperation);
  }

}
