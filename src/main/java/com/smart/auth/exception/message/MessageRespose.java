package com.smart.auth.exception.message;

import com.smart.auth.model.enumutils.ErrorType;
import com.smart.auth.service.error.IErrorServices;

/**
 * Class MessageRespose for set message.
 */
public final class MessageRespose implements IErrorServices {
  /**
   * Codigo de error.
   */
  private String code = "";
  /**
   * Mensaje de error.
   */
  private String message = "";
  /**
   * Tipo de errror.
   */
  private ErrorType type = null;

  /**
   * MessageRespose Set value data.
   * @param msg as String
   * @param cod as String
   */
  public MessageRespose(final String msg, final String cod) {
    code = cod;
    message = msg;
    type  = ErrorType.CORE_ERROR;
  }

  /**
   * Metodo codigo de error implementado de IErrorServices.
   * @return as String
   */
  @Override
  public String getErrorCode() {
    return code;
  }


  /**
   * Metodo mensaje de error implementado de IErrorServices.
   * @return as String
   */
  @Override
  public String getErrorMessage() {
    return message;
  }


  /**
   * Metodo tipo de error implementado de IErrorServices.
   * @return as String
   */
  @Override
  public ErrorType getErrorType() {
    return type;
  }
}
