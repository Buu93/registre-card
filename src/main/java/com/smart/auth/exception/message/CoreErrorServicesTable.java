package com.smart.auth.exception.message;

import com.smart.auth.model.enumutils.ErrorType;
import com.smart.auth.service.error.IErrorServices;

/**
 * Clase CoreErrorServicesTable.java.
 * */
public enum CoreErrorServicesTable implements IErrorServices {

  /**
   * Constante UNKNOWN_ERROR.
   * */
  UNKNOWN_ERROR("99", "CODIGO DE ERROR DESCONOCIDO"),
  /**
   * Constante OK.
   * */
  OK("200", "APROBADA"),
  /**
   * Constante INVALID_OPERATION.
   * */
  INVALID_OPERATION("01", "Operación no soportada"),
  /**
   * Constante INVALID_POS.
   * */
  INVALID_POS("170", "TRN. ORIGINAL NO EXISTE"),
  /**
   * Constante DB_INSERT_ERROR.
   * */
  DB_INSERT_ERROR("03", "ERROR AL INSERTAR OBJETO EN BASE DE DATOS"),
  /**
   * Constante DB_UPDATE_ERROR.
   * */
  DB_UPDATE_ERROR("04", "ERROR AL ACTUALIZAR BASE DE DATOS"),
  /**
   * Constante INVALID_LICENCE.
   * */
  INVALID_LICENCE("05", "LICENCIA NO VALIDA"),
  /**
   * Constante MAINTENENCE_MODE.
   * */
  MAINTENENCE_MODE("06", "SISTEMA EN MANTENIMIENTO"),
  /**
   * Constante UNSUPPORTED_CORE_TXN.
   * */
  UNSUPPORTED_CORE_TXN("07", "TRANSACCION NO SOPORTADA POR EL CORE"),
  /**
   * Constante INCOMPLETE_PARAMETERS.
   * */
  INCOMPLETE_PARAMETERS("08", "PARAMETROS INCOMPLETOS"),
  /**
   * Constante UNAVAILABLE_SERVICE.
   * */
  UNAVAILABLE_SERVICE("09", "SERVICIO NO DISPONIBLE"),
  /**
   * Constante INVALID_CRYPTED_DATA.
   * */
  INVALID_CRYPTED_DATA("10", "ERROR DESENCRIPTANDO DATOS"),
  /**
   * Constante QUERY_DATA_ERROR.
   * */
  QUERY_DATA_ERROR("11", "ERROR EN CONSULTA SQL"),
  /**
   * Constante QUERY_TXN_REFERENCE.
   * */
  QUERY_TXN_REFERENCE("12", "ERROR CONSULTANDO REFERENCIA DE TXN"),
  /**
   * Constante EXISTENT_TSN.
   * */
  EXISTENT_TSN("13", "TSN REPETIODO PARA EL POS"),
  /**
   * Constante SERVICE_TIME_OUT.
   * */
  SERVICE_TIME_OUT("14", "TIME OUT"),
  /**
   * Constante VENDOR_NO_RESPOND.
   * */
  VENDOR_NO_RESPOND("15", "SIN RESPUESTA DEL PROVEEDOR"),
  /**
   * Constante CANT_PARSE_RESPONSE.
   * */
  CANT_PARSE_RESPONSE("16", "IMPOSIBLE INTERPRETAR LA RESPUESTA"),
  /**
   * Constante UNSUPPORTED_SERVICE_TXN.
   * */
  UNSUPPORTED_SERVICE_TXN("17", "TRANSACCION NO SOPORTADA POR EL SERVICIO"),
  /**
   * Constante CANT_SEND_REQUEST.
   * */
  CANT_SEND_REQUEST("18", "ERROR ENVIANDO PETICION"),
  /**
   * Constante REVERSE_DENIED.
   * */
  REVERSE_DENIED("19", "REVERSO DENEGADO"),
  /**
   * Constante SERVICE_REQUEST_TIME_OUT.
   * */
  SERVICE_REQUEST_TIME_OUT("20", "OPERACION NO REALIZADA, REINTENTE"),
  /**
   * Constante ORIGINAL_TXN_DOES_NOT_EXIST.
   * */
  ORIGINAL_TXN_DOES_NOT_EXIST("21", "LA TRANSACCION NO EXISTE"),
  /**
   * Constante TRANSACTION_IS_CANCELED.
   * */
  TRANSACTION_IS_CANCELED("22", "TRANSACCION PREVIAMENTE CANCELADA"),
  /**
   * Constante ACCOUNT_NO_PRESENT.
   * */
  ACCOUNT_NO_PRESENT("803", "NO CARD RECORD"),
  /**
   * Constante REFERENCED_TXN_FAIL.
   * */
  REFERENCED_TXN_FAIL("802", "FORMAT ERROR"),
  /**
   * Constante NOT_COMM_PROVIDER.
   * */
  NOT_COMM_PROVIDER("183", "SERVIDOR SIN COMUNICACION"),
  /**
   * Constante INVALID_RESPONSE.
   * */
  INVALID_RESPONSE("150", "TRASACCION INVALIDA"),
  /**
   * Constante TXN_RELATED_NOT_EXIST.
   * */
  TXN_RELATED_NOT_EXIST("27", "TRANSACCIÓN RELACIONADA A LA CANCELACION NO EXISTE"),
  /**
   * Constante POS_INCOMPLETE_PARAMETERS.
   * */
  POS_INCOMPLETE_PARAMETERS("91", "SIN RESPUESTA DEL PROVEDOR"),
  /**
   * Constante DB_ERROR.
   * */
  DB_ERROR("29", "ERROR DE BASE DE DATOS"),
  /**
   * Constante NOT_ANSWER_PROVIDER.
   * */
  NOT_ANSWER_PROVIDER("91", "SIN RESPUESTA DEL PROVEEDOR"),
  /**
   * Constante TRANSACTION_IS_REVERSE.
   * */
  TRANSACTION_IS_REVERSE("172", "TRN. FUE REVERSADA"),
  /**
   * Constante VALID_POS.
   * */
  VALID_POS("32", "PUNTO DE VENTA REGISTRADO"),
  /**
   * Constante REQUIRED_POS.
   * */
  REQUIRED_POS("33", "PUNTO DE VENTA REQUERIDO"),
  /**
   * Constante REQUIRED_ID_CLIENT.
   * */
  REQUIRED_ID_CLIENT("34", "IDENTIFICADOR DE CLIENTE REQUERIDO"),
  /**
   * Constante REQUIRED_USER.
   * */
  REQUIRED_USER("35", "NOMBRE DE USUARIO REQUERIDO"),
  /**
   * Constante REQUIRED_USER_P.
   * */
  REQUIRED_USER_P("36", "CLAVE DE USUARIO REQUERIDO"),
  /**
   * Constante INVALID_AMOUNT_TXN.
   * */
  INVALID_AMOUNT_TXN("13", "MONTO INVALIDO"),
  /**
   * Constante REPEAT_TXN_INCOMPLETE_PARAMETERS.
   * */
  REPEAT_TXN_INCOMPLETE_PARAMETERS("155", "INTENTE NUEVAMENTE"),
  /**
   * Constante TXN_DUPLICATE.
   * */
  TXN_DUPLICATE("94", "TRANSACCION DUPLICADA"),
  /**
   * Constante GRAL_ERROR.
   * */
  GRAL_ERROR("96", "LOGIN INCORRECTO / ERROR DE SISTEMA"),

  /**
   * Constante GRAL_ERROR.
   * */
  REVNF_ERROR("99", "TRANSACCIÓN RELACIONADA AL REVERSO NO EXISTE"),

  /**
   * Constante CONFLIT.
   * */
  CONFLIT("409", "CONFLICTO CON EL ESTADO ACTUAL DEL SERVIDOR"),
  /**
   * Constante SERVICE_UNAVAILABLE.
   * */
  SERVICE_UNAVAILABLE("503", "SERVIDOR CAIDO O EN MANTENIMIENTO");

  /**
   * Variable code.
   * */
  private final String code;
  /**
   * Variable message.
   * */
  private final String message;

  /**
   * constructor.
   * @param code as String
   * @param message as String
   * */
  CoreErrorServicesTable(final String code, final String message) {
    this.code = code;
    this.message = message;
  }

  /**
   * Getter code.
   * @return String
   * */
  @Override
  public String getErrorCode() {
    return code;
  }

  /**
   * Getter message.
    @return String
   * */
  @Override
  public String getErrorMessage() {
    return message;
  }

  /**
   * Getter CORE_ERROR.
    @return ErrorType
   * */
  @Override
  public ErrorType getErrorType() {
    return ErrorType.CORE_ERROR;
  }
}
