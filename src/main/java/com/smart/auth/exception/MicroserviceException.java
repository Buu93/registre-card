package com.smart.auth.exception;


import com.smart.auth.exception.logs.Console;
import com.smart.auth.model.enumutils.ErrorType;
import com.smart.auth.service.error.IErrorServices;
import com.smart.auth.model.enumutils.Number;

import java.io.Serializable;

/**
 * Class MicroserviceException.
 */
public class MicroserviceException extends Exception implements Serializable {

  private static final long serialVersionUID = 2405172041950251807L;
  /** * descripcion. */
  private final String description;
  /** * mensaje de error. */
  private final String errorMessage;
  /** * IErrorServices. */
  private final transient IErrorServices ierror;
  /** * codigo de error. */
  private final String errorCode;
  /** * ErrorType. */
  private final ErrorType errorType;
  /** * idOperation. */
  private final String idOperation;

  /**
   * MicroserviceException.
   * @param error IErrorServices.
   * @param idOperation String.
   */
  public MicroserviceException(final IErrorServices error, final String idOperation) {
    this(error, "", idOperation, true);
  }

  /**
   * MicroserviceException.
   * @param error IErrorServices.
   * @param idOperation String.
   * @param logEx boolean.
   */
  public MicroserviceException(final IErrorServices error, final String idOperation, final boolean logEx) {
    this(error, "", idOperation, logEx);
  }

  /**
   * MicroserviceException.
   * @param error IErrorServices.
   * @param description String.
   * @param idOperation String.
   */
  public MicroserviceException(final IErrorServices error, final String description, final String idOperation) {
    this(error, description, idOperation, true);
  }

  /**
   * constructor.
   * @param error IErrorServices.
   * @param description String.
   * @param idOperation String.
   * @param logEx boolean.
   */
  public MicroserviceException(final IErrorServices error, final String description,
                               final String idOperation, final boolean logEx) {
    this.ierror = error;
    this.errorMessage = error.getErrorMessage();
    this.errorCode = error.getErrorCode();
    this.errorType = error.getErrorType();
    this.idOperation = idOperation;
    this.description = description;
    if (logEx) {
      logException();
    }
  }

  /**
   * constructor.
   * @param error IErrorServices.
   *
   */
  public MicroserviceException(final IErrorServices error) {
    this.ierror = error;
    this.errorMessage = error.getErrorMessage();
    this.errorCode = error.getErrorCode();
    this.errorType = error.getErrorType();
    this.idOperation = "";
    this.description = "";

  }

  /**
   * Constructor empty.
   */
  public void forceReturnDescription() {
    //Method empty
  }


  /**
   * set result logException.
   */
  private void logException() {
    String errLog = "" + errorCode + ":" + errorType.getLabel()
          + ":" + errorMessage + ":" + description;

    if (errLog.length() > Number.CODE254.value()) {
      errLog = errLog.substring(0, Number.CODE254.value());
    }

    Console.writeln(Console.Level.ERROR, this.idOperation, errLog);
    Console.writeException(idOperation, this);
  }

  /**
   * getErrorMsg.
   * @return String.
   */
  public final String getErrorMsg() {
    return errorMessage;
  }

  /**
   * getErrorCode.
   * @return String.
   */
  public final String getErrorCode() {
    return this.ierror.getErrorCode();
  }

  /**
   * getDescription.
   * @return String.
   */
  public String getDescription() {
    return description;
  }

}
