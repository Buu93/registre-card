/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception;


import com.smart.auth.service.error.IErrorServices;

import java.io.Serializable;

/**
 * Class GeneralException.java.
 *
 * @author Jose A Rodriguez M
 */
public class GeneralException extends CommonOperationException implements Serializable {

  private static final long serialVersionUID = -7398620005872098141L;

  /**
   * Constructor GeneralException.
   * @param error IErrorServices
   */
  public GeneralException(final IErrorServices error) {
    super(error.getErrorCode(), error.getErrorMessage());
  }
}
