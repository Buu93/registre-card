/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception;

/**
 * Class CommonOperationException.java.
 *
 * @author Carlos Lopez
 */
public class CommonOperationException extends Exception {
  /** * codigo de respuesta. */
  private final String respCode;
  /** * mensaje de respuesta. */
  private final String respMessage;

  /**
   * constructor.
   * @param code String.
   * @param message String.
   */
  public CommonOperationException(final String code, final String message) {
    super(message);
    this.respCode = code;
    this.respMessage = message;
  }

  /**
   * getRespCode.
   * @return String.
   */
  public String getRespCode() {
    return respCode;
  }

  /**
   * getRespMessage.
   * @return String.
   */
  public String getRespMessage() {
    return respMessage;
  }
}
