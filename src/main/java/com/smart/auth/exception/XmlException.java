/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smart.auth.exception;

/**
 * Class XmlException.java.
 *
 * @author Jose A Rodriguez M
 */
public class XmlException extends Exception {

  /** * Error. */
  private final Error error;
  /** * detail. */
  private final String detail;

  /**
   * constructor.
   * @param error Error.
   * @param detail String.
   */
  public XmlException(final Error error, final String detail) {
    super(error.message);
    this.error = error;
    this.detail = detail;
  }

  /**
   * constructor.
   * @param error Error.
   */
  public XmlException(final String error) {
    this.error = Error.EMPTY;
    this.detail = Error.PARSER_CONFIGURATION.toString();
  }

  /**
   * constructor XmlException.
   * @param error Error.
   */
  public XmlException(final Error error) {
    super(error.message);
    this.error = error;
    detail = "";
  }

  /**
   * state response service.
   */
  public enum Error {
    /** * NOREAD. */
    NOREAD("01", "Formato de XML incorrecto"),
    /** * NOCLOSE. */
    NOCLOSE("02", "XML no cerrado correctamente"),
    /** * PARSER_CONFIGURATION. */
    PARSER_CONFIGURATION("03", "Parser configuration smartMicroservicio.exception"),
    /** * EMPTY. */
    EMPTY("04", "XML vacío"),
    /** * NOTYPE. */
    NOTYPE("05", "Parámetro tipoOperacion no encontrado"),
    /** * NOHEADER. */
    NOHEADER("06", "Tag <header> no encontrado en el XML"),
    /** * NOMESSAGE. */
    NOMESSAGE("07", "Tag <message> no encontrado en el XML"),
    /** * NOPARAM. */
    NOPARAM("08", "Parámetro no encontrado"),
    /** * NOSBTFORMAT. */
    NOSBTFORMAT("09", "XML no cumple con formato GNP"),
    /** * ERRORPARAM. */
    ERRORPARAM("10", "Valor o formato incorrecto del parámetro"),
    /** * LEFTOVERTAG. */
    LEFTOVERTAG("11", "El tag no corresponde a la especificación");

    /** * message. */
    private final String message;
    /** * errorCode. */
    private final String errorCode;

    /**
     * Constructor Error.
     * @param errorCode String.
     * @param message String.
     */
    Error(final String errorCode, final String message) {
      this.message = message;
      this.errorCode = errorCode;
    }
  }

  /**
   * getCode.
   * @return String.
   */
  public String getCode() {
    return error.errorCode;
  }

  /**
   * Obtiene el código del mensaje.
   *
   * @return mensaje de error
   */
  public String getCodeMessage() {
    String det = "";
    if (detail != null && !detail.isEmpty()) {
      det = " (" + detail + ")";
    }
    return error.message + det;
  }

}
