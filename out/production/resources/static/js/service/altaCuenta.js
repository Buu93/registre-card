var Http = new XMLHttpRequest();
var url = window.location;
var arrayResponse;
var tokenResponse;
Http.open("GET", url);
Http.send();

Http.onreadystatechange = function () {
    tokenResponse = Http.getResponseHeader("x-authcheckout");
    console.log(tokenResponse);
    arrayResponse = tokenResponse.split("|");
};

//función que abre el modal
$(function () {
    $('#modal').click(function () {
        $('#miModal').modal('show');
        if(arrayResponse[5] !== " "){
            document.getElementById('logo').src = arrayResponse[5];
        }
        if(arrayResponse[6] !== " "){
            document.getElementById('tituloModal').innerHTML = arrayResponse[6];
        }
        if(arrayResponse[7] !== " "){
            document.getElementById('colorFondo').style.backgroundColor = rgbToHex(arrayResponse[7]);
        }
        if(arrayResponse[8] != " "){
            document.getElementById('tituloModal').style.font.fontcolor = rgbToHex(arrayResponse[8]);
        }
    });
});

//función que enviá la información al controller.
function envioAltaController() {
    var vForm = {
        numTarjeta: $('#numCard').val(),
        fechaVencimiento: $('#fecha').val(),
        tipoOperacion: "60",
        aliasTarjeta: $('#aliasCard').val(),
        cvv: $('#cvv').val(),
        clientID:arrayResponse[1]
    };
    validaDatos(vForm);
    $.ajax({
        url: '/api/doRequest/',
        headers: {
            'x-authcheckout':tokenResponse,
            'Content-Type':'application/json'
        },
        method: 'POST',
        dataType: 'json',
        data: JSON.stringify(vForm),
        success: function(data){
            console.log('succes: '+data);
        }
    });
};

function validaDatos(obj){
    if(obj.numTarjeta == "" || obj.numTarjeta == null){
        $('#miModal').modal({backdrop: 'static', keyboard: false})
    }
};

function limpiarDatos() {
    document.getElementById('numCard').value = "";
    document.getElementById('cvv').value = "";
    document.getElementById('aliasCard').value = "";
    document.getElementById('fecha').value = "";
};

function solonumeros(e)
{
    var key = window.event ? e.which : e.keyCode;
    if(key < 48 || key > 57)
        e.preventDefault();
};

//función que configura la fecha capturada en el modal
$(function () {
    $('#fecha').datetimepicker({
        viewMode: 'years',
        format: 'MM/YYYY'
    });
});


function rgbToHex(rgb) {

    rgb.split(/.{1,2}/g);

    var r = (+rgb[0]).toString(16),
        g = (+rgb[1]).toString(16),
        b = (+rgb[2]).toString(16);

    if (r.length == 1)
        r = "0" + r;
    if (g.length == 1)
        g = "0" + g;
    if (b.length == 1)
        b = "0" + b;

    return "#" + r + g + b;
};


